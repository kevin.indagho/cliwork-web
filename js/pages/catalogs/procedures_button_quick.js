var dtproceduresbuttons;

$(document).ready(function(){
	$.ajax({
		url: urlapi+"Api_data/procbtncate",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_addprocbtn [name="category"]').append('<option value="'+item.Id+'">'+item.Description+'</option>');	
				$('#form_editprocbtn [name="category"]').append('<option value="'+item.Id+'">'+item.Description+'</option>');	
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$('#form_addprocbtn [name="procedurenew"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						data : { amount: item.Amount  }
					}));
				});
            }
            return array;
        }
    });
	
	dtproceduresbuttons = $("#dtproceduresbuttons").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_addprocbtn"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'procbtnedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'procbtndel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_data/procbtnquick",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'categoryName'},
			{data: 'ProcedureName'},
			{data: 'Description'},
			{data: 'Surf'},
			{data: null},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){ 
			if(data.active == 1){ $('td:eq(4)',row).html('<span class="label label-success label-bordered label-ghost">Active</span>'); }
			else{
				$('td:eq(4)',row).html('<span class="label label-danger label-bordered label-ghost">Inactive</span>');
			} 
		}
	});
	
	$('#dtproceduresbuttons tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#procbtndel').prop('disabled',true);
			$('#procbtnedit').prop('disabled',true);
        }else{
			$('#procbtndel').prop('disabled',false);
			$('#procbtnedit').prop('disabled',false);
            dtproceduresbuttons.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtproceduresbuttons tbody').on('dblclick','tr',function(e){
		dtproceduresbuttons.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#procbtnedit').trigger('click');
	});
	
	$('#procbtnedit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not b','Selected a quick button from the list'); }
		else{
			var data = dtproceduresbuttons.row($('.selection')).data();
			console.log(data);
			$('#form_editprocbtn [name="id"]').val(data.Id);
			$('#form_editprocbtn [name="status"][value="1"]').prop('checked',parseInt(data.active));
			$('#form_editprocbtn [name="currentprocedure"]').val(data.ProcedureName);
			$('#form_editprocbtn [name="category"]').val(data.categoryid).trigger('change');
			$('#form_editprocbtn [name="descript"]').val(data.Description);
			$('#form_editprocbtn [name="surf"]').val(data.Surf);
			$('#modal_editprocbtn').modal();
		}
		
	});
	
	$.validate({
		form : '#form_editprocbtn',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			var data = new FormData($form[0]);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $($form).serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Procedure button quick update');
						//$('#form_newpatient').get(0).reset();
						$('#modal_editprocbtn').modal('hide');
						dtproceduresbuttons.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});

	$.validate({
		form : '#form_addprocbtn',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			var data = new FormData($form[0]);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $($form).serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Procedure button quick add');
						//$('#form_newpatient').get(0).reset();
						$('#modal_addprocbtn').modal('hide');
						dtproceduresbuttons.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
});