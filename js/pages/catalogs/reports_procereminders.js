var dtreminders;
$(document).ready(function(){
	$('#form_addreminder [name="procedureid"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						data : { amount: item.Amount  }
					}));
				});
            }
            return array;
        }
    });

	$('#form_editreminder [name="procedureid"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						data : { amount: item.Amount  }
					}));
				});
            }
            return array;
        }
    });
	
	$('.summernote').summernote({
		height: 200,
		toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['para', ['ul', 'ol', 'paragraph']],
			['fontsize', ['fontsize']],
			['color', ['color']]
		]
	});
	
	dtreminders = $("#dtreminders").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_addreminder"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'remedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'remdel' }
			}
		],
		"ajax": {
			url: urlapi+"Api_data/procereminders",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'Descript'},
			{data: 'dias'},
			{data: 'tipo'},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			if(data.tipo == 2){
				$('td:eq(2)',row).html('Email');
			}else{
				$('td:eq(2)',row).html('SMS');
			}
		}
	});
	
	$('#dtreminders tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#remdel').prop('disabled',true);
			$('#remedit').prop('disabled',true);
        }else{
			$('#remdel').prop('disabled',false);
			$('#remedit').prop('disabled',false);
            dtreminders.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#remedit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dtreminders.row($('.selection')).data();
			$('#form_editreminder [name="procedureid"]').val('').trigger('change');			
			$('#form_editreminder [name="id"]').val(data.id);
			$('#form_editreminder [name="type"]').val(data.tipo);
			$('#form_editreminder [name="procedure"]').val(data.Descript);
			$('#form_editreminder [name="days"]').val(data.dias);
			$('#form_editreminder [name="message"]').summernote("code",data.mensaje);
			$('#modal_editreminder').modal('show');
		}
	});
	
	$('#remdel').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not reminder','Selected a reminder from the list'); }
		else{
			var data = dtreminders.row($('.selection')).data();
			swal({
				title: "Are you sure delete this reminder ?",
				text: "",
				type: "warning",
				showConfirmButton: 1,
				showCancelButton: 1,
				html:true,
				dangerMode: true,},
				function(isConfirm){
					if(isConfirm){
						$.ajax({
							url: urlapi+"Api_data/procereminders",
							type: "delete",
							data: {id: data.id},
							dataType: "json",
							beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
							success: function(response){
								if(response.code == 0){
									dtreminders.ajax.reload();
									apptools.sweetNoty('success','Complete','Delete reminder');
								}else{
									apptools.sweetNoty('danger','Error','');
								}
							}
						});
					}
			});
		}
	});
	
	$.validate({
		form : '#form_addreminder',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form) {
			var isEmpty = $('#form_addreminder [name="message"]').summernote('isEmpty');
			if(isEmpty == false){
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: $form.serialize(),
					dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							$('#form_addreminder [name="procedureid"]').val('').trigger('change');		
							$('#modal_addreminder').modal('hide');
							dtreminders.ajax.reload();
							apptools.sweetNoty('success','Complete','Insert reminder information');
						}else{
							apptools.sweetNoty('danger','Error','');
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Message required','This field is required.');
			}
		  return false;
		},
	});
	
	$.validate({
		form : '#form_editreminder',
		onError : function($form) {
			notyFormRequired();
		},
		onSuccess : function($form) {
			var isEmpty = $('#form_editreminder [name="message"]').summernote('isEmpty');
			if(isEmpty == false){
				$.ajax({
					url: urlapi+$form.attr('action'),
					type: $form.attr('method'),
					data: $form.serialize(),
					dataType: "json",
					beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
					success: function(response){
						if(response.code == 0){
							$('#modal_editreminder').modal('hide');
							dtreminders.ajax.reload();
							apptools.sweetNoty('success','Complete','Update reminder information');
						}else{
							apptools.sweetNoty('danger','Error','');
						}
					}
				});
			}else{
				notyCustom('information','<i class="fa fa-info-circle"></i> Message required','This field is required.');
			}
		  return false;
		},
	});
	
});