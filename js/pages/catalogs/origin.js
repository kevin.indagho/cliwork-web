var dtorigin;
$(document).ready(function(){
	dtorigin = $("#dtorigin").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_neworigin"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'oredit' }
				
			}
		],
		"ajax": {
			url: urlapi+"Api_data/origen",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'nombre'},
			{data: 'nombre'},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			if(data.activo == 1){
				$('td:eq(1)',row).html('<span class="label label-success label-bordered label-ghost">Active</span>');
			}else{
				$('td:eq(1)',row).html('<span class="label label-danger label-bordered label-ghost">Inactive</span>');
			}
		}
	});
	
	$.validate({
		form : '#form_neworigen',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Add new origin');
						$('#form_neworigen').get(0).reset();
						$('#modal_neworigin').modal('hide');
						dtorigin.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});

	$.validate({
		form : '#form_editorigin',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.code == 0){
						apptools.sweetNoty('success','Complete','Update origin');
						$('#form_editorigin').get(0).reset();
						$('#modal_editorigin').modal('hide');
						dtorigin.ajax.reload();
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#dtorigin tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#ordel').prop('disabled',true);
			$('#oredit').prop('disabled',true);
        }else{
			$('#ordel').prop('disabled',false);
			$('#oredit').prop('disabled',false);
            dtorigin.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtorigin tbody').on('dblclick','tr',function(e){
		dtorigin.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#oredit').trigger('click');
	});
	
	$('#oredit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not origin','Selected a origin from the list'); }
		else{
			var data = dtorigin.row($('.selection')).data();
			
			$('#form_editorigin [name="color"]').colorpicker('setValue',data.color);
			$('#form_editorigin [name="color"]').colorpicker('update');
			$('#form_editorigin [name="id"]').val(data.id);
			$('#form_editorigin [name="name"]').val(data.nombre);
			$('#form_editorigin [name="status"][value="1"]').prop('checked',parseInt(data.activo));
			$('#modal_editorigin').modal('show');
		}
	});
	
});