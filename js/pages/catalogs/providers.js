var dtproviders;
$(document).ready(function(){
	$.ajax({
		url: urlapi+"Api_data/archivostipo",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$('#form_newfile [name="type"]').append('<option></option>');
			$.each(response.data,function(i,item){
				$('#form_newfile [name="type"]').append($('<option>',{text: item.nombre,value: item.id}));
				$('#form_editfile [name="type"]').append($('<option>',{text: item.nombre,value: item.id}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	dtproviders = $("#dtproviders").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_newprovider"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { id: 'proviedit' }
				
			},
			{
				text: '<span class="fa fa-trash"></span> Delete',
				className: 'btn btn-danger btn-sm btn-icon-fixed',
				attr: { id: 'providel' }
			},{
				text: '<span class="fa fa-expand"></span>',
				className: 'btn btn-default btn-sm btn-icon',
				action: function ( e, dt, node, config ) {
					app.block.expand('#block-expand');
				}
			}
		],
		"ajax": {
			url: urlapi+"Api_data/providers",
			type: "get",
			data: {IsHidden:0, specialty:"none"},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns: [
			{data: 'Abbr'},
			{data: 'LName'},
			{data: 'FName'},
			{data: 'horario'},
		]
	});
	
	$('#dtproviders tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#providel').prop('disabled',true);
			$('#proviedit').prop('disabled',true);
        }else{
			$('#providel').prop('disabled',false);
			$('#proviedit').prop('disabled',false);
            dtproviders.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtproviders tbody').on('dblclick','tr',function(e){
		dtproviders.$('tr.selection').removeClass('selection');
		$('#providel').prop('disabled',false);
		$('#proviedit').prop('disabled',false);
        $(this).addClass('selection');
		$('#proviedit').trigger('click');
	});
	
	$.validate({
		form : '#form_newprov',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error',
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_nemodal_newproviderwfile').modal('hide');
						dtproviders.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false;
		},
	});
	
	$.validate({
		form : '#form_editprov',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_newfile').modal('hide');
						dtproviders.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#form_editprov [name="color"]').on('changeColor', function(event){
		$('#squarecolor').css('color', event.color.toString());
      });
	  
	$('#proviedit').on('click',function(e,d){
		var data = dtproviders.row($('.selection')).data();
		horario = data.horario.split("-");
		$('#form_editprov [name="id"]').val(data.id);
		$('#form_editprov [name="color"]').colorpicker('setValue',data.Color);
		$('#form_editprov [name="color"]').colorpicker('update');
		$('#form_editprov [name="Abbr"]').val(data.Abbr);
		$('#form_editprov [name="fname"]').val(data.FName);
		$('#form_editprov [name="lname"]').val(data.LName);
		$('#form_editprov [name="hourstart"]').val(horario[0]);
		$('#form_editprov [name="hourend"]').val(horario[1]);
		$('#form_editprov [name="specialty"]').val(data.Specialty).trigger('change');
		if(data.IsHidden == 0){ $('#form_editprov [name="status"][value="0"]').prop('checked',true);}
		else{  $('#form_editprov [name="status"][value="0"]').prop('checked',false); }
		$('#modal_editprovider').modal();
	});

	$('#providel').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a provider from the list'); }
		else{
			var data = dtproviders.row($('.selection')).data();
			swal({
				title: "Are you sure delete this provider ?",
				text: data.FName +" "+data.LName ,
				type: "warning",
				showConfirmButton: 1,
				showCancelButton: 1,
				html:true,
				dangerMode: true,},
				function(isConfirm){
					if(isConfirm){
						$.ajax({
							url: urlapi+"Api_data/providers",
							type: "DELETE",
							data: { id:data.id},
							dataType: "json",
							beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
							success: function(response){
								dtproviders.ajax.reload();
							}
						});
					}else{
					}
			});
		}
	});
	
});