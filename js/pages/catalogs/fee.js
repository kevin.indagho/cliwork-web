var dtfee;
$(document).ready(function(){
	$.ajax({
		url: urlapi+"Api_data/feesched",
		type: "GET",
		dataType: "json",
		beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
		success: function(response){
			$.each(response.data,function(i,item){
				console.log(item);
				$('#form_addfee [name="category"]').append('<option value="'+item.id+'">'+item.Description+'</option>');
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$('#form_addfee [name="procedure"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {by:"Descript",name: '{{{q}}}',limit: 10 },
			dataType: "json",
			beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); }
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.AbbrDesc,
						value: item.id,
						data : { subtext: item.Descript }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
    });
	
	//feesched_get
	
	dtfee = $("#dtfee").DataTable({
		dom: '<"col-md-6"B><"col-md-6"f>rt<"col-md-6 hidden-mobile"i><"col-md-6"p>',
		buttons: [
			{
				text: '<span class="fa fa-plus"></span> Add',
				className: 'btn btn-success btn-sm btn-icon-fixed',
				attr:{
					"data-backdrop": "static",
					"data-toggle": "modal", 
					"data-target": "#modal_addfee"
					}
			},
			{
				text: '<span class="fa fa-pencil"></span> Edit',
				className: 'btn btn-warning btn-sm btn-icon-fixed',
				attr: { "disabled": "",id: 'feeEdit' }
				
			}
		],
		"ajax": {
			url: urlapi+"Api_data/precios",
			type: "get",
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
		},
		columns: [
			{data: 'Description'},
			{data: 'Descript'},
			{data: 'AmountFormat'},
		],
		"rowCallback": function( row, data ){
			$('td:eq(2)',row).addClass('text-right');
			/*if(data.Active == 1){ $('td:eq(4)',row).html('<span class="label label-success label-bordered label-ghost">Active</span>'); }
			else if(data.Active == 0){ $('td:eq(4)',row).html('<span class="label label-danger label-bordered label-ghost">Inactive</span>'); }*/
		}
	});
	
	$.validate({
		form : '#form_editfee',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Complete','Proceedure fee update');
						$('#modal_editfee').modal('hide');
						dtfee.ajax.reload();
					}else{
						apptools.sweetNoty('danger','Complete','Proceedure fee update');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_addfee',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Complete','Proceedure fee update');
						$('#modal_editfee').modal('hide');
						dtfee.ajax.reload();
					}else{
						apptools.sweetNoty('danger','Complete','Proceedure fee update');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});	
	
	$('#dtfee tbody').on( 'click', 'tr', function () {
        if($(this).hasClass('selection')){
            $(this).removeClass('selection');
			$('#feeEdit').prop('disabled',true);
        }else{
			$('#feeEdit').prop('disabled',false);
            dtfee.$('tr.selection').removeClass('selection');
            $(this).addClass('selection');
        }
    });
	
	$('#dtfee tbody').on('dblclick','tr',function(e){
		dtfee.$('tr.selection').removeClass('selection');
        $(this).addClass('selection');
		$('#feeEdit').trigger('click');
	});
	
	$('#feeEdit').on('click',function(){
		if($('.selection').length == 0){ apptools.notyCustom('information','<i class="fa fa-info"></i> Not Patient','Selected a patient from the list'); }
		else{
			var data = dtfee.row($('.selection')).data();
			console.log(data);
			$('#form_editfee [name="id"]').val(data.id);
			$('#form_editfee [name="category"]').val(data.Description);
			$('#form_editfee [name="procedure"]').val(data.Descript);
			$('#form_editfee [name="amount"]').val(data.Amount);
			$('#modal_editfee').modal('show');
		}
	});
	
});