var dtstage;
$(document).ready(function(){
	dtstage = $("#dtstage").DataTable({
		"ajax": {
			url: urlapi+"Api_data/etapas",
			type: "get",
			data: { activo:"none"},
			beforeSend: function (xhr){
				xhr.setRequestHeader('Authorization', auth);
			},
			data: {activo: "none"},
		},
		columns: [
			{data: 'name'},
			{data: 'dias',class:"hidden-mobile"},
			{data: null,class:"hidden-mobile"},
			{defaultContent: '<button type="button" class="edit btn btn-warning btn-xs"><i class="fa fa-pencil"></i></button>'},
		],
		"rowCallback": function( row, data ){
			if(data.activo == 1){ active = '<span class="label label-success">Active</span>'}
			else{ active = '<span class="label label-danger">Inactive</span>';}
			$('td:eq(2)',row).html(active);
		}
	});
	
	$.validate({
		form : '#form_newstage',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Completed','Data save on the system.');
						$('#modal_newstage').modal('hide');
						dtstage.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editstage',
		onError : function($form){
			notyCustom('error','<i class="fa fa-exclamation-circle"></i> Error','Complete all inputs');
		},
		onSuccess : function($form) {
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				beforeSend: function (xhr){ xhr.setRequestHeader('Authorization', auth); },
				success: function(response){
					if(response.data.code == 0){
						apptools.sweetNoty('success','Completed Update','Data save on the system.');
						$('#modal_editstage').modal('hide');
						dtstage.ajax.reload();
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$('#dtstage tbody').on('click','.edit',function(){
		var data = dtstage.row($(this).parents('tr')).data();
		$('#form_editstage [name="id"]').val(data.id);
		$('#form_editstage [name="name"]').val(data.name);
		$('#form_editstage [name="days"]').val(data.dias);
		$('#form_editstage [name="status"]').prop('checked',parseInt(data.activo));
		$('#modal_editstage').modal();
	});
});