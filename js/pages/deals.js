var origenProviders = [];

$(document).ready(function(){
	//apptools.sweetNoty('success','Activity Completed',"This activity mark as completed.");	
	$(".birthday_picker").datetimepicker({format: "MM/DD/YYYY",viewMode: 'years'});
	//$(".scrollCustom").mCustomScrollbar('destroy');
	$(".scrollProceadd").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});	
	patients();
	patients_m();
	
	$.ajax({
		url: urlapi+"Api_data/providers",
		type: "GET",
		data: {IsHidden: 0,specialty: "none"},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_editdeal [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
				$('#form_newdeal [name="doctor"]').append($('<option>',{text: item.FName+" "+item.LName,value: item.id}));
			});
			$('.bs-select').val('default').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/origen",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,etapa){
				$('#origencheck').append('<div class="app-checkbox inline"><label><input type="checkbox" name="origen[]" value="'+etapa.id+'" checked>'+etapa.nombre+'</label></div>');
			});
		},complete:function(){
			$(".app-checkbox label, .app-radio label").each(function(){
                $(this).append("<span></span>");
            });
			data = $('#origencheck input[name="origen[]"]:checked');
			$.each(data,function(i,item){
				origenProviders.push($(item).val());
			});
			
			$('#origencheck input[name="origen[]"]').on('change',function(){
				data = $('#origencheck input[name="origen[]"]:checked');
				origenProviders = [];
				$.each(data,function(i,item){
					origenProviders.push($(item).val());
				});
				origenProviders = {'origen':origenProviders};
				dtproviders.ajax.reload(function(){
					apptools.notyCustom('information','Update providers comparation','');
				});
			});
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/pais",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,item){
				$('#form_editpatient [name="country"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
				$('#form_newpatient [name="country"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_pacienteestados/lista",
		type: "GET",
		data: {activo: 'none'},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,etapa){
				$('#form_editpatient [name="etapa"]').append($('<option>',{
					text: etapa.name,
					value: etapa.id
				}));
				
				$('#form_newpatient [name="etapa"]').append($('<option>',{
					text: etapa.name,
					value: etapa.id
				}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/origen",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,etapa){
				$('#form_newpatient [name="origin"]').append($('<option>',{
					text: etapa.nombre,
					value: etapa.id
				}));
				$('#form_editpatient [name="origin"]').append($('<option>',{
					text: etapa.nombre,
					value: etapa.id
				}));
			});
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	
	$.ajax({
		url: urlapi+"Api_pacienteestados/lista",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,estado){
				$('#form_editstatus [name="etapa"]').append($('<option>',{
					text: estado.name,
					value: estado.id
				}));
				
				$('#panelsaccordion').append('<div class="panel panel-default">'
                                            +'<div class="panel-heading" role="tab" href="#stage'+estado.id+'" data-toggle="collapse" data-parent="#panelsaccordion" aria-expanded="false" >'
                                                +'<h4 class="panel-title">'
                                                    +'<a role="button">'
                                                        +estado.name
                                                    +'</a>'
                                                +'</h4>'
                                            +'</div>'
                                            +'<div id="stage'+estado.id+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body padding-0"></div></div>'
                                        +'</div>');
										
				$('#tabsstage').append('<div class="item">'
					+'<div class="heading"><div class="title">'+estado.name+'</div></div>'
					+'<div class="content"><div class="block"></div></div>'
				+'</div>');
			});
			
			 var app_accordion = $('#tabsstage');
               app_accordion.find(".item").each(function(){
                    var app_accordion_item = $(this);                   
                    if(!app_accordion.data("type")){
                        app_accordion.addClass("app-accordion-simple");
					}                   
                    app_accordion_item.find(".heading").on("click",function(){                       
                        if(app_accordion_item.hasClass("open")){
                            app_accordion_item.removeClass("open").removeAttr("style");
						}else{
							app_accordion_item.addClass("open");
						}
                        if(app_accordion.data("open") === "close-other"){
                           app_accordion.find(".item").not(app_accordion_item).removeClass("open").removeAttr("style");                           
                        }
                        if(app_accordion.data("type") === "full-height"){
                            app.accordionFullHeight(app_accordion);
                        }
                        
                    });
               });
			$('.bs-select').selectpicker('refresh');
		}
	});
	
	$.ajax({
		url: urlapi+"Api_tratamiento/lista",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,tratamiento){
				$('#form_newdeal [name="treatment[]"]').append($('<option>',{
					text: tratamiento.name,
					value: tratamiento.id
				}));
				$('#form_editdeal [name="treatment[]"]').append($('<option>',{
					text: tratamiento.name,
					value: tratamiento.id
				}));
			});
			$('.multiselect').multiSelect('refresh');
		}
	});
	
	$.validate({
		form : '#form_newpatient',
		modules : 'logic',
		validateOnBlur : true, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$('#index'+tabname).addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onError : function($form){
			//apptools.notyCustom('error','fields Required','Complete All inputs');
		},
		onSuccess : function($form){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			
			var data = new FormData($form[0]);
			
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				headers: apptools.headersajax(),
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						let nombre = $("#form_newpatient [name='nombre']").val();
						let apellidos = $("#form_newpatient [name='apellidos']").val();
						
						$('#form_newdeal [name="patient"]').append("<option value='"+response.id+"'>"+nombre+" "+apellidos+"</option>");
						$('#form_newdeal [name="patient"]').selectpicker('refresh');
						$('#form_newdeal [name="patient"]').val(response.id).trigger("change");
						
						$('#form_newpatient').get(0).reset();
						$('#modal_newpatient').modal('hide');
						swal({
							title: "Complete",
							text: "Add new patient",
							type: "success",
							showConfirmButton: 1,
							showCancelButton: 0,
							dangerMode: true,},
							function(isConfirm){
							  if (isConfirm) {
								$("#modal_newdeal").modal();
							  }else{}
							});
					}else{
						apptools.sweetNoty('error','Error','');
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});

	$.validate({
		form : '#form_editstatus',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form){
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			data.push({name: "usario", value: apptools.userlogin});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						apptools.shownoty("information",'<i class="fa fa-refresh fa-spin fa-fw"></i> Stages Update',"Updating list of stages");
						patients();
						patients_m();
						$('#modal_editstatus').modal('hide');
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_newdeal',
		validateOnBlur : false, 
		errorMessagePosition : 'top',
		onElementValidate : function(valid, $el, $form, errorMess){
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			if(valid == false){
				tabname = $el.parents('.tab-pane').attr('id');
				$form.find('a[href="#'+tabname+'"]').addClass('indexerror');
			}
		},
		onValidate : function($form) {
			$('.indexerror').addClass('text-danger');
			$('.indexerror').addClass('indexerror');
		},
		onSuccess : function($form) {
			$('.indexerror').removeClass('text-danger');
			$('.indexerror').removeClass('indexerror');
			var data = $form.serializeArray();
			data.push({name:"usuario",value: apptools.userlogin});
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					if(response.code == 0){
						patients();
						patients_m();
						apptools.sweetNoty('success','Deal created','the new deal save');
						$("#form_newdeal").trigger("reset");
						$('#listProcedures').empty();
						$('#modal_newdeal').modal('hide');
					}else{
						apptools.sweetNoty('warning','Error',response.message);
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	$.validate({
		form : '#form_editdeal',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						patients();
						patients_m();
						apptools.sweetNoty('success','Completed','Update Deal');
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});

	$.validate({
		form : '#form_lostpatient',
		onError : function($form) {
			noty({
				layout: 'top',
				theme: 'defaultTheme',
				timeout: 2000,
				type: 'error', // success, error, warning, information, notification
				text: '<strong>ERROR</strong>Complete All inputs',
			});
		},
		onSuccess : function($form) {
			$form.find('button[type="submit"]').prop('disabled',true);
			$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-spinner fa-pulse fa-fw');
			var data = $form.serializeArray();
			data.push( {name: "userid", value: apptools.userlogin })
			$.ajax({
				url: urlapi+$form.attr('action'),
				type: $form.attr('method'),
				data: data,
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$form.find('button[type="submit"]').prop('disabled',false);
					$form.find('button[type="submit"]').find('span').find('i').removeClass().addClass('fa fa-floppy-o');
					if(response.code == 0){
						patients();
						noty({
							layout: 'topRight',
							theme: 'defaultTheme',
							timeout: 4000,
							type: 'success',
							text: '<strong><i class="fa fa-check"></i> Complete</strong> update deal',
						});
						$('#form_lostpatient').get(0).reset();
						$('#modal_lostpatient').modal('hide');
					}else{
						noty({
							layout: 'top',
							theme: 'defaultTheme',
							type: 'error',
							text: '<strong>Error</strong>'+response.message,
						});
					}
				}
			});
		  return false; // Will stop the submission of the form
		},
	});
	
	dtproviders = $("#dtproviders").DataTable({
		"ajax": {
			url: urlapi+"Api_paciente/provcomp",
			type: "get",
			headers: apptools.headersajax(),
			data: function ( d ) {
				var params = origenProviders;
				return params;
			},
		},
		columns: [
			{data: 'name',class:"padding-5"},
			{data: 'amount',class:"padding-5"},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			if(data.EtapaId != null){ estado = '<span class="label label-'+data.etapaColor+' label-bordered label-ghost">'+data.etapaName+'</span>'}
			else{ estado = '';}
			$('td:eq(3)',row).html(estado);
			
			if(data.Gender == 1){ genero = 'Female'}
			else if(data.Gender == 2){ genero = "Male";}
			else if(data.Gender == 3){ genero = "Gender-Neutral";}
			else{ genero = '';}
			$('td:eq(2)',row).html(genero);
		}
	});
	
	dtlostdeals = $("#dtlostdeals").DataTable({
		"ajax": {
			url: urlapi+"Api_paciente/ofertas",
			type: "get",
			headers: apptools.headersajax(),
			data: {estado: 2,paciente: 59},
		},
		columns: [
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
			{data: 'patient'},
			{data: 'treatments[].ing'},
			{data: 'value'},
			{data: 'user'},
			{defaultContent: '<button type="button" class="ver btn btn-info btn-xs"><i class="fa fa-eye"></i></button>'},
		]
	});
	
	/*Actions controls*/
	$('#btnAddPatient').on('click',function(){
		$('#modal_newdeal').modal('hide');
		setTimeout(function(){ $('#modal_newpatient').modal(); }, 500);
	});
	
	$('#showcontact').on('click',function(){
		$('#contactPatient').slideToggle("slow");
		//$('#contactPatient').show();
	});

	$('#form_newpatient [name="country"]').on('change',function(){
		id = $(this).val();
		$('#form_newpatient [name="state"]').empty();
		$('#form_newpatient [name="state"]').append('<option></option>');
		$.ajax({
			url: urlapi+"Api_data/paisestado",
			type: "GET",
			data: {activo: 1,pais: id},
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				$.each(response.data,function(i,item){
					$('#form_newpatient [name="state"]').append('<option value="'+item.id+'">'+item.nombre+'</option>');
				});
				$('.bs-select').selectpicker('refresh');
			}
		});
	});

	$('#form_newpatient [name="birthdate"]').on('dp.change',function(){
		var date = $(this).val()
		$.ajax({
			url: urlapi+"Tools/calculate_age",
			type: "POST",
			data: {date: date},
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				$('#age')[0].innerHTML = "Age: "+response;
			}
		});
	});
	
	$('#form_newdeal [name="patient"]').on('input',function(){
		$('#addonpatient')[0].innerHTML = '<i class="fa fa-spinner fa-pulse"></i>';
		$('#patientsearch').empty();
		var val = this.value;
		if( val != ""){
			$.ajax({
				url: urlapi+"Api_paciente/buscador",
				type: "GET",
				data: {nombre: val},
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$('#patientsearch').empty();
					$.each(response.data,function(i,tratamiento){
						$('#patientsearch').append($('<option value="'+tratamiento.LName+" "+tratamiento.FName+'" data-id="'+tratamiento.Id+'"></option>'));
					});
					$('.multiselect').multiSelect('refresh');

					var patient = $('#patientsearch option[value="'+val+'"]');
					if(patient.length > 0){
						$('#form_newdeal [name="id"]').val(patient.data('id'));
						$('#addonpatient')[0].innerHTML = '<i class="fa fa-eye"></i>';
						$('#addonpatient').attr('data-original-title','Select patient existing');
						getpatient(patient.data('id'));
					}else{
						$('#form_newdeal [name="id"]').val(0);
						$('#addonpatient')[0].innerHTML = "<i class='fa fa-plus'></i>";
						$('#addonpatient').attr('data-original-title','Add new patient');
					}
				}
			});
		}else{
			$('#addonpatient')[0].innerHTML = '';
		}
	});
	
	$('#form_newdeal [name="treatment[]"]').on('change',function(){
		var t = $(this).val();
		if(t != ""){
			$.ajax({
				url: urlapi+"Api_tratamiento/totalcosto",
				type: "POST",
				data: {tratamientos: t},
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$('#form_newdeal [name="value"]').val(response.data[0].totalFormat);
				}
			});
		}else{
			$('#form_newdeal [name="value"]').val("");
		}
	});
	
	$('#form_editdeal [name="treatment[]"]').on('change',function(){
		var t = $(this).val();
		if(t != ""){
			$.ajax({
				url: urlapi+"Api_tratamiento/totalcosto",
				type: "POST",
				data: {tratamientos: t},
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$('#form_editdeal [name="value"]').val(response.data[0].totalFormat);
				}
			});
		}else{
			$('#form_editdeal [name="value"]').val("");
		}
	});
	
	$('#btnlost').on('click',function(){
		$('#modal_editstatus').modal('hide');
		$('#modal_lostpatient').modal();
	});

	$('#dtlostdeals tbody').on('click', 'td.details-control', function(){
        var tr = $(this).closest('tr');
        var row = dtlostdeals.row( tr );
        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	$('.dealdetails').on('click',function(){
		var dealid = $('#form_editstatus [name="idoferta"]').val();
		var patientid = $('#form_lostpatient [name="id"]').val();
		window.open(base_url+"deals/details/"+dealid+"/"+patientid, '_blank');
	});

	$('#form_newdeal [name="seller"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_usuario/buscador",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {tipo: "3", name: '{{{q}}}' },
			dataType: "json",
			headers: apptools.headersajax()
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.nombre+" "+item.apellidos,
						value: item.id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
    });
	
	$('#form_newdeal [name="patient"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/patsearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : { name: '{{{q}}}' },
			dataType: "json",
			headers: apptools.headersajax()
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.FName+" "+item.LName,
						value: item.Id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
    });
	
	$('#form_newdeal [name="procedure"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			headers: apptools.headersajax()
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						data : { amount: item.Amount  }
					}));
				});
            }
            return array;
        }
    });
	
	$('#form_editdeal [name="procedure"]').selectpicker({liveSearch: true}).ajaxSelectPicker({
        ajax : {
			url: urlapi+"Api_data/proceduressearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : {limit:20, by:"Descript", name: '{{{q}}}' },
			dataType: "json",
			headers: apptools.headersajax()
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.Descript,
						value: item.id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            return array;
        }
    });
	
	$('#form_newdeal [name="patient"]').on('changed.bs.select',function(){
		val = $(this).val();
		if(val != ''){
			$.ajax({
				url: urlapi+"Api_paciente/detalle",
				type: "GET",
				data : { id: val},
				dataType: "json",
				headers: apptools.headersajax(),
				success: function(response){
					$('#form_newdeal [name="email').val(response.data[0].Email);
					$('#form_newdeal [name="phome"]').val(response.data[0].HmPhone);
					$('#form_newdeal [name="pmobile"]').val(response.data[0].WirelessPhone);
					$('#form_newdeal [name="pwork]').val(response.data[0].WkPhone);
				}
			});
		}
	});
	
	$('#form_newdeal [name="procedure"]').on('changed.bs.select',function(){
		ul = $('#listProcedures li');
		amount = $(this)[0].selectedOptions[0].dataset.amount;
		text = $(this)[0].selectedOptions[0].label;
		val = $(this).val();
		if(val != ''){
			$('#listProcedures').append('<li id="addli'+ul.length+'" data-amount="'+amount+'" class="list-group-item"><input type="hidden" name="treatment[]" value="'+val+'"><a href="#" onclick="deleteproce('+ul.length+');">'+text+'</a><span class="badge badge-success">$ '+amount+'</span></li>');
		}
		$('#form_newdeal [name="procedure"]').val('default').selectpicker("refresh");
		addof_caltotal();
	});
	
	$('#form_editdeal [name="procedure"]').on('changed.bs.select',function(){
		text = $(this)[0].selectedOptions[0].label;
		val = $(this).val();
		if(val != ''){
			$('#listProceduresEdit').append('<li class="list-group-item"><input type="hidden" name="proceadd[]" value="'+val+'">'+text+'</li>');
		}
	});
	
	$('#btnrefresh').on('click',function(){
		
		patients();
		patients_m();
		
	});

	$('#tbl_act tbody').on('change','.complete',function(){
		check = $(this);
		idacti = check.val();
		estado = (check[0].checked == true?2:1);
		$.ajax({
			url: urlapi+"Api_paciente/actividadestado",
			type: "POST",
			data: {id: idacti,estado: estado},
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				if(response.code == 0){
					ofertaid = $('#form_lostpatient [name="ofertaid"]').val();
					pacienteid = $('#form_lostpatient [name="id"]').val();
					etapaid = $('#form_editstatus [name="etapa"]').val();
					//apptools.sweetNoty('success','Activity Completed',"This activity mark as completed.");
					updateAct(pacienteid,ofertaid,etapaid);
					swal({
						title: "Activity Completed",
						text: "Add new activity",
						type: "success",
						showConfirmButton:1,
						showCancelButton:1,
						confirmButtonText:"No",
						cancelButtonText:"Yes",
						html:true,
						dangerMode: true,},
						function(isConfirm){
							if(!isConfirm){
								window.location = apptools.base_url+"appointments/agenda";    
							}
					});
				}else{
					apptools.sweetNotyError(response.msg);
				}
			}
		});
	});

	$("#quicknewpat").on("click",function(){
		
		$("#modal_newdeal").modal('hide');
		setTimeout(function(){ $("#modal_newpatient").modal(); }, 500);
	});

	$("#form_newpatient [name='nombre'], #form_newpatient [name='apellidos']").on('focusout',function(){
		let nombre = $("#form_newpatient [name='nombre']").val();
		let apellidos = $("#form_newpatient [name='apellidos']").val();
		$.ajax({
			url: urlapi+"Api_data/patsearch",
			type: "GET",
			data : { name: apellidos+" "+nombre},
			dataType: "json",
			headers: apptools.headersajax(),
			success:function(response) {
				if(response.data.length > 0){
					$('#namecomparation').html(response.data[0].FName+" "+response.data[0].LName);
					apptools.notyCustom("warning","Se encontro una coincidencia con el nombre:",response.data[0].FName+" "+response.data[0].LName);
				}else{
					$('#namecomparation').html("");
				}
			}
        });
	});

});	

function addof_caltotal(){
	ul = $('#listProcedures li');
	total = 0;
	$.each(ul,function(i,li){
		amountAttach = $(li).data('amount');
		total = total + amountAttach;
	});
	$('#form_newdeal [name="value"]').val(total);	
}

function deleteproce(id){
	$('#addli'+id).remove();
	addof_caltotal();
}

function completedacti(idacti){
	$.ajax({
		url: urlapi+"Api_paciente/actividadestado",
		type: "POST",
		data: {id: idacti,estado: 2},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			if(response.code == 0){
				ofertaid = $('#form_lostpatient [name="ofertaid"]').val();
				pacienteid = $('#form_lostpatient [name="id"]').val();
				etapaid = $('#form_editstatus [name="etapa"]').val();
				apptools.sweetNoty('success','Activity Completed',"This activity mark as completed.");
				updateAct(pacienteid,ofertaid,etapaid);
			}else{
				apptools.sweetNotyError(response.msg);
			}
		}
	});
}

function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';
}
	
function patients(){
	app.block.loading.start("#block_deals");
	$.ajax({
		url: urlapi+"Api_pacienteestados/listapacientes",
		type: "GET",
		data: {activo: 1,vendido: 0,user:apptools.userlogin ,usertype: apptools.userlogintype},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$('#tbllist thead').empty();
			$('#tbllist tbody').empty();
			$('#tbllist thead').append('<tr></tr>');
			$('#tbllist tbody').append('<tr></tr>');
			
			$.each(response.data,function(i,etapa){
				$('#tbllist thead tr').append('<th class="tbl_headers">'
					+'<h4><span class="text-bold">'+etapa.name+'</span></h4>'
					+'<p class="subheader"><small><i class="fa fa-usd" aria-hidden="true"></i> '+etapa.total+'</small></p>'
					+'</th>');
				var li = "";
				$.each(etapa.ofertas,function(i,oferta){
					li += '<li style="word-break: break-word;" data-oldetapa="'+oferta.etapaId+'" data-idoferta="'+oferta.ofertaid+'" data-idpaciente="'+oferta.patientid+'" class="sort list-group-item grab">'
						+'<strong>'+oferta.FName+' '+oferta.LName+'</strong>'
						+'<button class="editstage btn btn-'+oferta.Estado+' btn-icon btn-xs btn-glow " onclick="editstage('+oferta.patientid+','+oferta.ofertaid+','+etapa.id+','+oferta.etapaId+');" type="button" id="pop'+oferta.patientid+'" style="position: absolute; top: 50%; right: 0; margin: -16px 8px 0 0;" >'
							+'<span style="'+(oferta.Estado != "default"? "color:white;": "")+'" class="icon-menu"></span>'
						+'</button>'
						/***/
						/***/
						+'<br><small class="text-muted">'+oferta.fechaestado+'</small>'
						+'<br><small class="text-muted">$'+oferta.valorFormato+'  '+oferta.tratamientosNames+'</small></li>';
				});
				
				$('#tbllist tbody tr').append('<td class="padding-0" style="width:16.6%; white-space: normal;">'
					+'<div id="'+etapa.name+'" class="sort padding-5" style="overflow: hidden;">'
						+'<ul data-etapa="'+etapa.id+'" id="list'+etapa.name+'" class="listSort list-group" style="height: 500px;">'+li+'</ul>'
					+'</div>'
				+'</td>');
				$('#stage'+etapa.id+' .panel-body').append('<ul data-etapa="'+etapa.id+'" id="list'+etapa.name+'" class="list-group" style="height: 500px;">'+li+'</ul>');
			});

			$(".listSort").disableSelection();
			$(".listSort").sortable({
					revert: 'invalid',
					connectWith: ".listSort",
					items: "li.sort",
					receive: function( event, ui ){
						etapaid = event.target.dataset.etapa;
						oldetapa = ui.item[0].dataset.oldetapa;
						ofertaid = ui.item[0].dataset.idoferta;
						pacienteid = ui.item[0].dataset.idpaciente;
						$.ajax({
							url: urlapi+"Api_paciente/editaretapaoferta",
							type:"POST",
							dataType:"json",
							data:{
								oldetapa: oldetapa, 
								etapa: etapaid, 
								idoferta: ofertaid,
								userid: apptools.userlogin,
								patientid: pacienteid
							},
							headers: apptools.headersajax(),
							success: function(response){
								if(response.code == 0){
									apptools.shownoty("information",'<i class="fa fa-refresh fa-spin fa-fw"></i> Stages Update',"Updating list of stages");
									patients();
									patients_m();
								}
							}							
						});
					}
				});
		},complete:function(){
			$(".listSort").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});	
			app.block.loading.finish("#block_deals");
		}
	});
}

function patients_m(){
	$.ajax({
		url: urlapi+"Api_pacienteestados/listapacientes",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			
			$.each(response.data,function(i,etapa){
				var li = "";
				$.each(etapa.ofertas,function(i,oferta){
					li += '<li style="word-break: break-word; cursor:move;" data-idoferta="'+oferta.ofertaid+'" data-idpaciente="'+oferta.patientid+'" class="sort list-group-item border-left-0 border-right-0">'
						+'<strong>'+oferta.FName+' '+oferta.LName+'</strong>'
						+'<button class="editstage btn btn-default btn-icon btn-xs btn-glow btn-'+oferta.Estado+'" onclick="editstage('+oferta.patientid+','+oferta.ofertaid+','+etapa.id+');" type="button" id="pop'+oferta.patientid+'" style="position: absolute; top: 50%; right: 0; margin: -16px 8px 0 0;" >'
							+'<span style="color:white;" class="icon-menu"></span>'
						+'</button>'
						+'<br><small class="text-muted">'+oferta.fechaestado+'</small>'
						+'<br><small class="text-muted">$'+oferta.valorFormato+'  '+oferta.tratamientosNames+'</small></li>';
				});
				$('#stage'+etapa.id+' .panel-body').empty();
				$('#stage'+etapa.id+' .panel-body').append('<ul data-etapa="'+etapa.id+'" id="list'+etapa.name+'" class="list-group" style="height: 500px;">'+li+'</ul>');
			});
		}
	});
}

function getpatient(pacienteid){
//detalle_get
	$.ajax({
		url: urlapi+"Api_paciente/detalle",
		type: "GET",
		data: {id: pacienteid},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$('#form_newdeal [name="email"]').val(response.data[0].Email)
			$('#form_newdeal [name="phome"]').val(response.data[0].HmPhone)
			$('#form_newdeal [name="pwork"]').val(response.data[0].WkPhone)
			$('#form_newdeal [name="pmobile"]').val(response.data[0].WirelessPhone)
		}
	});
}

function details(){	
	var id = $('#form_editstatus [name="id"]').val();
	window.open(base_url+"patient/details/"+id, '_blank');
}

function editstage(pacienteid,ofertaid,etapaid,oldetapa){
	updateAct(pacienteid,ofertaid,etapaid);
	$.ajax({
		url: urlapi+"Api_paciente/detalleoferta",
		type: "GET",
		data: {id: ofertaid},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$('#form_lostpatient [name="id"]').val(response[0].pacienteid);
			$('#form_lostpatient [name="patient"]').val(response[0].FName+" "+response[0].LName);
			$('#form_lostpatient [name="ofertaid"]').val(response[0].id);
			$('#form_editstatus [name="oldetapa"]').val(oldetapa);
			$('#form_editstatus [name="idoferta"]').val(response[0].id);
			$('#form_editstatus [name="patientid"]').val(response[0].pacienteid);
			$('#form_editstatus [name="patient"]').val(response[0].FName+" "+response[0].LName);
			$('#form_editstatus [name="etapa"]').val(response[0].EtapaId).trigger('change');
			$('#form_editdeal [name="id"]').val(response[0].id);
			$('#form_editdeal [name="patient"]').val(response[0].FName+" "+response[0].LName);
			
			console.log(response);
			$('#form_editdeal [name="value"]').val(response[0].valorFormato);
			$('#form_editdeal [name="closedate"]').val(response[0].fechaCierre);
			$('#form_editdeal [name="email"]').val(response[0].Email);
			$('#form_editdeal [name="phome"]').val(response[0].HmPhone);
			$('#form_editdeal [name="pwork"]').val(response[0].WkPhone);
			$('#form_editdeal [name="pmobile"]').val(response[0].WirelessPhone);
			
			$('#form_editdeal [name="doctor"]').val(response[0].doctorid).trigger('change');
			
			$('#form_editdeal [name="treatment[]"]').val(response[0].tratamientos).trigger('change');
			$('#form_editdeal [name="treatment[]"]').multiSelect("refresh");
			$('#listProceduresEdit').empty();
			$.each(response[0].procedimientos,function(i,item){
				$('#listProceduresEdit').append('<li class="list-group-item"><div class="app-checkbox margin-0"><label><input type="checkbox" name="procedel[]" value="'+item.id+'">'+item.Descript+'<span></span></label> </div></li>');
			});
		},complete: function(){
			$(".scrollCustom").mCustomScrollbar('destroy');
			$(".scrollCustom").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});	
		}
	});
	$('#modal_editstatus').modal('show');
}

function updateAct(pacienteid,ofertaid,etapaid){
	$('#tbl_act tbody').empty();
	$.ajax({
		url: urlapi+"Api_paciente/listaactividadespendientes",
		type: "GET",
		data: {id: pacienteid,ofertaid: ofertaid,etapaid: etapaid},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,actividad){
				$('#tbl_act tbody').append('<tr>'
					+'<td class="padding-5"><label data-toggle="tooltip" data-placement="right" class="switch switch-sm switch-cube">'
					+'<input type="checkbox" class="complete checkbox" value="'+actividad.id+'">'
					+'<span></span>'
				+'</label></td>'
					+'<td class="padding-5">'+actividad.fechaInicio+'</td>'
					+'<td class="padding-5">'+actividad.usuarioNombre+' '+actividad.usuarioApellido+'</td>'
					+'<td class="padding-5">'+actividad.nota+'</td>'
				+'</tr>');
			});
		}
	});
}