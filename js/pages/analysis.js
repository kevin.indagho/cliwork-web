var origenProviders = [];

$(document).ready(function(){
	//$(".scrollCustom").mCustomScrollbar('destroy');
	$(".scrollProceadd").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});	
	
	$.ajax({
		url: urlapi+"Api_paciente/provcompdeals",
		type: "GET",
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			let tr = $('#tbl_provcomp thead')[0].children[0]
			$.each(response[0].providers,function(i,item){
				$(tr).append('<th class="padding-5">'+item.provi+'</th>');
			});
			
			let table = $('#tbl_provcomp tbody');
			$.each(response,function(i,item){
				tr = "<tr><td class='padding-5'>"+item.mes+"</td>";
				$.each(item.providers,function(i,provider){
					tr +='<td class="padding-5">'+provider.mount+'</td>';
				});
				$(table).append(tr);
			});
		}
	});
	
	$.ajax({
		url: urlapi+"Api_data/origen",
		type: "GET",
		data: {activo: 1},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$.each(response.data,function(i,etapa){
				$('#origencheck').append('<div class="app-checkbox inline"><label><input type="checkbox" name="origen[]" value="'+etapa.id+'" checked>'+etapa.nombre+'</label></div>');
			});
		},complete:function(){
			$(".app-checkbox label, .app-radio label").each(function(){
                $(this).append("<span></span>");
            });
			data = $('#origencheck input[name="origen[]"]:checked');
			$.each(data,function(i,item){
				origenProviders.push($(item).val());
			});
			
			$('#origencheck input[name="origen[]"]').on('change',function(){
				data = $('#origencheck input[name="origen[]"]:checked');
				origenProviders = [];
				$.each(data,function(i,item){
					origenProviders.push($(item).val());
				});
				origenProviders = {'origen':origenProviders};
				dtproviders.ajax.reload(function(){
					apptools.notyCustom('information','Update providers comparison','');
				});
			});
		}
	});
	
	dtproviders = $("#dtproviders").DataTable({
		"ajax": {
			url: urlapi+"Api_paciente/provcomp",
			type: "get",
			headers: apptools.headersajax(),
			data: function ( d ) {
				var params = origenProviders;
				return params;
			},
		},
		columns: [
			{data: 'name',class:"padding-5"},
			{data: 'amount',class:"padding-5"},
		],
		"drawCallback": function( settings ){ $("[data-toggle='tooltip']").tooltip(); },
		"rowCallback": function( row, data ){
			if(data.EtapaId != null){ estado = '<span class="label label-'+data.etapaColor+' label-bordered label-ghost">'+data.etapaName+'</span>'}
			else{ estado = '';}
			$('td:eq(3)',row).html(estado);
			
			if(data.Gender == 1){ genero = 'Female'}
			else if(data.Gender == 2){ genero = "Male";}
			else if(data.Gender == 3){ genero = "Gender-Neutral";}
			else{ genero = '';}
			$('td:eq(2)',row).html(genero);
		}
	});

});