var userid = $('#userid').val();
var usermd5 = $('#usermd5').val();
var base_url = "http://localhost:8080/cliwork-web/";
var pathapi = "http://localhost:8080/cliwork-api/";
var urlapi = "http://localhost:8080/cliwork-api/index.php/";
var auth = "Basic " + Base64.encode("2040b8bc8c13f06adb24cd9758e588a22657d01c029e7f71840ea99cec8d9773bb32f78cf2969495431832fb84a473b27518ead45f6867963f63e6b01be270c5TIO0/ac2gzTBEYNDJdB56A7h2u0tJJnEp0hqSO2XACs=:17c7d09355671abdaceaeab5fd6d3c37f867dd7aff78562f2134ff3d2e58b072aaa3877232adc61910cf883dc022378ecc56ab0a805d2f666d6529a5713b3fe1AECc3D2WIx+uxnILG6NqajmzzSE177nru1I3XzMyd4E=");
var dateToday = new Date();

var apptools = {
	base_url: "http://localhost:8080/cliwork-web/",
	pathapi: "http://localhost:8080/cliwork-api/",
	urlapi: "http://localhost:8080/cliwork-api/index.php/",
	usermd5: $('#usermd5').val(),
	userlogin: $('#userid').val(),
	userlogintype: $('#usertype').val(),
	nombreid: $('#nombreid').val(),
	clinicTime:"",
	activityTime:{
		calendar:"01:00",
		timepicker:"01:00"
	},
	headersajax: function(){
		data = {
			'usertype': $('#usertype').val(),
			'dbconfig':dbconfig,
			'Authorization':"Basic "+Base64.encode("2040b8bc8c13f06adb24cd9758e588a22657d01c029e7f71840ea99cec8d9773bb32f78cf2969495431832fb84a473b27518ead45f6867963f63e6b01be270c5TIO0/ac2gzTBEYNDJdB56A7h2u0tJJnEp0hqSO2XACs=:17c7d09355671abdaceaeab5fd6d3c37f867dd7aff78562f2134ff3d2e58b072aaa3877232adc61910cf883dc022378ecc56ab0a805d2f666d6529a5713b3fe1AECc3D2WIx+uxnILG6NqajmzzSE177nru1I3XzMyd4E=")
		};
		return data;
	},
	openPrint: function(dataContent){		
		params  = 'width='+screen.width;
		params += ', height='+screen.height;
		params += ', top=0, left=0'
		params += ', fullscreen=yes';
		params += ', directories=no';
		params += ', location=no';
		params += ', menubar=no';
		params += ', resizable=no';
		params += ', scrollbars=no';
		params += ', status=no';
		params += ', toolbar=no';
		var WinId = window.open('', 'newwin',params);
		WinId.document.open();
		WinId.document.write(dataContent);
		WinId.document.close();
	},
	sweetNoty: function(type,title,msg){
		swal(title,msg,type);
	},
	sweetNotyError: function(msg){
		swal('Error',msg,'error');
	},
	notyCustom: function(type,title,message){
		/*
			alert
			warning
			information
			success
			error
		*/
		noty({
			layout: 'topRight',
			theme: 'defaultTheme',
			type: type,
			timeout: 10000,
			animation: {
				open: 'animated fadeInRightBig',
				close: 'animated fadeOutRightBig'
			},
			text: '<strong>'+title+'</strong>'+message
		});
	},
	notysuccess: function(){
		noty({
			layout: 'topRight',
			theme: 'defaultTheme',
			type: 'success',
			timeout: 3000,
			text: '<strong><i class="fa fa-check fa-lg"></i> Complete</strong> Data save on the system.'
		});
	},
	notyerror: function(){
		noty({
			layout: 'topRight',
			theme: 'defaultTheme',
			type: 'error',
			timeout: 10000,
			text: '<strong><i class="fa fa-exclamation-circle fa-lg"></i> Error</strong> Data not save on the system.'
		});
	},
	readNotification: function(notiid){
		$.ajax({
			url: urlapi+"Api_noti/leida",
			type: "POST",
			data: {id: notiid},
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				console.log(response);
			}
		});
	},
	loadNotifications: function(userid){
		app.block.loading.start("#block_noti");
		$('#listnoti').empty();
		$.ajax({
			url: urlapi+"Api_usuario/notificaciones",
			type: "GET",
			data: {id: userid,nueva:"none"},
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				if(response.cantidad[0].cantidad > 0){
					$('#informernoti').append('<span class="informer informer-danger informer-sm informer-square">+'+response.cantidad[0].cantidad+'</span>'); 
				} else {
					$('#informernoti').hide();
				}
				$.each(response.notificaciones,function(i,noti){
					if(noti.leida == 0){ color = "background-color: beige;";
					}else{ color = ""; }
					$('#listnoti').append('<div data-read="'+noti.leida+'" data-id="'+noti.id+'" data-type="'+noti.tipoid+'" data-pat="'+noti.pacienteid+'" data-deal="'+noti.ofertaid+'" class="ver app-timeline-item">'
								+'<div class="dot dot-'+noti.color+'"></div>'
								+'<div class="content pointer" style="'+color+'">'
								+noti.cuerpo
								+'<p>'
									+'<span class="pull-right text-muted"><i class="fa fa-calendar"></i> '+noti.fecha+'</span>'
								+'</p>'
								+'</div>'
							+'</div>');
				});
			},
			complete: function(){
				$("#listnoti").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
				app.block.loading.finish("#block_noti");
			}
		});
	},
	shownoty: function(type,title,message){
		noty({
			layout: 'topRight',
			theme: 'defaultTheme',
			type: type,
			timeout: 3000,
			text: '<strong>'+title+'</strong>'+message
		});	
	},
	load_unseen_notification: function(){
		var today = new Date();
		var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
		var time = today.getHours() + ":" + today.getMinutes();
		var dateTime = date+' '+time;
		
		$.ajax({
			url: urlapi+"Api_usuario/notificaciones",
			type: "GET",
			data: {id:userid, nueva:1},
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				if(response.cantidad[0].cantidad > 0){ 
					noty({
						layout: 'topRight',
						theme: 'defaultTheme',
						type: 'information',
						timeout: 5000,
						icon: 'far fa fa-info-circle',
						text: '<strong><i class="fa fa-info-circle" aria-hidden="true"></i> New notifications</strong> You have new notifications',
						 callback:{
							afterShow:function(){
								audio.play();
							}
						}
					});
				}
				$.each(response.notificaciones,function(i,noti){
					if(noti.leida == 0){ color = "background-color: beige;";
					}else{ color = ""; }
					
					$('#listnoti').append('<div data-read="'+noti.leida+'" data-id="'+noti.id+'" data-type="'+noti.tipoid+'" data-pat="'+noti.pacienteid+'" data-deal="'+noti.ofertaid+'" class="ver app-timeline-item" style=".pointer {cursor: pointer;}">'
								+'<div class="dot dot-'+noti.color+'"></div>'
								+'<div class="content" style="'+color+'">'
								+noti.cuerpo
								+'<p>'
									+'<span class="pull-right text-muted"> '+noti.nueva+' <i class="fa fa-calendar"></i> '+noti.fecha+'</span>'
								+'</p>'
								+'</div>'
							+'</div>');
				});
			},
			complete: function(){
				$("#listnoti").mCustomScrollbar('destroy');
				$("#listnoti").mCustomScrollbar({axis:"y", autoHideScrollbar: true, scrollInertia: 200, advanced: {autoScrollOnFocus: false}});
				app.block.loading.finish("#block_noti");
			}
		});
	}
};


function resetForm(idform){
	$('#'+idform).get(0).reset();
}

function notyCustom(type,title,message){
	noty({
		layout: 'topRight',
		theme: 'defaultTheme',
		type: type,
		text: '<strong>'+title+'</strong>'+message
	});	
}

function notyFormSuccess(){
	noty({
		layout: 'topRight',
		theme: 'defaultTheme',
		timeout: 5000,
		type: 'success',
		text: '<strong><i class="fa fa-check-circle" aria-hidden="true"></i> Complete</strong> Data saved in the system.',
	});
}

function notyFormRequired(){
	noty({
		layout: 'topRight',
		theme: 'defaultTheme',
		timeout: 5000,
		type: 'error', // success, error, warning, information, notification
		text: '<strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Fields required</strong> Complete all fields required.',
	});
}

function notyError(message){
	noty({
		layout: 'topRight',
		theme: 'defaultTheme',
		type: 'error',
		text: '<strong>Error</strong>'+message,
	});
}

function viewdeallost(pat,deal){
	$.ajax({
		url: urlapi+"Api_paciente/detalleoferta",
		type: "GET",
		data: {id: deal},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			$('#form_lostpatient [name="date"]').val(response[0].perdidaFecha);
			$('#form_lostpatient [name="reason"]').val(response[0].perdidaRazon);
			$('#form_lostpatient [name="patient"]').val(response[0].FName+" "+response[0].LName);
			$('#form_lostpatient [name="etapa"]').val(response[0].EtapaId).trigger('change');
			$('#form_lostpatient [name="id"]').val(response[0].id);
			$('#form_lostpatient [name="patient"]').val(response[0].FName+" "+response[0].LName);
			$('#form_lostpatient [name="value"]').val(response[0].valorFormato);
			$('#form_lostpatient [name="closedate"]').val(response[0].fechaCierre);
			$('#form_lostpatient [name="email"]').val(response[0].Email);
			$('#form_lostpatient [name="phome"]').val(response[0].HmPhone);
			$('#form_lostpatient [name="pwork"]').val(response[0].WkPhone);
			$('#form_lostpatient [name="pmobile"]').val(response[0].WirelessPhone);
			$('#form_lostpatient [name="treatment[]"]').val(response[0].tratamientos).trigger('change');
			$('#form_lostpatient [name="treatment[]"]').multiSelect("refresh");
		}
	});	
	$('#modal_deallost').modal('show');
}

function viewdeal(pat,deal){
	$.ajax({
		url: urlapi+"Api_paciente/detalleoferta",
		type: "GET",
		data: {id: deal},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			
			$('#form_view_deal [name="patient"]').val(response[0].FName+" "+response[0].LName);			
			$('#form_view_deal [name="email"]').val(response[0].Email);
			$('#form_view_deal [name="phome"]').val(response[0].HmPhone);
			$('#form_view_deal [name="pwork"]').val(response[0].WkPhone);
			$('#form_view_deal [name="pmobile"]').val(response[0].WirelessPhone);			
			$('#form_view_deal [name="value"]').val(response[0].valorFormato);
			$('#form_view_deal [name="closedate"]').val(response[0].fechaCierre);
			$('#modal_view_deal .viewdealdetails').attr('data-patid',response[0].pacienteid);
			$('#modal_view_deal .viewdealdetails').attr('data-dealid',response[0].id);
			
			treatments = response[0].tratamientosNames.split(',');
			$('#treatmentlist').empty();
			$.each(treatments,function(i,item){
				if(item != ''){
					$('#treatmentlist').append('<li class="list-group-item disabled">'+item+'</li>');
				}
			});
		}
	});
	$('#modal_view_deal').modal('show');
}

apptools.loadNotifications(userid);

$('#listnoti').on('click','.ver',function(){
	id = $(this).data('id');
	read = $(this).data('read');
	type = $(this).data('type');
	deal = $(this).data('deal');
	pat = $(this).data('pat');
	$(this).find('.content').removeAttr('style');
	if(type == 1){ viewdeallost(pat,deal); }
	if(type == 2){ viewdeal(pat,deal); }
	if(read == 0){ apptools.readNotification(id); }
});

$(function(){
	console.log("loading configurations ...");
	
	$('#patsearch').selectpicker({size:10,liveSearch: true}).ajaxSelectPicker({
		ajax : {
			url: urlapi+"Api_data/patsearch",
			type: "GET",
			// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
			// automatically replace it with the value of the search query.
			data : { by:'AbbrDesc',name: '{{{q}}}' },
			dataType: "json",
			headers: apptools.headersajax()
        },
        locale : { emptyTitle: 'Nothing selected' },
        log : 3,
        preprocessData: function (response){
            var l = response.data.length, array = [];			
            if (l){
				$.each(response.data,function(i,item){
					array.push($.extend(true,i,{
						text : item.FName+" "+item.LName,
						value: item.Id,
						//data : { subtext: "Sub Text" }
					}));
				});
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
	});
	
	$('#patsearch').on('changed.bs.select',function(e){
		val = $(this).val();
		if(val != ""){
			window.open(base_url+'patient/perfil/'+val,'_blank');
		/*$.ajax({
			url: base_url+"Cuenta/select_patient",
			type:"post",
			data: {patient: val},
			success: function(response){
			}
		});*/
		}
	});
	
	if(selectpat != ''){
		$.ajax({
			url: urlapi+"Api_data/patient",
			type: "GET",
			data : {id: selectpat },
			dataType: "json",
			headers: apptools.headersajax(),
			success: function(response){
				$('#frmsearchpat [name="patient"]').append('<option value="'+response.data[0].Id+'">'+response.data[0].FName+' '+response.data[0].LName+'</option>').selectpicker('refresh');
				$('#frmsearchpat [name="patient"]').val(response.data[0].Id).trigger('change');
			}
		});
	}
	
	$('#logout').on('click',function(){
		swal({
		  title: "Quieres cerrar sesión",
		  text: "?",
		  type: "warning",
		  showConfirmButton: 1,
		  showCancelButton: 1,
		  dangerMode: true,},
		  function(isConfirm){
			if (isConfirm) {
				window.location.href = apptools.base_url+'logout';
			}else{}
		  });
	});

	$('.viewdealdetails').on('click',function(){
		window.open(base_url+"deals/details/"+$(this).data('dealid')+"/"+$(this).data('patid'), '_blank');
	});
	
	$(".bs-datepickerusa").datetimepicker({format: "MM/DD/YYYY"});
	
	$(".bs-datepickerusa_dis_past").datetimepicker({format: "MM/DD/YYYY", minDate: dateToday});

	/*
	* leer configuraciones del sistema.
	*/
	$.ajax({
		url: apptools.urlapi+"Api_data/config",
		type: "GET",
		async:false,
		data: {id: "none"},
		dataType: "json",
		headers: apptools.headersajax(),
		success: function(response){
			apptools.clinicTime = response.data[2].instrucciones.split('-');
			apptools.activityTime.calendar = response.data[5].instrucciones;
			apptools.activityTime.timepicker = response.data[5].instrucciones.split(':')[1];
		}
	});	
	
	$('#btnSyncNot').on('click',function(){
		$(".listnoti").mCustomScrollbar("destroy");
		apptools.loadNotifications(userid);
	});
});

