<?php 
class Analysis extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "analysis";
		$encabezado['submenu'] = "comproviders";
		$encabezado['optbtn'] = "";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/analysis.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('analysis/index');
		$this->load->view('theme/piepagina',$pie);
	}
}
?>