<?php 
class Treatplan extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	
#region VIEWS
	public function index($tpid){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "tp";
		$encabezado['submenu'] = "";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		$pie['scripts'] = array('js/pages/treatplan.js');
		
		$data['tpid'] = $tpid;
		$data['chartup'] = $this->load->view('treatplan/chart',null,true);
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('treatplan/index',$data);
		$this->load->view('theme/piepagina',$pie);
		
	}
#endregion
	
}?>