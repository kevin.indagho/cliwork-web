<?php 
class Citas extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	
#region VIEWS
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "appointments";
		$encabezado['submenu'] = "agenda";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/appointments_agenda.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('citas/index');
		$this->load->view('theme/piepagina',$pie);
		
	}
#endregion
	
}?>