<?php 
class Cuestionarios extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		
	}
	
#region VIEWS
	public function index(){
		if($this->session->autenticado == false){ redirect("login"); }
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "appointments";
		$encabezado['submenu'] = "agenda";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/cuestionarios.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('cuestionarios/index');
		$this->load->view('theme/piepagina',$pie);
	}
	
	public function responder($id){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "appointments";
		$encabezado['submenu'] = "agenda";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		$pie['scripts'] = array('js/pages/cuestionarios_responder.js');
		
		$data['proceid'] = $id;
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('cuestionarios/responder',$data);
		$this->load->view('theme/piepagina',$pie);
		
	}
	
	public function preguntas(){
		if($this->session->autenticado == false){ redirect("login"); }
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "appointments";
		$encabezado['submenu'] = "agenda";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/cuestionarios_preguntas.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('cuestionarios/preguntas');
		$this->load->view('theme/piepagina',$pie);
	}
	
#endregion
	
}?>