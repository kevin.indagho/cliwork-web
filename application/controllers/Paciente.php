<?php 
class Paciente extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	
	#region VIEWS
		public function index(){
			$encabezado['menunav'] = "";
			$encabezado['menu'] = "patiens";
			$encabezado['submenu'] = "list";
			$encabezado['optbtn'] = "minimize";
			$encabezado['menuminimized'] = false;
			
			$pie['scripts'] = array('js/pages/patient.js');
			
			$this->load->view('theme/encabezado',$encabezado);
			$this->load->view('theme/navegacion');
			$this->load->view('paciente/index');
			$this->load->view('theme/piepagina',$pie);
		}
		
		public function perfil($patid){
			$encabezado['menu'] = "patiens";
			$encabezado['submenu'] = "list";			
			$pie['scripts'] = array('js/pages/patient_perfil.js');
			
			$data['patient'] = $patid;
			$this->load->view('theme/encabezado',$encabezado);
			$this->load->view('theme/navegacion');
			$this->load->view('paciente/perfil',$data);
			$this->load->view('theme/piepagina',$pie);
		}
		
		public function detalle($pacienteid){
			$encabezado['menunav'] = "app-sidebar-left-closed";
			$encabezado['menu'] = "patiens";
			$encabezado['submenu'] = "list";
			$encabezado['optbtn'] = "toggle";
			$encabezado['menuminimized'] = false;
			
			$pie['scripts'] = array('js/pages/patientdetails.js');
			
			$detalle['id'] = $pacienteid;
			$this->load->view('theme/encabezado',$encabezado);
			$this->load->view('theme/navegacion');
			$this->load->view('paciente/detalle',$detalle);
			$this->load->view('theme/piepagina',$pie);
		}
		
		public function ofertas(){
			$encabezado['menunav'] = "app-sidebar-left-closed";
			$encabezado['menu'] = "patiens";
			$encabezado['submenu'] = "deals";
			$encabezado['optbtn'] = "toggle";
			$encabezado['menuminimized'] = false;
			
			$pie['scripts'] = array('js/pages/deals.js');
			
			$this->load->view('theme/encabezado',$encabezado);
			$this->load->view('theme/navegacion');
			$this->load->view('paciente/ofertas');
			$this->load->view('theme/piepagina',$pie);
		}
		
		public function ofertaslost(){
			$encabezado['menunav'] = "app-sidebar-left-closed";
			$encabezado['menu'] = "patiens";
			$encabezado['submenu'] = "deals";
			$encabezado['optbtn'] = "toggle";
			$encabezado['menuminimized'] = false;
			
			$pie['scripts'] = array('js/pages/deals_lost.js');
			
			$this->load->view('theme/encabezado',$encabezado);
			$this->load->view('theme/navegacion');
			$this->load->view('oferta/lost');
			$this->load->view('theme/piepagina',$pie);
		}
		
		public function cuenta(){
			$encabezado['menunav'] = "app-sidebar-left-closed";
			$encabezado['menu'] = "patiens";
			$encabezado['submenu'] = "";
			$encabezado['menuminimized'] = false;
			
			$pie['scripts'] = array('js/pages/account.js');
			
			$this->load->view('theme/encabezado',$encabezado);
			$this->load->view('theme/navegacion');
			$this->load->view('paciente/cuenta');
			$this->load->view('theme/piepagina',$pie);
		}
	#endregion
	
	public function post(){
		print_r($_POST);
	}
	
	public function prueba(){
		$co = array('nombre' => $_POST['nombre'],"configuracion" => $_POST['config']);
		$this->db->insert("tables_configuracion",$co);
		$data = $this->db->error();
		print_r(json_encode($data));
		
	}

	public function pruebados(){
		$this->db->select('configuracion');
		$this->db->from('tables_configuracion');
		$this->db->where('nombre',"DataTables_dtpatient");
		$data = $this->db->get()->result();
		print(json_encode($data[0]->configuracion));
		
	}
	
}
?>