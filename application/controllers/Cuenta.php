<?php 
class Cuenta extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}
	
	public function login(){
		if($this->session->autenticado == true){
			redirect("home");
		}else{
			$this->load->view('cuenta/login');
		}
	}

	public function autenticar(){
		$this->load->library('encryption');
		$this->load->database();
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('correo', 'Email', 'required');
		$this->form_validation->set_rules('contrasena', 'password', 'required');
		
		if ($this->form_validation->run() == FALSE){
			$response = array('status' => false,'msg' =>"Formulario incompleto");
		}else{
			$correo = $this->input->post('correo');
			$contrasena = $this->input->post('contrasena');
			/*
			* Leer los datos del usuario
			*/
			$this->db->select('cliente_usuarios.id,
			cliente_usuarios.correo,
			cliente_usuarios.contrasena,
			clientes.nombre,
			clientes.hostNombre,
			clientes.hostUsuario,
			clientes.hostContrasena,
			clientes.hostDBNombre');
			$this->db->from('cliente_usuarios');
			$this->db->join('clientes','cliente_usuarios.clienteid = clientes.id','inner');
			$this->db->where('correo',$correo);
			$this->db->where('contrasena',md5($contrasena));
			$data = $this->db->get();
			
			/*
			* Validar si se encontro un usuario
			*/
			if($data->conn_id->affected_rows > 0){
				$data = $data->row_array();
				
				/*
				* Encriptar la informacion de conexion de la base de datos
				*/
				$response = array('status' => true,'nombre'=>MD5($data['nombre']),'data' => $this->encryption->encrypt(json_encode($data)));
			}else{
				$response = array('status' => false,'msg' =>"Correo o Contraseña incorrectos.");
			}
			
		}
		$this->db->close();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	public function permisos($tipoid){
		$data = array();
		$this->db->select('modulos.id,
		modulos.menu,
		modulos.submenu,
		modulos.nombre,
		modulos.description,
		modulos.descripcion,
		modulos_permisos.id as permisoid,
		modulos_permisos.permitido,
		modulos_permisos.usuariotipid');
		$this->db->from('modulos_permisos');
		$this->db->join('modulos','modulos_permisos.moduloid = modulos.id','inner');
		$this->db->where('modulos_permisos.usuariotipid',$tipoid);
		$permisos = $this->db->get()->result_array();
		foreach($permisos as $permiso){
			$data[$permiso['id']] = $permiso;
		}
		return $data;
	}
	
	public function select_patient(){
		$this->session->set_userdata('patient',$this->input->post('patient'));
	}
	
	public function crear_session(){
		$this->load->library('encryption');
		$newdata = array(
			'id' => $this->input->post('id'),
			'md5' => MD5($this->input->post('id')),
			'nombre' => $this->input->post('nombre'),
			'nombreid' => $this->input->post('nombreid'),
			'apellidos' => $this->input->post('apellidos'),
			'tipoid' => $this->input->post('tipoid'),
			'tiponombre' => $this->input->post('tiponombre'),
			'permisos' => $this->input->post('permisos'),
			'correo' => $this->input->post('correo'),
			'dbconfig' => $this->input->post('db'),
			'autenticado' => TRUE);
		$this->session->set_userdata($newdata);
		
		$response = array('status'=>true);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($response));
	}
	
	public function cerrar_session(){
		$this->session->sess_destroy();
        $data = array( "autenticado" => false );
        $this->session->set_userdata($data);
        redirect('login');
	}
	
	public function d(){
		print_r($_SESSION);
	}
	
}
?>