<?php 
class Usuario extends CI_controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	/*VIEWS*/
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "users";
		$encabezado['submenu'] = "list";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/users.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('usuario/index');
		$this->load->view('theme/piepagina',$pie);
	}
	
	public function permisos(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "users";
		$encabezado['submenu'] = "per";
		$encabezado['optbtn'] = "minimize";
		
		$pie['scripts'] = array('js/pages/userpermission.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('usuario/permisos');
		$this->load->view('theme/piepagina',$pie);
	}
	
	/******/
	
}
?>