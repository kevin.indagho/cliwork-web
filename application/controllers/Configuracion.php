<?php
class Configuracion extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "config";
		$encabezado['submenu'] = "system";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/config.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('configuracion/index');
		$this->load->view('theme/piepagina',$pie);
	}
	
	public function notifications(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "config";
		$encabezado['submenu'] = "system";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/config_notifications.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('configuracion/notifications');
		$this->load->view('theme/piepagina',$pie);
	}
	
}
 ?>