<?php
class Oferta extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	
	public function index($ofertaid,$pacienteid){
		$encabezado['menunav'] = "app-sidebar-left-closed";
		$encabezado['menu'] = "patients";
		$encabezado['submenu'] = "deals";
		$encabezado['optbtn'] = "toggle";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/dealsdetails.js');
		
		$detalle['ofertaid'] = $ofertaid;
		$detalle['pacienteid'] = $pacienteid;
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('oferta/detalle',$detalle);
		$this->load->view('theme/piepagina',$pie);
	}
}
?>