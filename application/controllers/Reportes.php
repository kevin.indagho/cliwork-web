<?php 
Class Reportes extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "reports";
		$encabezado['submenu'] = "act";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		$pie['scripts'] = array('js/pages/reports_others.js');
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('reportes/otros');
		$this->load->view('theme/piepagina',$pie);
	}
	
	public function actividades(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "reports";
		$encabezado['submenu'] = "act";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/reports_activities.js');
		
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('reportes/actividades');
		$this->load->view('theme/piepagina',$pie);
		
	}

	public function ventas(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "reports";
		$encabezado['submenu'] = "ven";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/reports_deals.js');
		
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('reportes/ventas');
		$this->load->view('theme/piepagina',$pie);
		
	}
	
	public function recordatorios_ofertas_procedimientos(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "reports";
		$encabezado['submenu'] = "act";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		$pie['scripts'] = array('js/pages/catalogs/reports_procereminders.js');
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('reportes/ofertas_recordatorios');
		$this->load->view('theme/piepagina',$pie);
	}

}
?>