<?php
class Dashboard extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	
	/* VIEWS */
	public function index(){
		$pie['scripts'] = array('js/pages/dashboard.js');
		
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "home";
		$encabezado['submenu'] = "";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('dashboard/index');
		$this->load->view('theme/piepagina',$pie);
	}
	
	public function calendario(){
		$pie['scripts'] = array('js/pages/dashboard.js');
		
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "home";
		$encabezado['submenu'] = "";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('dashboard/calendario');
		$this->load->view('theme/piepagina',$pie);
	}
	/* ***** */
	
	
}
?>