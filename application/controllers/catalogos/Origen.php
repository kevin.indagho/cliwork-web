<?php 
class Origen extends CI_controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	/*VIEWS*/
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "data";
		$encabezado['submenu'] = "stage";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/catalogs/origin.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('catalogos/origen');
		$this->load->view('theme/piepagina',$pie);
	}
	
	/******/
	
}