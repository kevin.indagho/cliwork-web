<?php 
class Procedures extends CI_controller{
	public function __construct(){
		parent::__construct();
		if($this->session->autenticado == false){ redirect("login"); }
	}
	/*VIEWS*/
	public function index(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "data";
		$encabezado['submenu'] = "procedures";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/catalogs/procedures.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('catalogos/procedures');
		$this->load->view('theme/piepagina',$pie);
	}
	
	public function quickbuttons(){
		$encabezado['menunav'] = "";
		$encabezado['menu'] = "data";
		$encabezado['submenu'] = "procedures";
		$encabezado['optbtn'] = "minimize";
		$encabezado['menuminimized'] = false;
		
		$pie['scripts'] = array('js/pages/catalogs/procedures_button_quick.js');
		
		$this->load->view('theme/encabezado',$encabezado);
		$this->load->view('theme/navegacion');
		$this->load->view('catalogos/procedures_quick');
		$this->load->view('theme/piepagina',$pie);
	}
	
	/******/
	
}