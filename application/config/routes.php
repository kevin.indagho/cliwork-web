<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Dashboard';
$route['404_override'] = 'Error_msg/error_404';
$route['translate_uri_dashes'] = FALSE;

/*Inicio de sesion*/
$route['login'] = 'cuenta/login';
$route['logout'] = 'cuenta/cerrar_session';

$route['home'] = 'Dashboard';

#region Analysis
	$route["analysis/providers"] = "Analysis/index";
#nedregion

/*Usuario*/
	$route['users/list'] = 'Usuario/index';
	$route['users/permission'] = 'Usuario/permisos';
#region Appoientmens
	$route['appointments/agenda'] = 'Citas/index';
#endregion

#region TreatPlan
	$route['treatplan/(:num)'] = 'Treatplan/index/$1';
#nedregion

/*Paciente*/
	$route['patient/list'] = 'Paciente/index';
	$route['patient/perfil/(:num)'] = 'Paciente/perfil/$1';
	$route['patient/deals'] = 'Paciente/ofertas';
	$route['patient/deals/lost'] = 'Paciente/ofertaslost';
	$route['patient/account'] = 'Paciente/cuenta';
	$route['patient/account/pdf'] = 'Paciente/pdf_tp_account';
	$route['patient/details/(:num)'] = 'Paciente/detalle/$1';

/*CUESTIONARIOS*/
	$route['survey/list'] = 'Cuestionarios/index';
	$route['survey/questions'] = 'Cuestionarios/preguntas';
	$route['survey/(:num)'] = 'Cuestionarios/responder/$1';

/*Oferta*/
	$route['deals/details/(:num)/(:num)'] = 'Oferta/index/$1/$2';
/*reportes*/
	$route['reports/procedure/reminders'] = 'Reportes/recordatorios_ofertas_procedimientos';
	$route['reports/activities'] = 'Reportes/actividades';
	$route['reports/deals'] = 'Reportes/ventas';
	$route['reports/others'] = 'Reportes/index';

/*CONFIGURACION*/
	$route['config'] = 'Configuracion/index';
	$route['config/notifications'] = 'Configuracion/notifications';

/*Catalogos*/
	$route['data/stage'] = 'catalogos/Etapas/index';
	$route['data/files'] = 'catalogos/Archivos/index';
	$route['data/providers'] = 'catalogos/Provider/index';
	$route['data/procedures'] = 'catalogos/Procedures/index';
	$route['data/procedures/buttonquick'] = 'catalogos/Procedures/quickbuttons';
	$route['data/fee'] = 'catalogos/Precios/index';
	$route['data/origin'] = 'catalogos/Origen/index';