	<style>
		.testTrunc.conTextTruncado {overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }
		.testTrunc {width: 500px; }
	</style>
	
	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-cog"></i></span>
		<div class="title">
			<h2>Configuration</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Configuration</li>
				<li class="active">Notifications</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>List</h2>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtconfig" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Instructions</th>
									<th>User type</th>
									<th>Edit</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_edit"  autocomplete="off" action="Api_data/confignoti" method="put">
			<input type="hidden" name="configid">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Time</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-12 control-label">Name</label>
					<div class="col-md-12">
						<input type="text" name="name" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 control-label">Description</label>
					<div class="col-md-12">
						<textarea name="description" class="form-control" disabled></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 control-label">Configuration</label>
					<div class="col-md-12">
						<input type="text" name="configuration" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 control-label">User type</label>
					<div class="col-md-12">
						<select name="type[]" multiple class="bs-select" data-live-search="true" data-style="btn-sm btn btn-default"></select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
