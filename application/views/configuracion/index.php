	<style>
		.testTrunc.conTextTruncado {overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }
		.testTrunc {width: 500px; }
	</style>
	
	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-cog"></i></span>
		<div class="title">
			<h2>Configuration</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Configuration</li>
				<li class="active">system</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>List</h2>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtconfig" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Config name</th>
									<th>Description</th>
									<th>Instructions</th>
									<th>Edit</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_config_timeclinic" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning modal-sm" role="document">
		<div class="modal-content">
		<form id="form_config_timeclinic"  autocomplete="off" action="Api_data/config" method="patch">
			<input type="hidden" name="configid" value="3">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Time</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-12 control-label">Hour Start</label>
					<div class="col-md-12">
						<input name="starthour" type="text" class="form-control input-sm timepicke mask" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 control-label">Hour finish</label>
					<div class="col-md-12">
						<input name="finishhour" type="text" class="form-control input-sm timepicke mask" data-validation="required">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_config" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning modal-sm" role="document">
		<div class="modal-content">
		<form id="form_config"  autocomplete="off" action="Api_data/config" method="patch">
			<input type="hidden" name="configid">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Update configuration</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div id="bodyconfig" class="modal-body"></div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_config_usuarios" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_config_users"  autocomplete="off" action="Api_usuario/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Select Users</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label">Report</label>
					<div class="col-md-12">
						<input name="reporte" class="form-control" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label">Users</label>
					<div class="col-md-12">
						<select name="emails" class="multiselect form-control" multiple="multiple" data-live-search="true" data-validation="required"></select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
