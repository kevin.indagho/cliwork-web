	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-list-ol"></i></span>
		<div class="title">
			<h2>Fee</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Data</li>
				<li class="active">Fee</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title"><h2>List</h2></div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtfee" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Category</th>
									<th>Procedure</th>
									<th>Amount</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_addfee" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addfee" autocomplete="off" class="form-horizontal" action="Api_data/precios" method="POST">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add Procedure</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Category</label>
					<div class="col-md-8">
						<select name="category" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure</label>
					<div class="col-md-8">
						<select name="procedure" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Amount</label>
					<div class="col-md-8">
						<input name="amount" class="form-control input-sm" data-validation="number" data-validation="required" data-validation-ignore=".">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editfee" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editfee" autocomplete="off" class="form-horizontal" action="Api_data/precios" method="PUT">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Procedure</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Category</label>
					<div class="col-md-8">
						<input name="category" type="text" class="form-control input-sm" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure</label>
					<div class="col-md-8">
						<textarea name="amount" class="form-control" rows="5" disabled></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Amount</label>
					<div class="col-md-8">
						<input name="amount" class="form-control input-sm" type="text" data-validation="number" data-validation="required" data-validation-ignore=".">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        