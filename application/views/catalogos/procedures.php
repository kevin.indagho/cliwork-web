	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-list-ol"></i></span>
		<div class="title">
			<h2>Procedures</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Data</li>
				<li class="active">Procedures</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title"><h2>List</h2></div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtprocedures" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Code</th>
									<th>Abbr</th>
									<th>Time</th>
									<th>Descript</th>
									<th>Surf</th>
									<th>Crown</th>
									<th>Implant</th>
									<th>Status</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_addproce" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addproc" autocomplete="off" class="form-horizontal" action="Api_data/procedure" method="POST">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add Procedure</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Code</label>
					<div class="col-md-8">
						<input name="code" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Abbr</label>
					<div class="col-md-8">
						<input name="abbr" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Time <small class="text-muted">(minutes)</small></label>
					<div class="col-md-8">
						<input name="duration" class="form-control input-sm" type="text" data-validation="number" data-validation="required" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Descript</label>
					<div class="col-md-8">
						<textarea name="descript" type="text" class="form-control" rows="5" data-validation="required"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="col-md-12 control-label padding-0">Is Quarter</label>
					</div>
					<div class="col-md-8">
						<div class="col-md-4 padding-0">
							<input type="hidden" name="isquarter" checked="" value="0">
							<label class="switch switch-sm switch-cube margin-0">
								<input type="checkbox" name="isquarter" value="1">
							</label>
						</div>
						<div class="col-md-8 padding-0">
							<select name="toothrange" class="form-control">
								<option></option>
								<option value="1-8">1-8</option>
								<option value="9-16">9-16</option>
								<option value="17-24">17-24</option>
								<option value="25-32">25-32</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input type="hidden" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Is Surf</label>
					<div class="col-md-8">
						<input type="hidden" name="issurf" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="issurf" value="1">
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Is Crown</label>
					<div class="col-md-8">
						<input type="hidden" name="iscrown" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="iscrown" value="1">
						</label>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-md-4 control-label">Is Implant</label>
					<div class="col-md-8">
						<input type="hidden" name="isimplant" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="isimplant" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editproce" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editproc" autocomplete="off" class="form-horizontal" action="Api_data/procedure" method="PUT">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Procedure</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Code</label>
					<div class="col-md-8">
						<input name="code" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Abbr</label>
					<div class="col-md-8">
						<input name="abbr" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Time <small class="text-muted">(minutes)</small></label>
					<div class="col-md-8">
						<input name="duration" class="form-control input-sm" type="text" data-validation="number" data-validation="required" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Descript</label>
					<div class="col-md-8">
						<textarea name="descript" type="text" class="form-control" rows="5" data-validation="required"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="col-md-12 control-label padding-0">Is Quarter</label>
					</div>
					<div class="col-md-8">
						<div class="col-md-4 padding-0">
							<input type="hidden" name="isquarter" checked="" value="0">
							<label class="switch switch-sm switch-cube margin-0">
								<input type="checkbox" name="isquarter" value="1">
							</label>
						</div>
						<div class="col-md-8 padding-0">
							<select name="toothrange" class="form-control">
								<option></option>
								<option value="1-8">1-8</option>
								<option value="9-16">9-16</option>
								<option value="17-24">17-24</option>
								<option value="25-32">25-32</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input type="hidden" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Is Surf</label>
					<div class="col-md-8">
						<input type="hidden" name="issurf" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="issurf" value="1">
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Is Crown</label>
					<div class="col-md-8">
						<input type="hidden" name="iscrown" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="iscrown" value="1">
						</label>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-md-4 control-label">Is Implant</label>
					<div class="col-md-8">
						<input type="hidden" name="isimplant" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="isimplant" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        