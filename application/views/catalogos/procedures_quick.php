	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-list-ol"></i></span>
		<div class="title">
			<h2>Procedures buttons quick</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Data</li>
				<li class="active">Procedures buttons quick</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title"><h2>List</h2></div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtproceduresbuttons" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Category</th>
									<th>Procedure</th>
									<th>Description</th>
									<th>Surf</th>
									<th>Status</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_editprocbtn" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editprocbtn" autocomplete="off" class="form-horizontal" action="Api_data/procbtnquick" method="PUT">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add quick button</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Category</label>
					<div class="col-md-8">
						<select class="bs-select" name="category" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">New Procedure</label>
					<div class="col-md-8">
						<select class="bs-select" name="procedurenew" data-live-search="true" data-style="btn-sm btn btn-default"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Current Procedure</label>
					<div class="col-md-8">
						<input name="currentprocedure" type="text" class="form-control input-sm" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Surf</label>
					<div class="col-md-8">
						<input name="surf" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Description</label>
					<div class="col-md-8">
						<input name="descript" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input type="hidden" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_addprocbtn" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addprocbtn" autocomplete="off" class="form-horizontal" action="Api_data/procbtnquick" method="post">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add quick button</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Category</label>
					<div class="col-md-8">
						<select class="bs-select" name="category" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default">
							<option></option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">New Procedure</label>
					<div class="col-md-8">
						<select class="bs-select" name="procedurenew" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Surf</label>
					<div class="col-md-8">
						<input name="surf" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Description</label>
					<div class="col-md-8">
						<input name="descript" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input type="hidden" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>