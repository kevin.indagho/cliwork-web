	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-list-ol"></i></span>
		<div class="title">
			<h2>Providers</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Data</li>
				<li class="active">Providers</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="block-content">
						<table id="dtproviders" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Abbr</th>
									<th>Fisrt Name</th>
									<th>Last Name</th>
									<th>Hour abailable</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_newprovider" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newprov"  autocomplete="off" class="form-horizontal" action="Api_data/providers" method="POST">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add new provider</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">First name</label>
					<div class="col-md-8">
						<input name="fname" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Last name</label>
					<div class="col-md-8">
						<input name="lname" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Last name</label>
					<div class="col-md-8">
						<select data-style="btn-sm btn btn-default" name="specialty" multiple class="form-control bs-select show-tick">
							<option value="265">General (Doctor)</option>
							<option value="266">Hygienist</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Abbr</label>
					<div class="col-md-8">
						<input name="Abbr" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Horario</label>
					<div class="col-md-4">
						<input name="hourstart" type="text" class="form-control input-sm bs-timepicker" data-validation="required">
					</div>
					<div class="col-md-4">
						<input name="hourend" type="text" class="form-control input-sm bs-timepicker" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Color</label>
					<div class="col-md-8">
					<div class="input-group input-group-sm">
						<span class="input-group-addon"><i style="color:#FFFFFF;" class="fa fa-square" aria-hidden="true"></i></span>
						<input name="color" type="text" class="form-control input-sm bs-colorpicker">
					</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editprovider" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editprov" autocomplete="off" class="form-horizontal" action="Api_data/providers" method="PUT">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Provider</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">First name</label>
					<div class="col-md-8">
						<input name="fname" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Last name</label>
					<div class="col-md-8">
						<input name="lname" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Abbr</label>
					<div class="col-md-8">
						<input name="Abbr" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Specialty</label>
					<div class="col-md-8">
						<select data-style="btn-sm btn btn-default" name="specialty" class="form-control bs-select show-tick" data-validation="required">
							<option></option>
							<option value="265">General (Doctor)</option>
							<option value="266">Hygienist</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Horario</label>
					<div class="col-md-4">
						<input name="hourstart" type="text" class="form-control input-sm bs-timepicker" data-validation="required">
					</div>
					<div class="col-md-4">
						<input name="hourend" type="text" class="form-control input-sm bs-timepicker" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Color</label>
					<div class="col-md-8">
					<div class="input-group input-group-sm">
						<span class="input-group-addon"><i id="squarecolor" style="color:#FFFFFF;" class="fa fa-square" aria-hidden="true"></i></span>
						<input name="color" type="text" class="form-control input-sm bs-colorpicker">
					</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Is Hidden</label>
					<div class="col-md-8">
						<input type="hidden" name="status" value="1">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="0">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        