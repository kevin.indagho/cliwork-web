	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-list-ol"></i></span>
		<div class="title">
			<h2>Stage</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Data</li>
				<li class="active">Stage</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>List</h2>
							<p><button class="btn btn-success btn-xs btn-icon-fixed" data-backdrop="static" data-toggle="modal" data-target="#modal_newstage">
								<span class="fa fa-plus"></span>
							New Stage</button></p>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtstage" width="100%" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Days</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_newstage" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newstage"  autocomplete="off" class="form-horizontal" action="Api_data/etapas" method="POST">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add Stage</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input name="name" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Days</label>
					<div class="col-md-8">
						<input name="days" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editstage" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editstage" autocomplete="off" class="form-horizontal" action="Api_data/etapas" method="PUT">
			<input name="id" type="hidden" data-validation="required">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Stage</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input name="name" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Days</label>
					<div class="col-md-8">
						<input name="days" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="status" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="status" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        