<style>
	.datanavbar{
		color: white;
		line-height: 32px;
		display: inline-block;
		border-right: solid 1px #fff;
		padding-right: 5px;
		margin-right: 5px;
	}
</style>
	<div class="app-heading padding-5" style="background-color: #20252a;">
		<div class="col-md-6 text-right">
			<span class="datanavbar">259-563-78-23</span>
			<span class="datanavbar">Misión de San Diego 2993, Zona centro Tijuana BC, México</span>
			<span class="datanavbar">midireccion@dominio.com</span>
		</div>
		<div class="col-xl-5 col-lg-6 col-md-8 col-sm-12 col-8 text-right">
			<a href="http://facebook.com/" class="btn btn-default btn-xs"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="https://twitter.com/" class="btn btn-default btn-xs"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<a href="https://www.instagram.com/" class="btn btn-default btn-xs"><i class="fa fa-instagram" aria-hidden="true"></i></a>
			<a href="https://plus.google.com/" class="btn btn-default btn-xs"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
			<a href="https://www.linkedin.com/" class="btn btn-default btn-xs"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
			<a href="https://www.youtube.com/" class="btn btn-default btn-xs"><i class="fa fa-youtube" aria-hidden="true"></i></a>
			<a href="https://www.pinterest.com/" class="btn btn-default btn-xs"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
			<a href="#" style="background-color:#c25038; border-color:#c25038;" class="btn btn-danger wpm-btn-radio-cero btn-cab-boca" style="margin-left: 30px;" data-toggle="modal" data-target="#exampleModalCenter">PLAN YOUR TREATMENT</a>
		</div>
	</div>
	<div class="app-heading text-center padding-5" style="background-color:white;">
		<div>
			<div class="row">
				<div class="col-4 contenedor-wpm-logo-menu-movil">
					<a href="http://webmarketingnetworks.com/updated"><img style="height:68px;" src="http://webmarketingnetworks.com/updated/wp-content/themes/smile4ever/img/logo.png" alt="Smile 4 Ever Mexico – The Best Dentists in Tijuana Mexico"></a>    
				</div>
			</div>
		</div>
	</div>
	
	<div class="container margin-0" style="background-color:white;">
		<div class="col-sm-12 col-md-8 col-sm-push-0 col-md-push-2">
			<div class="block padding-0" style="border:none;">
				<h1 class="text-center">Answer a few questions to see if Smile4Ever migth be right for you</h1>
				<form id="formulario" class="form-horizontal" action="Api_data/cuestionario" method="post">
				<input type="hidden" id="id" name="procedure" value="<?=$proceid?>">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label">First Name</label>
						<div class="col-md-8">
							<input name="fname" type="text" class="form-control input-sm" data-validation="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Last Name</label>
						<div class="col-md-8">
							<input name="lname" type="text" class="form-control input-sm">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Birthdate</label>
						<div class="col-md-8">
							<input name="birthdate" type="text" class="form-control input-sm">
						</div>
					</div>
				</div>
				<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label">Cell</label>
							<div class="col-md-8">
								<input name="cell" type="text" class="form-control input-sm" data-validation="number" data-validation-ignore="*+#-: " data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<input name="phone" type="text" class="form-control input-sm" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
								<input name="email" type="text" class="form-control input-sm" data-validation="email" data-validation="required">
							</div>
						</div>
					</div>
				<div class="col-md-12">                                
					<h1 class="text-center heading-line-below margin-top-20">Questions</h1>
					<div id="email-error-dialog"></div>
				</div>
				<div id="formulario_contenido"></div>
				<div class="form-group margin-bottom-20 text-center">
					<button style="background-color:#c25038; border-color:#c25038;" class="btn btn-danger" type="submit">Save information</button>
				</div>
				</form>
			</div>
		</div>
	</div>	
</div>