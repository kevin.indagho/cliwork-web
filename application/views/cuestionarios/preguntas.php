	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Questions</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Survey</li>                                                     
				<li class="active">Questions</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>
					</div>
					<div class="block-content">
						<table id="dtquestions" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Procedure</th>
									<th>Question</th>
									<th>Description</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_addquestion" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addquestion"  autocomplete="off" class="form-horizontal" action="Api_data/precedure_preguntas" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Add question</h4>
			</div>
			<div class="modal-body">
			<div class="form-group">
					<label class="col-md-4 control-label">Procedure</label>
					<div class="col-md-8">
						<select class="bs-select" name="procedure" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input type="text" name="name" class="form-control input-sm">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Description</label>
					<div class="col-md-8">
						<textarea rows="5" name="description" class="form-control"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select class="bs-select" name="type" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default">
							<option value="1">Input</option>
							<option value="2">Checkbox</option>
							<option value="3">Radio button</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Active</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="active"  value="0">
						<label class="switch switch-cube">
							<input type="checkbox" name="active" value="1" checked="checked">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editquestion" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editquestion"  autocomplete="off" class="form-horizontal" action="Api_data/precedure_preguntas" method="put">
		<input type="hidden" name="id">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Edit question</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure</label>
					<div class="col-md-8">
						<input type="text" name="procedure" class="form-control input-sm" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input type="text" name="name" class="form-control input-sm">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Description</label>
					<div class="col-md-8">
						<textarea rows="5" name="description" class="form-control"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Active</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="active"  value="0">
						<label class="switch switch-cube">
							<input type="checkbox" name="active" value="1" checked="checked">
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select class="bs-select" name="type" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default">
							<option value="1">Input</option>
							<option value="2">Checkbox</option>
							<option value="3">Radio button</option>
						</select>
					</div>
				</div>
				<div id="divoptions" class="form-group">
					<label class="col-md-4 control-label">Options</label>
					<div class="col-md-8">
						<div class="col-md-12 padding-0 margin-bottom-5">
							<div class="input-group">
                                <span class="input-group-btn">
									<button id="btnAddOption" class="btn btn-success btn-sm margin-0" type="button"><i class="fa fa-plus"></i></button>
								</span>
                                <input name="newOption" type="text" class="form-control input-sm" placeholder="new option">
                            </div>
						</div>
						<div class="col-md-12 padding-0 scroll" style="height:200px;">
							<ul id="listOptions" class="list-group"></ul>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>