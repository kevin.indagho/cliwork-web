	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Survey</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li class="active">Survey</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="app-widget-tile app-widget-tile-info">                                            
					<div id="totalInformer" class="intval intval-lg odometer">0</div>
					<div class="line">
						<div class="title wide text-center">Libres</div>                                                
					</div>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>Cuestionarios</h2>
						</div>
						<div class="heading-elements">
							<select id="dtfilter" class="form-control input-sm">
								<option value="1">Me</option>
								<option value="0">All</option>
							</select>
						</div>
					</div>
					<div class="block-content">
					<button class="btn btn-default btn-xs" data-toggle="modal" data-target="#modal_surveylist"><span class="fa fa-list-ol"></span> surveys links</button>
					<!--<a id="beep" href="http://localhost:8080/clienteopendental/assets/noty.mp3">Play Song</a>-->
						<table id="dtcuest" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Date</th>
									<th>Doctor</th>
									<th>Sales advisor</th>
									<th>Procedure</th>
									<th>Fisrt Name</th>
									<th>Last Name</th>
									<th>status</th>
									<th>Last Note</th>
									<th>Files</th>
									<th>Open</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_details" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-info modal-lg" role="document">
		<div class="modal-content">
		<form id="form_details"  autocomplete="off" onsubmit="return false;" class="form-horizontal"  action="Api_data/cuestionario" method="put">
			<input name="id" type="hidden">
			<input name="patientid" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Details</h4>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabs-7" data-toggle="tab">Data</a></li>
					<li><a href="#tabs-8" data-toggle="tab">Notes</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabs-7">
						<fieldset>
							<div id="errormsg"></div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">First Name</label>
									<div class="col-md-8">
										<input name="fname" type="text" class="form-control input-sm" data-validation="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Last Name</label>
									<div class="col-md-8">
										<input name="lname" type="text" class="form-control input-sm" data-validation="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Birthdate</label>
									<div class="col-md-8">
										<input name="birthdate" type="text" class="form-control input-sm">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label">Cell</label>
									<div class="col-md-8">
										<input name="cell" type="text" class="form-control input-sm" data-validation="number" data-validation-ignore="*+#-: " data-validation-optional-if-answered="email,phone">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Home Phone</label>
									<div class="col-md-8">
										<input name="phone" type="text" class="form-control input-sm" data-validation="number" data-validation-ignore="*+#-: " data-validation-optional-if-answered="email,cell">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label">Email</label>
									<div class="col-md-8">
										<input name="email" type="text" class="form-control input-sm" data-validation="email" data-validation-optional-if-answered="cell,phone">
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset class="scrollCustom" style="height:300px;">
							<div id="filegallery" class="col-md-12"></div>
							<div id="formulario_contenido" class="col-md-12" ></div>
						</fieldset>
					</div>
					<div class="tab-pane" id="tabs-8">
						<div class="form-group">
							<label class="col-md-4 control-label">Status</label>
							<div class="col-md-8">
								<div class="app-radio inline"> 
									<label><input type="radio" name="status" value="0">Pending</label> 
								</div>
								<div class="app-radio success inline"> 
									<label><input type="radio" name="status" value="1">Approved</label> 
								</div>
								<div class="app-radio warning inline"> 
									<label><input type="radio" name="status" value="2">Checking</label> 
								</div>
								<div class="app-radio danger inline"> 
									<label><input type="radio" name="status" value="3">Cancelled</label> 
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Notes</label>
							<div class="col-md-8">
								<span class="help-block">500 Characters</span>
								<textarea name="notes" class="form-control" rows="5" maxlength="500"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div id="timelineList" class="col-md-12" style="height:300px;">
								<div class="app-timeline"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_files" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">
	<div class="modal-dialog modal-info modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Upload Images</h4>
			</div>
			<div class="modal-body">
				<form id="formaddfile" class="dropzone" enctype="multipart/form-data"></form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_surveylist" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">
	<div class="modal-dialog modal-info modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Procedure survey list</h4>
			</div>
			<div class="modal-body">
				<table id="dt_procesurvey" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Procedure</th>
									<th>Link</th>
								</tr>
							</thead> 
						</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>            
</div>

<div class="modal fade" id="preview" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-cross"></span></button>
		<div class="modal-content">
			<div class="modal-body padding-5"></div>
		</div>
	</div>            
</div>