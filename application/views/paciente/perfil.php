	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Patients Perfil</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Patients</li>
				<li class="active">Perfil</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="panel panel-default" id="panel-toggle">
					<div class="panel-body">
						<input type="hidden" id="patientid" value="<?=$patient?>">
						<div class="col-md-2 text-center">
							<img id="ProfileImage" style="width:150px; padding:1px; border:1px solid #021a40;" src="<?=base_url()?>img/user/no-image.png"></img>
							<br>
							<b id="FullName"></b><br>
							<b id="LastAppt"><i class="fa fa-calendar"></i> Last Appt: No available</b><br>
							<b id="NextAppt"><i class="fa fa-calendar"></i> Next Appt: No available</b><br>
							<br>
						</div>
						<div class="col-md-10">
							<div>
								<ul class="nav nav-tabs">
									<li class="active"><a href="#patinfo" data-toggle="tab">Patient</a></li>
									<li><a href="#pattp" data-toggle="tab">Treat Plan</a></li>
									<li><a href="#documents" data-toggle="tab">Documents</a></li>
									<li><a href="#playmentplan" data-toggle="tab">Payment plan</a></li>
									<li><a href="#playments" data-toggle="tab">Payments</a></li>
								</ul>
								<div class="tab-content tab-content-bordered">
									<div class="tab-pane active" id="patinfo">
										<fieldset class="col-md-6">
											<div class="form-group">
												<label class="col-md-2 control-label">First Name</label>
												<div class="col-md-8">
													<input id="firstname" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Last Name</label>
												<div class="col-md-8">
													<input id="lastname" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Gender</label>
												<div class="col-md-8">
													<input id="gender" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Stage</label>
												<div class="col-md-8">
													<input id="stage" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
										</fieldset>
										<fieldset class="col-md-6">
											<div class="form-group">
												<label class="col-md-2 control-label">Email</label>
												<div class="col-md-8">
													<input id="email" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Home phone</label>
												<div class="col-md-8">
													<input id="hphone" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Work phone</label>
												<div class="col-md-8">
													<input id="wphone" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">Mobile phone</label>
												<div class="col-md-8">
													<input id="mphone" type="text" class="form-control input-sm" disabled />
												</div>
											</div>
										</fieldset>
									</div>
									<div class="tab-pane" id="pattp">
										<table id="dt_treatplans" class="table table-bordered table-head-custom" width="100%">
											<thead>
												<tr>
													<th>Date</th>
													<th>Name</th>
													<th>Subtotal</th>
													<th>Adjusment</th>
													<th>Total</th>
													<th>Status</th>
													<th>Complete</th>
													<th>Procedures</th>
													<th>PDF</th>
												</tr>
											</thead>
										</table>
									</div>
									<div class="tab-pane" id="documents">
										<table id="dt_documents" class="table table-bordered table-head-custom" width="100%">
											<thead>
												<tr>
													<th>Date</th>
													<th>User</th>
													<th>Name</th>
													<th></th>
												</tr>
											</thead>
										</table>
									</div>
									<div class="tab-pane" id="playmentplan">
										<table id="dt_paymentplan" class="table table-bordered table-head-custom" width="100%">
											<thead>
												<tr>
													<th>Date</th>
													<th>User</th>
													<th>Comision</th>
													<th>Deuda</th>
													<th>Status</th>
													<th>OPEN</th>
													<th>PDF</th>
												</tr>
											</thead>
										</table>
									</div>
									<div class="tab-pane" id="playments">
										<table id="dt_payments" class="table table-bordered table-head-custom" width="100%">
											<thead>
												<tr>
													<th>Date</th>
													<th>Provider</th>
													<th>Type</th>
													<th>Amount</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>                                    
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<ul id="widgetAppt" class="app-feature-gallery app-feature-gallery-noshadow margin-0" style="height: 132px;">
					<li id="aptdisplay">
						<div class="app-widget-tile">
							<div class="row">
								<div class="col-sm-4">
									<div class="icon icon-lg">
										<span class="fa-stack fa-lg">
											<i class="fa fa-calendar fa-stack-1x"></i>
											<i class="fa fa-ban fa-stack-2x text-danger"></i>
										</span>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="line"><div class="title"></div></div>
									<div class="intval text-left">No Appointment available</div>
								</div>
							</div>
							<div class="line" style="height:250px; overflow:auto;"></div>
						</div>
					</li>
				</ul>
			</div>
			<!--<div class="col-md-3">
				<div class="col-md-6">
					<div class="app-widget-tile">                                                                                    
						<div id="noteCount" class="intval odometer intval-lg">0</div>
						<div class="line">
							<div class="title wide text-center">Notes</div>                                                
						</div>
						<div class="line">
							<div id="openNotes" class="subtitle"><a href="#">View all</a></div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="app-widget-tile">                                                                                    
						<div id="visitsCount" class="intval odometer intval-lg">0</div>
						<div class="line">
							<div class="title wide text-center">Visits</div>                                                
						</div>
						<div class="line">
							<div id="openVisits" class="subtitle"><a href="#"></a></div>                                            
						</div>
					</div>
				</div>
			</div>-->
			<div class="col-md-9">
				<div class="block" id="block-account">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>Procedure list</h2>
							<p id="accTpName"></p>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-account');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content" style="overflow: auto; max-height: 400px;">
						<table id="dt_account" class="table table-bordered table-head-custom" width="100%">
							<thead>
								<tr>
									<th style="width:100px;">Date</th>
									<th>Doctor</th>
									<th>Description</th>
									<th style="width: 20px;">Tooth</th>
									<th style="width: 50px;">Amount</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<div id="filter_log" class="btn-group" role="group">
					<button data-type="all" type="button" class="btn btn-default btn-xs">All</button>
					<button data-type="2" type="button" class="btn btn-default btn-xs">Activities</button>
					<button data-type="1" type="button" class="btn btn-default btn-xs">Notes</button>
					<button data-type="3" type="button" class="btn btn-default btn-xs">Files</button>
					<button data-type="4" type="button" class="btn btn-default btn-xs">Email</button>
					<button type="button" class="btn btn-default btn-xs">ChangeLog</button>
				</div>                                    
			</div>
			<div class="col-md-12 scrollCustom" style="height:500px;">
				<div id="activitiestimeline" class="app-timeline"></div>
			</div>
		</div>
	</div>
</div>

	<div class="modal fade" id="preview" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-cross"></span></button>
			
			<div class="modal-content">
				<div class="modal-body padding-5"></div>
			</div>
		</div>            
	</div>
			
	<div class="modal fade" id="modal_newtreatplan" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-success" role="document">
			<div class="modal-content">
			<form id="form_newtreatplan"  autocomplete="off" class="form-horizontal" action="Api_treatplan/treatplan" method="post">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">Treat plan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-4 control-label">Name</label>
						<div class="col-md-8">
							<input name="name" type="text" class="form-control input-sm" data-validation="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Notes</label>
						<div class="col-md-8">
							<textarea name="notes" rows="5" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Status</label>
						<div class="col-md-8">
							<input style="display:none;" type="checkbox" name="status" checked="" value="0">
							<label class="switch switch-sm switch-cube">
								<input type="checkbox" name="status" checked="" value="1">
							</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</form>
			</div>
		</div>            
	</div>

	<div class="modal fade" id="modal_newdocument" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-success" role="document">
			<div class="modal-content">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">New document</h4>
				</div>
				<div class="modal-body">
					<h5>Allowed </h5>
					<p><b>Size:</b> 2MB. <b>Types:</b> JPG | PNG | PDF</p>
					<form id="formaddfile" class="dropzone" enctype="multipart/form-data"></form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>            
	</div>
	
	<div class="modal fade" id="modal_edittreatplan" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-warning" role="document">
			<div class="modal-content">
			<form id="form_edittreatplan"  autocomplete="off" class="form-horizontal" action="Api_treatplan/treatplan" method="put">
				<input name="id" type="hidden">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">Edit treatment plan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-4 control-label">Name</label>
						<div class="col-md-8">
							<input name="name" type="text" class="form-control input-sm" data-validation="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Notes</label>
						<div class="col-md-8">
							<textarea name="notes" rows="5" class="form-control" data-validation="required"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Complete</label>
						<div class="col-md-8">
							<input style="display:none;" type="checkbox" name="complete" checked="" value="0">
							<label class="switch switch-sm switch-cube">
								<input type="checkbox" name="complete" value="1">
							</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Status</label>
						<div class="col-md-8">
							<input style="display:none;" type="checkbox" name="status" checked="" value="0">
							<label class="switch switch-sm switch-cube">
								<input type="checkbox" name="status" value="1">
							</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</form>
			</div>
		</div>            
	</div>

	<div class="modal fade" id="modal_notes" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-info modal-lg" role="document">
			<div class="modal-content">
			<form id="form_edittreatplan"  autocomplete="off" class="form-horizontal" action="Api_treatplan/treatplan" method="put">
				<input name="id" type="hidden">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">View all notes</h4>
				</div>
				<div class="modal-body">
					<table id="dt_notes" class="table table-bordered table-head-custom" width="100%">
						<thead>
							<tr>
								<th>Date</th>
								<th>User</th>
								<th>Notes</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>            
	</div>

	<div class="modal fade" id="modal_payplanedit" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-info" role="document">
			<div class="modal-content">
			<form id="form_editpaymentplan"  autocomplete="off" class="form-horizontal" action="Api_account/paymentplan" method="post">
			<input name="patientid" type="hidden">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">Payment plan</h4>
				</div>
				<div class="modal-body">
					<div id="errormsg"></div>
					<div class="form-group">
								<label class="col-md-4 control-label">Date</label>
								<div class="col-md-8">
									<input name="date" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Value</label>
						<div class="col-md-8">
							<input name="value" type="text" class="form-control input-sm" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Duration</label>
						<div class="col-md-4">
							<input name="quantity" type="text" class="form-control input-sm" disabled>
						</div>
						<div class="col-md-4">
							<select name="type" type="text" class="form-control input-sm bs-select"  data-style="btn-sm btn btn-default margin-0" disabled>
								<option value="1">Mounts</option>
								<option value="2">Weeks</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Pay</label>
						<div class="col-md-3">
							<input name="pay" type="text" class="form-control input-sm" disabled>
						</div>
						<div class="col-md-2">
							<input name="profit" type="text" class="form-control input-sm" disabled>
						</div>
						<div class="col-md-3">
							<input name="paytotal" type="text" class="form-control input-sm" disabled>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<table id="tbl_dates_edit" class="table table-bordered table-head-custom">
								<thead>
									<tr>
										<th>Date</th>
										<th>Pay</th>
										<th>Pagado</th>
									</tr>
								</thead>
								<tbody></tbody>
								<tfoot></tfoot>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</form>
			</div>
		</div>            
	</div>
        