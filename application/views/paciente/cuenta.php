	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Patients</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Patients</li>
				<li class="active">List</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block block-condensed">
					<div class="block-content">
						<form id="form_newappointment"  autocomplete="off" class="form-horizontal" action="Api_cita/citas" method="post">
							<div class="form-group">
								<div class="col-md-3">
									<label>Search patient</label>
									<select id="patientsearch" class="form-control bs-selec" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
								</div>
								<!--<div class="col-md-3">
									<label>Select treatplan</label>
									<select id="treatplan" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
								</div>-->
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Sub Total</div>
						<div class="title pull-right"></div>
					</div>              
					<div id="allSubTotal" class="intval odometer">0.00</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Additions</div>
						<div class="title pull-right"></div>
					</div>              
					<div id="allAdditions" class="intval odometer">0.00</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">substractions</div>
						<div class="title pull-right"></div>
					</div>              
					<div id="allSubstractions" class="intval odometer">0.00</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Total</div>
						<div class="title pull-right"></div>
					</div>              
					<div id="allTotal" class="intval odometer">0.00</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Payments</div>
						<div id="allPaymentsPercent" class="title pull-right">0%</div>
					</div>              
					<div id="allPayments" class="intval odometer">0.00</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Balance</div>
						<div id="allDuePercent" class="title pull-right">0%</div>
					</div>              
					<div id="allDue" class="intval odometer">0.00</div>
				</div>
			</div>
			<!--<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Total</div>
						<div class="title pull-right"></div>
					</div>              
					<div id="tptotal" class="intval odometer">0.00</div>
					<div class="line">
						<div class="selecttreatplan subtitle">Select treatplan</div>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Payments</div>
						<div id="paymentspercent" class="title pull-right">0%</div>
					</div>              
					<div id="tppayments" class="intval odometer">0.00</div>
					<div class="line">
						<div class="selecttreatplan subtitle">Select treatplan</div>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="app-widget-tile">
					<div class="line">
						<div class="title">Due</div>
						<div id="duepercent" class="title pull-right">0 %</div>
					</div>              
					<div id="tpdue" class="intval odometer">0.00</div>
					<div class="line">
						<div class="selecttreatplan subtitle">Select treatplan</div>
					</div>
				</div>
			</div>-->
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-account">
					<div class="block-content">
						<div class="col">
							<div class="btn-toolbar" role="toolbar">
								<div class="btn-group" role="group">
									<button disabled id="btnPayment" type="button" class="btn btn-default btn-icon-fixed btn-sm"><span class="fa fa-usd"></span> Payment</button>
								</div>
								<div class="btn-group" role="group">
									<button disabled id="btnAdjustment"  data-toggle="modal" data-target="#modal_adjustmen" type="button" class="btn btn-default btn-icon-fixed btn-sm"><span class="fa fa-balance-scale"></span> Adjustment</button>
								</div>
								<div id="divbtnplayplan" class="btn-group" role="group">
									<button disabled id="btnPayPlan"  type="button" class="btn btn-default btn-icon-fixed btn-sm"><span class="fa fa-calendar"></span> Payment Plan</button>
								</div>
								<div id="divbtnplayplanview" class="btn-group" role="group" style="display:none;">
									<button id="btnPayPlanView" type="button" class="btn btn-warning btn-icon-fixed btn-sm"><span class="fa fa-eye"></span> Payment Plan Active</button>
								</div>
								<div class="btn-group pull-right" role="group">
									<button onclick="donwload();" type="button" class="btn btn-default btn-icon btn-sm"><span class="fa fa-print"></span></button>
									<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-account');"><span class="fa fa-expand"></span></button>
								</div>
							</div>
						</div>
						<div id="printaccount">
						<table id="dt_account" width="100%" class="margin-top-5 table table-bordered table-head-custom">
							<thead>
								<tr>
									<th style="width:100px;">Date</th>
									<th>Doctor</th>
									<th>Treatplan</th>
									<th>Description</th>
									<th style="width: 20px;">Tooth</th>
									<th style="width: 50px;">Amount</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot></tfoot>
						</table>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_payment" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-success modal-lg" role="document">
		<div class="modal-content">
		<form id="form_addpayment"  autocomplete="off" class="form-horizontal" action="Api_account/payment" method="post">
		<input name="treatplan" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Add payment</h4>
			</div>
			<div class="modal-body">
				<fieldset class="col-md-6 padding-0">
					<div class="form-group">
						<label class="col-md-4 control-label">Provider</label>
						<div class="col-md-8">
							<select name="provider" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Type</label>
						<div class="col-md-8">
							<select name="type" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Amount</label>
						<div class="col-md-8">
							<input name="amount" class="form-control input-sm" type="text" data-validation="number" data-validation-ignore="." data-validation="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Documents</label>
						<div class="col-md-8">
							<input name="document" type="file" class="form-control input-sm" accept="image/png, image/jpeg" data-validation-allowing="jpg, png" data-validation="mime size" data-validation-max-size="2M">
							<span class="help-block">Only JPG|PNG Size: 2MB.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Notes</label>
						<div class="col-md-8">
							<textarea name="notes" rows="5" class="form-control"></textarea>
						</div>
					</div>
				</fieldset>
				<fieldset class="col-md-6 padding-0">
					<div class="form-group">
						<label class="col-md-4 control-label">SubTotal</label>
						<div class="col-md-8">
							<input name="subtotal" class="form-control input-sm" type="text" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Adjustment</label>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon"><i class="fa fa-plus"></i></span>
								<input name="additions" type="text" class="form-control" disabled>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm">
								<span class="input-group-addon"><i class="fa fa-minus"></i></span>
								<input name="discounts" type="text" class="form-control" disabled>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Total</label>
						<div class="col-md-8">
							<input name="total" class="form-control input-sm" type="text" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Pay</label>
						<div class="col-md-8">
							<input name="pay" class="form-control input-sm" type="text" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Due</label>
						<div class="col-md-8">
							<input name="due" class="form-control input-sm" type="text" disabled>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_payplan" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addpaymentplan"  autocomplete="off" class="form-horizontal" action="Api_account/paymentplan" method="post">
			<input name="patientid" type="hidden">
			<input name="planid" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Payment Plan</h4>
			</div>
			<div class="modal-body">
				<div id="msgUpdate" class="col-md-12">
					<div class="alert alert-warning alert-icon-block" role="alert"> 
						<div class="alert-icon">
							<span class="fa fa-exclamation-triangle"></span> 
						</div>                                        
						<strong>Danger!</strong> El plan anterior se remplazara con el nuevo.
					</div>
				</div>
				<div id="errormsg"></div>
				<div class="form-group">
					<label class="col-md-4 control-label">Patient</label>
					<div class="col-md-8">
						<input name="patient" type="text" class="form-control input-sm" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Value</label>
					<div class="col-md-8">
						<input name="value" type="text" class="form-control input-sm" readonly>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Duration</label>
					<div class="col-md-4">
						<input name="quantity" type="text" class="form-control input-sm" data-validation="required">
					</div>
					<div class="col-md-4">
						<select name="type" type="text" class="form-control input-sm bs-select" data-style="btn-sm btn btn-default margin-0" data-size="5">
							<option value="1">Mounts</option>
							<option value="2">Weeks</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Pay</label>
					<div class="col-md-3">
						<input name="pay" type="text" class="form-control input-sm" readonly>
					</div>
					<div class="col-md-2">
						<input name="profit" type="text" class="form-control input-sm" data-validation="required">
					</div>
					<div class="col-md-3">
						<input name="paytotal" type="text" class="form-control input-sm" data-validation="required" readonly>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">First pay</label>
					<div class="col-md-8">
						<input name="datestart" type="text" class="form-control input-sm bs-datepickerusa_dis_past" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<table id="tbl_dates" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Date</th>
									<th>Mount</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot></tfoot>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_payplanedit" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editpaymentplan"  autocomplete="off" class="form-horizontal" action="Api_account/paymentplan" method="post">
			<input name="planid" type="hidden">
			<input name="patientid" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Payment plan active</h4>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<div class="form-group">
							<label class="col-md-4 control-label">Date</label>
							<div class="col-md-8">
								<input name="date" type="text" class="form-control input-sm" disabled>
							</div>
						</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Value</label>
					<div class="col-md-8">
						<input name="value" type="text" class="form-control input-sm" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Duration</label>
					<div class="col-md-4">
						<input name="quantity" type="text" class="form-control input-sm" disabled>
					</div>
					<div class="col-md-4">
						<select name="type" type="text" class="form-control input-sm bs-select"  data-style="btn-sm btn btn-default margin-0" disabled>
							<option value="1">Mounts</option>
							<option value="2">Weeks</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Pay</label>
					<div class="col-md-3">
						<input name="pay" type="text" class="form-control input-sm" disabled>
					</div>
					<div class="col-md-2">
						<input name="profit" type="text" class="form-control input-sm" disabled>
					</div>
					<div class="col-md-3">
						<input name="paytotal" type="text" class="form-control input-sm" disabled>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<table id="tbl_dates_edit" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Date</th>
									<th>Pay</th>
									<th>Pagado</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot></tfoot>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button id="updateplan" type="button" class="btn btn-warning" >Update plan</button>
				<button id="completeplan" type="button" class="btn btn-success" >Complete</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editpayment" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editpayment"  autocomplete="off" onsubmit="return false;" class="form-horizontal" action="Api_account/paymentedit" method="post">
		<input type="hidden" name="id">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Edit payment</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Provider</label>
					<div class="col-md-8">
						<select name="provider" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Treat plan</label>
					<div class="col-md-8">
						<select name="treatplan" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="type" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Amount</label>
					<div class="col-md-8">
						<input name="amount" class="form-control input-sm" type="text" data-validation="number" data-validation-ignore=".," data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Notes</label>
					<div class="col-md-8">
						<textarea name="notes" rows="5" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_adjustmen" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addadjusment"  autocomplete="off" class="form-horizontal" action="Api_account/adjustment" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Add Adjustment</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Treat plan</label>
					<div class="col-md-8">
						<select name="treatplan" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="type" class="form-control bs-select" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Amount</label>
					<div class="col-md-8">
						<input name="amount" class="form-control input-sm" type="text" data-validation="number" data-validation-ignore=".," data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Notes</label>
					<div class="col-md-8">
						<textarea name="notes" rows="5" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        