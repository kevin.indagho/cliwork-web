
	
	<!--<div class="app-heading app-heading-bordered app-heading-page">
		<div class="icon icon-lg">
			<span class="icon-home"></span>
		</div>
		<div class="title">
			<h1>Users</h1>
			<p>List of users</p>
		</div>
	</div>-->
	
	<div class="app-heading-container app-heading-bordered bottom">
		<ul class="breadcrumb">
			<li><a href="<?=base_url()?>">Home</a></li>                                                     
			<li><a href="<?=base_url("patient/list")?>">Patients</a></li>
			<li class="active">Detail</li>
			
		</ul>
	</div>
	<div class="block">
		<input id="pacienteid" value="<?=$id?>" type="hidden">
		<div class="app-heading app-heading-background app-heading-light" 
		style="background: url(assets/header/header-2.jpg) center center no-repeat;">
			<div class="contact contact-rounded contact-bordered contact-xlg status-online margin-bottom-0">
				<img src="assets/images/users/user_1.jpg">
				<div class="contact-container">
					<a id="patname" href="#"></a>
					<span id="pataddress"></span>
				</div>
			</div>                        
			<div class="heading-elements">
				<a href="#" class="btn btn-danger" id="page-like"><span class="app-spinner loading"></span> loading...</a>
				<a href="https://themeforest.net/item/boooya-revolution-admin-template/17227946?ref=aqvatarius&license=regular&open_purchase_for_item_id=17227946" class="btn btn-success btn-icon-fixed"><span class="icon-text">$24</span> Purchase</a>
			</div>
		</div>
		<div class="col-md-12">
			<div class="progress">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">50%</div>
			</div>
		</div>
	</div>
	<div class="app-content-separate app-content-separated-left">
                        
		<div class="app-content-separate-left" data-separate-control-height="true">
			<div class="app-content-separate-panel padding-20 visible-mobile">
				<div class="pull-left">
					<h4 class="text-sm text-uppercase text-bolder margin-bottom-0">Visible On Mobile</h4>
					<p class="subheader">Use this panel to control this sidebar</p>
				</div>
				<button class="btn btn-default btn-icon pull-right" data-separate-toggle-panel=".app-content-separate-left"><span class="icon-menu"></span></button>
			</div>
			<div class="app-content-separate-content padding-20">
				<fieldset>
					<legend>Contact</legend>
					<b>Patient : </b> <span id="patientdeal"></span><br>
					<b>Home Phone : </b> <span id="conHmPhone"></span><br>
					<b>Work Phone : </b> <span id="conWkPhone"></span><br>
					<b>Mobile Phone : </b> <span id="conWiPhone"></span><br>
					<b>Email : </b> <span id="conEmail"></span><br>
					<b>Address</b><br><span id="conAddress"></span>
					<br>
				</fieldset>
				<fieldset>
					<legend>Details</legend>
					<dl> 
						<dt>Pick Up Time</dt>
						<dd></dd>
						<dt>Appointment Day/Hour</dt>
						<dd></dd>
					</dl>
				</fieldset>
			</div>
		</div>
		
		<div class="app-content-separate-content">
			<div class="container">
				
				<div class="row">
					<div class="col-lg-12">
						
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tabActi" data-toggle="tab"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Add Activity</a></li>
							<li><a href="#tabNote" data-toggle="tab"><i class="fa fa-sticky-note" aria-hidden="true"></i> Patient Notes</a></li>
							<li><a href="#tabEmail" data-toggle="tab"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send Email</a></li>
							<li><a href="#tabsFiles" data-toggle="tab"><i class="fa fa-paperclip" aria-hidden="true"></i> Upload Files</a></li>
						</ul>
						<div class="tab-content tab-content-bordered margin-bottom-20">
							<div class="tab-pane" id="tabNote">
								<form id="form_addnote" action="Api_paciente/agregarnota" method="post">
									<input name="tipo" value="1" type="hidden">
									<div class="form-group">
										<label>Note <b class="text-danger">*</b></label>
										<textarea name="note" class="summernote" data-validation="required"></textarea>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-floppy-o"></span>Save Note</button>
									</div>
								</form>
							</div>
							<div class="tab-pane active" id="tabActi">
								<form id="form_addact" action="Api_paciente/agregaractividad" method="post">
									<input name="tipo" value="2" type="hidden">
									<fieldset class="col-md-6">
										<div class="form-group">
											<label class="col-md-12 control-label">Type</label>
											<div class="col-md-12">
												<div class="btn-group" data-toggle="buttons">
												  <label class="btn btn-default btn-sm active"><input type="radio" name="type" value="1" checked="checked"><i class="fa fa-phone"></i> Call</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="2"><i class="fa fa-users"></i> Meeting</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="3"><i class="fa fa-clock-o"></i> Task</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="4"><i class="fa fa-flag"></i> Deadline</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="5"><i class="fa fa-envelope"></i> Email</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="6"><i class="fa fa-cutlery"></i> Lunch</label>
											</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">User</label>
											<div class="col-md-8">
												<select class="form-control bs-select" data-live-search="true" name="user" data-validation="required"></select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Date</label>
											<div class="col-md-8">
												<input name="startdate" type="text" class="form-control input-sm bs-datepickerusa" data-validation="required" disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Hour Start</label>
											<div class="col-md-8">
												<input name="starthour" type="text" class="form-control input-sm timepicker" data-validation="required" disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">End Time</label>
											<div class="col-md-8">
												<input name="finishhour" type="text" class="form-control input-sm timepicker" data-validation="required" disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-12 control-label">Description</label>
											<div class="col-md-12 margin-left-5">
												<textarea name="note" class="summernote" data-validation="required"></textarea>
											</div>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-floppy-o"></span>Save Activity</button>
										</div>
									</fieldset>
									<fieldset class="col-md-6" height="650px">
										<div id="calendar"></div>
									</fieldset>
								</form>
							</div>
							<div class="tab-pane" id="tabEmail">
							<form id="form_sendemail" class="form-horizontal" action="Api_paciente/sendemail" method="post">
								<div class="form-group">
									<label class="col-md-2">From <b class="text-danger">*</b></label>                                            
									<select class="col-md-10 bs-select">
										<option>System@smile4ever.com</option>
									</select>                                            
								</div>                        
								<div class="form-group">
									<label class="col-md-2">To <b class="text-danger">*</b></label>                                            
									<div class="col-md-10">
										<input name="to" type="text" class="form-control" data-validation="required" data-role="tagsinput">
									</div>
								</div>
								<div class="form-group" id="mail-cc">
									<label class="col-md-2">Cc </label>                                            
									<div class="col-md-10">
										<input name="cc" type="text" class="form-control" data-role="tagsinput" data-placeholder="add email">                                            
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Subject <b class="text-danger">*</b></label>
									<div class="col-md-10">
										<input name="subject" type="text" class="form-control" data-validation="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Template</label>                                            
									<div class="col-md-10">
										<select class="bs-select"></select>                                            
									</div>
								</div>
								<div class="form-group" id="fileinputs">
									<label class="col-md-2">File</label>                                            
									<div class="col-md-10">
										<input name="files[]" type="file" class="form-control" multiple />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Body <b class="text-danger">*</b></label>
									<div class="col-md-12">
										<textarea  name="body" class="summernote"></textarea>                                                                        
									</div>
								</div>
								<div class="form-group">
									<div class="pull-right">
										<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-envelope"></span>Send Email</button>
									</div>
								</div>
							</form>
							</div>
							<div class="tab-pane" id="tabsFiles">
								<form id="formaddfile" class="dropzone" enctype="multipart/form-data"></form>
							</div>
						</div>
						<div id="activitiestimeline" class="app-timeline"></div>
						
					</div>
					
				</div>

			</div>
		</div>
	</div>
</div>
        