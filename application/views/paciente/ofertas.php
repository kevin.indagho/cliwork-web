	<style>
	.tbl_headers{
		/*background-size: contain;
		background-position: right;
		background-repeat: no-repeat;
		background-image: url(https://terapiametabolica.com/wp-content/uploads/2013/03/left-arrow-right-hi.png);
		*/
		min-width: 250px;
		background-color:#F5F5F5;
		/*border:1px solid #cacaca; 
		font-size:12px;font-family:arial, helvetica, sans-serif; 
		padding: 10px 10px 10px 10px; 
		color:#FFFFFF;
		  font-weight:bold; color: #FFFFFF;
		background-color: #E6E6E6; background-image: -webkit-gradient(linear, left top, left bottom, from(#E6E6E6), to(#CCCCCC));
		background-image: -webkit-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -moz-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -ms-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -o-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: linear-gradient(to bottom, #E6E6E6, #CCCCCC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E6E6E6, endColorstr=#CCCCCC);*/
	}
	</style>
	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-usd"></i></span>
		<div class="title">
			<h2>Deals</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Patient</li>
				<li class="active">Deals</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row hidden-mobile">
			<div class="col-md-12">
				<div class="block" id="block_deals">
					<div class="block-content">
						<div class="col-sm-6">
							<button data-backdrop="static" data-toggle="modal" data-target="#modal_newdeal" class="btn btn-success btn-xs btn-icon-fixed"><span class="fa fa-plus"></span> New Deal</button>
							<button data-backdrop="static" data-toggle="modal" data-target="#modal_newpatient" class="btn btn-success btn-xs btn-icon-fixed"><span class="fa fa-plus"></span> New patient</button>
							<button class="btn btn-default btn-xs btn-icon" onclick="app.block.expand('#block_deals');"><i class="fa fa-expand"></i></button>
							<button id="btnrefresh" class="btn btn-default btn-xs btn-icon"><i class=" fa fa-refresh"></i></button>
						</div>
						<div class="col-sm-6 text-right">
							<span class="label label-warning label-bordered label-ghost">without activities</span>
							<span class="label label-success label-bordered label-ghost">Pending </span>
							<span class="label label-danger label-bordered label-ghost">Expired</span>
						</div>
					</div>
					<div class="block-content">
						<div class="table-responsive">
							<table id="tbllist" class="table table-bordered">
								<thead></thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>						
		</div>
		
		<div class="row visible-mobile clear-mobile">
			<div class="col-md-12">
				<div class="block">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<button data-backdrop="static" data-toggle="modal" data-target="#modal_newdeal" class="btn btn-success btn-sm btn-icon-fixed"><span class="fa fa-plus"></span> New Deal</button>
							<button data-backdrop="static" data-toggle="modal" data-target="#modal_newpatient" class="btn btn-success btn-sm btn-icon-fixed"><span class="fa fa-plus"></span> New patient</button>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" id="block-refresh-example"><span class="fa fa-refresh"></span></button>
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<span class="label label-primary label-bordered label-ghost">without activities</span>
						<span class="label label-success label-bordered label-ghost">Pending </span>
						<span class="label label-warning label-bordered label-ghost">Soon to expire</span>
						<span class="label label-danger label-bordered label-ghost">Expired</span>
					</div>
					<div class="block-content">
						<div id="panelsaccordion"  class="panel-group" role="tablist" aria-multiselectable="true">
						<!--<div id="tabsstage" class="app-accordion" data-type="full-heigh" data-open="close-other"></div>-->
					</div>
				</div>
			</div>						
		</div>
		
	</div>
	
</div>

<div class="modal fade" id="modal_newpatient" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newpatient"  autocomplete="off" class="form-horizontal" onsubmit="return false;" action="Api_paciente/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add new patient</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<ul class="nav nav-tabs">
					<li class="active"><a id="indexetabPersonal" href="#etabPersonal" data-toggle="tab">Personal</a></li>
					<li><a id="indexetabContact" href="#etabContact" data-toggle="tab">Contact info</a></li>
					<li><a id="indexetabContact" href="#etabDireccion" data-toggle="tab">Direccion</a></li>
				</ul>
				<div class="tab-content tab-validate tab-content-bordered">
					<div class="tab-pane active" id="etabPersonal">
						<div class="form-group">
							<label class="col-md-4 control-label">Origen <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select class="bs-select" name="origin" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Name <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Last Name <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="apellidos" type="text" class="form-control input-sm" data-validation="required">
								<span id="namecomparation" class="help-block text-warning"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Gender <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select class="bs-select" name="genero" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default">
									<option value="0"></option>
									<option value="1">Female</option>
									<option value="2">Male</option>
									<option value="3">Gender-neutral</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Birthdate</label>
							<div class="col-md-8">
								<div class="input-group">
									<input name="birthdate" type="text" class="form-control input-sm birthday_picker">
									<span id="age" class="input-group-addon input-sm"></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Status</label>
							<div class="col-md-8">
								<select name="etapa" class="bs-select" data-live-search="true" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="etabContact">
						<p class="label label-info">Only one is required</p>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
									<input type="email" name="correo" class="form-control input-sm"
									data-validation="email" 
									data-validation-optional-if-answered="casatelefono, trabajotelefono, moviltelefono, ">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<input name="casatelefono" type="text" class="form-control input-sm"
								data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="correo, trabajotelefono, moviltelefono">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
									<input name="trabajotelefono" type="text" class="form-control input-sm" 
									data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="correo, casatelefono, moviltelefono" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<input name="moviltelefono" type="text" class="form-control input-sm" 
								data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="correo, casatelefono, trabajotelefono" >
							</div>
						</div>
					</div>
					<div class="tab-pane" id="etabDireccion">
						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-8">
								<select class="bs-select" name="country" data-live-search="true" data-style="btn-sm btn btn-default"><option></option></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">State</label>
							<div class="col-md-8">
								<select class="bs-select" name="state" data-live-search="true" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">City</label>
							<div class="col-md-8">
								<input name="city" type="text" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Zip code</label>
							<div class="col-md-8">
								<input name="zip" type="text" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-8">
								<input name="address" type="text" class="form-control input-sm">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editstatus" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Detail deal</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabact" data-toggle="tab">Activities</a></li>
					<li><a href="#tabetapa" data-toggle="tab">Stage</a></li>
					<li><a href="#taboferta" data-toggle="tab">Deal</a></li>
					<!--<li><a href="#tabCita" data-toggle="tab">Appointment</a></li>-->
				</ul>
				<div class="tab-content tab-content-bordered padding-0">
					<div class="tab-pane active scrollCustom" id="tabact" style="max-height: 300px;">
						<table id="tbl_act" class="table table-head-light">
							<thead>
								<tr>
									<th class="padding-5">Done</th>
									<th class="padding-5">Due date</th>
									<th class="padding-5">Assigned to user</th>
									<th class="padding-5">Descript</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class="form-group">
							<div class="col-md-12">
								<div id="activitiestimeline" class="app-timeline app-timeline-simple"></div>
							</div>
						</div>
					</div>
					<div class="tab-pane padding-15" id="tabetapa">
						<form id="form_editstatus"  autocomplete="off" class="form-horizontal" action="Api_paciente/editaretapaoferta" method="post">
							<input type="hidden" name="patientid">
							<input type="hidden" name="idoferta">
							<input type="hidden" name="oldetapa">
							<div class="form-group">
								<label class="col-md-4 control-label">Patient</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="patient" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Stage</label>
								<div class="col-md-8">
									<select data-style="btn-sm btn btn-default" class="bs-select" name="etapa">
										<option></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Send to Unscheduled List</label>
								<div class="col-md-7">
									<input type="hidden" name="vendida" value="0">
									<div class="app-checkbox margin-0"><label><input type="checkbox" name="vendida" value="1"><span></span></label></div>
								</div>
							</div>
							<div class="form-group text-right">
								<button id="btnlost" type="button" class="btn btn-sm btn-danger">Mark as lost deal</button>
								<button type="submit" class="btn btn-success btn-sm btn-icon-fixed"><span class="fa fa-floppy-o"></span>Save</button>
							</div>
						</form>
					</div>
					<div class="tab-pane padding-15" id="taboferta">
						<form id="form_editdeal"  autocomplete="off" class="form-horizontal" action="Api_paciente/editaroferta" method="post">
						<input type="hidden" name="id">
						<div class="form-group">
							<label class="col-md-4 control-label">Patient</label>
							<div class="col-md-8">
								<div class="input-group input-group-sm">
									<input type="text" list="patientsearch" class="form-control" name="patient" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
									<input name="email" type="text" class="form-control" placeholder="Email" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-home"></i></span>
									<input name="phome" type="text" class="form-control" placeholder="Home Phone" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<input name="pwork" type="text" class="form-control" placeholder="Work Phone" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									<input name="pmobile" type="text" class="form-control" placeholder="Mobile Phone" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Provider</label>
							<div class="col-md-8">
								<select data-style="btn-sm btn btn-default" name="doctor" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"><option></option></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Procedure search</label>
							<div class="col-md-12">
								<select name="procedure" class="for-control" data-style="btn-sm btn btn-default" data-size="5" data-live-search="true"></select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label">Procedures Attach</label>
							</div>
							<div class="col-md-12">
								<span class="help-block">Check for delete</span>
								<ul id="listProceduresEdit" class="list-group"></ul>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Value</label>
							<div class="col-md-4">
								<input type="text" class="form-control input-sm" name="value" data-validation="number" data-validation="required" data-validation-allowing="float" data-validation-decimal-separator=", .">
							</div>
							<div class="col-md-4">
								<select class="form-control bs-selec input-sm" name="typevalue">
									<option>US Dollar (USD)</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Expected close date</label>
							<div class="col-md-4">
								<input type="text" class="form-control bs-datepickerusa input-sm" name="closedate">
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success btn-sm btn-icon-fixed"><span class="fa fa-floppy-o"></span>Save</button>
						</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="dealdetails btn btn-info btn-sm">Go to activities</button>
			</div>
		
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_newdeal" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newdeal" class="form-horizonta" action="Api_paciente/agregaroferta" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Add Deal</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabnewdeal" data-toggle="tab">New Deal</a></li>
					<li><a href="#tadealdetail" data-toggle="tab">Deal Details</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabnewdeal">
						<?php if($this->session->tipoid == 1):?>
						<div class="form-group">
							<label class="col-md-4 control-label">Seller <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<div class="col-md-12 padding-0">
									<select name="seller" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<div class="form-group">
							<label class="col-md-4 control-label">Patient <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<div class="col-md-12 padding-0">
									<select name="patient" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
									<span class="help-block"><button id="quicknewpat" type="button" class="btn btn-xs btn-success">New patient</button></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
								<input name="email" class="form-control input-sm" data-validation="required" data-validation="email">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-home"></i></span>
									<input name="phome" type="text" class="form-control input-sm">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<input name="pwork" type="text" class="form-control input-sm">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									<input name="pmobile" type="text" class="form-control input-sm">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tadealdetail">
						<div class="form-group">
							<label class="col-md-4 control-label">Provider</label>
							<div class="col-md-8">
								<select data-style="btn-sm btn btn-default" name="doctor" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"><option></option></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Procedure search</label>
							<div class="col-md-12">
								<select name="procedure" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-live-search="true"></select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label">Procedures Attach</label>
							</div>
							<div class="col-md-12 scrollProceadd" style="height: 300px;" >
								<div id="listProcedures" class="list-group"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Value</label>
							<div class="col-md-4">
								<input type="text" class="form-control input-sm" name="value" data-validation="number" data-validation-allowing="float" data-validation-decimal-separator=", .">
							</div>
							<div class="col-md-4">
								<select class="form-control bs-selec input-sm" name="typevalue">
									<option>US Dollar (USD)</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Expected close date</label>
							<div class="col-md-4">
								<input type="text" class="form-control bs-datepickerusa input-sm" name="closedate">
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tablostdeals">
						<table id="dtlostdeals" class="table table-striped table-bordered" width="100%">
							<thead>
								<th></th>
								<th>Patient</th>
								<th>Deal</th>
								<th>Value</th>
								<th>User</th>
								<th></th>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-icon-fixed btn-sm"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_lostpatient" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-danger" role="document">
		<div class="modal-content">
		<form id="form_lostpatient" class="form-horizonta" action="Api_paciente/lost" method="post">
			<input type="hidden" name="id">
			<input type="hidden" name="ofertaid">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Lost Patient</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Patient</label>
					<div class="col-md-8">
						<input type="text" list="patientsearch" class="form-control" name="patient" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Reason</label>
					<div class="col-md-8">
						<textarea class="form-control" name="reason" rows="4"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-backdrop="static" data-toggle="modal" data-target="#modal_editstatus" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>