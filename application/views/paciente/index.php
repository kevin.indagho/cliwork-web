	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Patients</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Patients</li>
				<li class="active">List</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>Patient List</h2>
						</div>
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></button>
							<ul class="dropdown-menu dropdown-left">
								<li class="dropdown-header highlight"><i class="fa fa-eye-slash" aria-hidden="true"></i> Hide Columns</li>
								<li><a><div class="app-checkbox"><label><input class="hc" data-column="0" type="checkbox" checked="">Name<span></span></label></div></a></li>
								<li><a><div class="app-checkbox"><label><input class="hc" data-column="1" type="checkbox" checked="">Email<span></span></label></div></a></li>
								<li><a><div class="app-checkbox"><label><input class="hc" data-column="2" type="checkbox" checked="">Status<span></span></label></div></a></li>
								<li><a><div class="app-checkbox"><label><input class="hc" data-column="3" type="checkbox" checked="">Gender<span></span></label></div></a></li>
								<li class="dropdown-header highlight"><span class="fa fa-cloud-download"></span> Export Data</li>
								<li><a id="btnEXC"><i class="fa fa-file-excel-o"></i> Excel</a></li>
								<li class="divider"></li>
								<li><a id="btnPDF"><i class="text-danger fa fa-file-pdf-o"></i> PDF</a></li>
							</ul>
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>
					</div>
					<div class="block-content">
						<table id="dtpatient" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Name</th>
									<th class="hidden-mobile">Email</th>
									<th class="hidden-mobile">Gender</th>
									<th class="hidden-mobile">Status</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_dtpatientconfig" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-danger" role="document">
		<div class="modal-content">
		<form id="form_configpatient"  autocomplete="off" class="form-horizontal" action="Api_paciente/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Config Table Patient</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Nombre</label>
					<div class="col-md-8">
						<label class="switch switch-cube">
							<input type="checkbox" name="nombre" checked="" value="0">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<!--<button type="submit" class="btn btn-success">Save</button>-->
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_newpatient" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newpatient"  autocomplete="off" class="form-horizontal" action="Api_paciente/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add new patient</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<ul class="nav nav-tabs">
					<li class="active"><a id="indexetabPersonal" href="#etabPersonal" data-toggle="tab">Personal</a></li>
					<li><a id="indexetabContact" href="#etabContact" data-toggle="tab">Contact info</a></li>
					<li><a id="indexetabContact" href="#etabDireccion" data-toggle="tab">Address</a></li>
				</ul>
				<div class="tab-content tab-validate tab-content-bordered">
					<div class="tab-pane active" id="etabPersonal">
						<div class="form-group">
							<label class="col-md-4 control-label">Picture</label>
							<div class="col-md-8">
								<div class="contact contact-rounded contact-bordered contact-single">
                                    <img id="pictureadd" src="<?=base_url()?>img/user/no-image.png">
                                </div>
								<input name="foto" type="file" class="form-control input-sm" accept="image/png, image/jpeg" data-validation-allowing="jpg, png" data-validation="mime size" data-validation-max-size="2M">
								<span class="help-block">2MB JPG|PNG</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Refered by <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select class="bs-select" name="origin" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Name <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Last Name <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="apellidos" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Gender <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select class="bs-select" name="genero" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default">
									<option value="0"></option>
									<option value="1">Female</option>
									<option value="2">Male</option>
									<option value="3">Gender-neutral</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Birthdate</label>
							<div class="col-md-8">
								<div class="input-group">
									<input name="birthdate" type="text" class="form-control input-sm birthday_picker">
									<span id="age" class="input-group-addon input-sm"></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Status</label>
							<div class="col-md-8">
								<select name="etapa" class="bs-select" data-live-search="true" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="etabContact">
						<p class="label label-info">Only one is required</p>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
									<input type="email" name="correo" class="form-control input-sm"
									data-validation="email" 
									data-validation-optional-if-answered="casatelefono, trabajotelefono, moviltelefono, ">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<input name="casatelefono" type="text" class="form-control input-sm"
								data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="correo, trabajotelefono, moviltelefono">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
									<input name="trabajotelefono" type="text" class="form-control input-sm" 
									data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="correo, casatelefono, moviltelefono" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<input name="moviltelefono" type="text" class="form-control input-sm" 
								data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="correo, casatelefono, trabajotelefono" >
							</div>
						</div>
					</div>
					<div class="tab-pane" id="etabDireccion">
						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-8">
								<select class="bs-select" name="country" data-live-search="true" data-style="btn-sm btn btn-default"><option></option></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">State</label>
							<div class="col-md-8">
								<select class="bs-select" name="state" data-live-search="true" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">City</label>
							<div class="col-md-8">
								<input name="city" type="text" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Zip code</label>
							<div class="col-md-8">
								<input name="zip" type="text" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-8">
								<input name="address" type="text" class="form-control input-sm">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editpatient" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editpatient" autocomplete="off" class="form-horizontal" action="Api_paciente/editar" method="post">
			<input name="id" type="hidden" data-validation="required">
			<input name="oldpicture" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Patient</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<ul class="nav nav-tabs">
					<li class="active"><a id="indextabPersonalEdit" href="#tabPersonalEdit" data-toggle="tab">Personal</a></li>
					<li><a id="indextabContactEdit" href="#tabContactEdit" data-toggle="tab">Contact info</a></li>
					<li><a href="#tabDireccionEdit" data-toggle="tab">Address</a></li>
					<li><a href="#tabLostEdit" data-toggle="tab"><span class="label label-danger">LOST</span></a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabPersonalEdit">
						<div class="form-group">
							<label class="col-md-4 control-label">
								<div id="linkpreview" class="contact contact-rounded contact-bordered contact-single preview"  data-preview-image="assets/images/large/img-1.jpg" data-preview-size="modal-sm">
									<img id="pictureedit">
                                </div>
							</label>
							<div class="col-md-8">
								<input name="foto" type="file" class="form-control input-sm" accept="image/png, image/jpeg" data-validation-allowing="jpg, png" data-validation="mime size" data-validation-max-size="2M">
								<span class="help-block">This will replace the old photo. 2MB JPG|PNG</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Refered by <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select class="bs-select" name="origin" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Name <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Last Name <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="apellidos" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Gender <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select class="bs-select" name="genero" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default">
									<option value="3">Gender-neutral</option>
									<option value="1">Female</option>
									<option value="2">Male</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Birthdate</label>
							<div class="col-md-8">
								<div class="input-group">
									<input name="birthdate" type="text" class="form-control input-sm birthday_picker">
									<span id="ageEdit" class="input-group-addon input-sm"></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Stage <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select name="etapa" class="bs-select" data-live-search="true" data-validation="required" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tabContactEdit">
						<p class="label label-info"><i class="fa fa-info"></i> Only one is required</p>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
									<input type="email" name="correo" class="form-control input-sm"
									data-validation="email" 
									data-validation-optional-if-answered="casatelefono, trabajotelefono, moviltelefono, ">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<input name="casatelefono" type="text" class="form-control input-sm"
								data-validation="number" data-validation-ignore="*+#-: " data-validation-optional-if-answered="correo, trabajotelefono, moviltelefono">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
									<input name="trabajotelefono" type="text" class="form-control input-sm" 
									data-validation="number" data-validation-ignore="*+#-: " data-validation-optional-if-answered="correo, casatelefono, moviltelefono" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<input name="moviltelefono" type="text" class="form-control input-sm" 
								data-validation="number" data-validation-ignore="*+#-: " data-validation-optional-if-answered="correo, casatelefono, trabajotelefono" >
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tabDireccionEdit">
						<div class="form-group">
							<label class="col-md-4 control-label">Country</label>
							<div class="col-md-8">
								<select class="bs-select" name="country" data-live-search="true" data-style="btn-sm btn btn-default"><option></option></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">State</label>
							<div class="col-md-8">
								<select class="bs-select" name="state" data-live-search="true" data-style="btn-sm btn btn-default"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">City</label>
							<div class="col-md-8">
								<input name="city" type="text" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Zip code</label>
							<div class="col-md-8">
								<input name="zip" type="text" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Address</label>
							<div class="col-md-8">
								<input name="address" type="text" class="form-control input-sm">
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tabLostEdit">
						<div class="form-group">
							<label class="col-md-4 control-label">User</label>
							<div class="col-md-8">
								<input name="lostuser" class="form-control" disabled />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Date</label>
							<div class="col-md-8">
								<input name="lostdate" class="form-control" disabled />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Reason</label>
							<div class="col-md-8">
								<textarea name="lostreason" class="form-control" rows="5" disabled></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Value</label>
							<div class="col-md-8">
								<input name="lostvalue" class="form-control" disabled />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Treatment</label>
							<div class="col-md-8">
								<div class="list-group list-group-noborder">
									<div class="list-group-item">List group item 1</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Active</label>
							<div class="col-md-8">
								<button id="btnlost" type="button" class="btn btn-sm btn-success btn-glow">activate</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="details btn btn-info">Details</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="preview" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-cross"></span></button>
		
		<div class="modal-content">
			<div class="modal-body padding-5"></div>
		</div>
	</div>            
</div>
        