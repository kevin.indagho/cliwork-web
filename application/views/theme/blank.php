<div class="app-content app-sidebar-left">
	<!-- START APP HEADER -->
	<div class="app-header">
		<ul class="app-header-buttons">
			<li class="visible-mobile"><a href="#" class="btn btn-link btn-icon" data-sidebar-toggle=".app-sidebar.dir-left"><span class="icon-menu"></span></a></li>
			<li class="hidden-mobile"><a href="#" class="btn btn-link btn-icon" data-sidebar-minimize=".app-sidebar.dir-left"><span class="icon-list"></span></a></li>
		</ul>
		<form class="app-header-search" action="" method="post">
			<input type="text" name="keyword" placeholder="Search">
		</form>
	</div>
	<!-- END APP HEADER  -->
	
	<!-- START PAGE HEADING -->
	<div class="app-heading app-heading-bordered app-heading-page">
		<div class="icon icon-lg">
			<span class="icon-home"></span>
		</div>
		<div class="title">
			<h1>Blank</h1>
			<p>Subtitle</p>
		</div>
	</div>
	<div class="app-heading-container app-heading-bordered bottom">
		<ul class="breadcrumb">
			<li><a href="#">First level</a></li>                                                     
			<li class="active">Current</li>
		</ul>
	</div>
	<div class="container">
	</div>
</div>
        