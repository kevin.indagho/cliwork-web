<!DOCTYPE html>
<html lang="en">
    <head>                        
        <title>Cliwork</title>            
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?=base_url()?>css/styles.css">
		<link href="<?=base_url()?>css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>css/smart_wizard.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>js/vendor/fullcalendar/scheduler.min.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>css/odontogram.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>js/vendor/odometer/odometer-theme-default.css" rel="stylesheet" type="text/css" />
		
    </head>	
    <body>
        <div class="app">
            <div class="app-container">        
