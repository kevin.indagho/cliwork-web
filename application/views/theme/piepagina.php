
            </div>
</div>

 <div class="modal fade" id="preview" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon-cross"></span></button>
		
		<div class="modal-content">
			<div class="modal-body"></div>
		</div>
	</div>            
</div>
			
<div class="modal fade" id="modal_searchpatient" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-info" role="document">
		<div class="modal-content">
		<form id="form_lostpatient" class="form-horizontal">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Search patient</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Search </label>
						<select id="patsearch" name="patient[id]" class="form-control" data-style="btn-sm btn btn-default" data-size="10" data-validation="required" data-live-search="true"></select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_deallost" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-danger" role="document">
		<div class="modal-content">
		<form id="form_lostpatient" class="form-horizontal">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Deal Lost Detail</h4>
			</div>
			<div class="modal-body">
				<div class="form-group has-error">
					<div class="col-md-12">
						<label class="control-label">Date Lost </label>
						<input type="text" class="form-control" name="date" disabled>
					</div>
				</div>
				<div class="form-group has-error">
					<div class="col-md-12">
						<label class="control-label">Reason Lost</label>
						<textarea class="form-control" name="reason" disabled rows="5"></textarea>
					</div>
				</div>
				<div class="scrollCustom" style="height: 250px; float:left">
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Patient</label>
						<div class="input-group input-group-sm">
							<input type="text" list="patientsearch" class="form-control" name="patient" disabled>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="input-group input-group-sm">
							<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
							<input name="email" type="text" class="form-control" placeholder="Email" disabled>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon"><i class="fa fa-home"></i></span>
							<input name="phome" type="text" class="form-control" placeholder="Home Phone" disabled>
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon"><i class="fa fa-building"></i></span>
							<input name="pwork" type="text" class="form-control" placeholder="Work Phone" disabled>
						</div>
					</div>
					<div class="col-md-4">
						<div class="input-group input-group-sm">
							<span class="input-group-addon"><i class="fa fa-phone"></i></span>
							<input name="pmobile" type="text" class="form-control" placeholder="Mobile Phone" disabled>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 control-label">Treatment</label>
					<div class="col-md-12">
						<select class="multiselect form-control input-sm" multiple="multiple" name="treatment[]" disabled></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Value</label>
					<div class="col-md-4">
						<input type="text" class="form-control input-sm" name="value" data-validation="required" data-validation-allowing="float" data-validation-decimal-separator="," disabled>
					</div>
					<div class="col-md-4">
						<select class="form-control bs-selec input-sm" name="typevalue" disabled>
							<option>US Dollar (USD)</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Expected close date</label>
					<div class="col-md-8">
						<input type="text" class="form-control input-sm" name="closedate" disabled>
					</div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_view_deal" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-info" role="document">
		<div class="modal-content">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Detail deal</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="alert alert-warning alert-icon-block" role="alert"> 
					<div class="alert-icon">
						<i class="fa fa-info-circle fa-2x" aria-hidden="true"></i>
					</div>
					<strong>Info!</strong> This deal WITHOUT ACTIVITIES. 
				</div>
				<ul class="nav nav-tabs">
					<li class="active"><a href="#taboferta" data-toggle="tab">Deal</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="taboferta">
						<form id="form_view_deal"  autocomplete="off" class="form-horizontal">
						<div class="form-group">
							<label class="col-md-4 control-label">Patient</label>
							<div class="col-md-8">
								<div class="input-group input-group-sm">
									<input type="text" list="patientsearch" class="form-control" name="patient" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
									<input name="email" type="text" class="form-control" placeholder="Email" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-home"></i></span>
									<input name="phome" type="text" class="form-control" placeholder="Home Phone" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<input name="pwork" type="text" class="form-control" placeholder="Work Phone" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="input-group input-group-sm">
									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									<input name="pmobile" type="text" class="form-control" placeholder="Mobile Phone" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-12 control-label">Treatment</label>
							<div class="col-md-12">
								<ul id="treatmentlist" class="list-group list-group-noborder"></ul>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Value</label>
							<div class="col-md-4">
								<input type="text" class="form-control input-sm" name="value" disabled>
							</div>
							<div class="col-md-4">
								<select class="form-control bs-selec input-sm" name="typevalue" disabled>
									<option>US Dollar (USD)</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Expected close date</label>
							<div class="col-md-4">
								<input type="text" class="form-control bs-datepickerusa input-sm" name="closedate" disabled>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="viewdealdetails btn btn-info btn-sm btn-icon-fixed"><span class="fa fa-info"></span>Details</button>
			</div>
		</div>
	</div>            
</div>


		<script src="<?=base_url()?>js/vendor/chartjs/chartjs.js"></script>
		<script src="<?=base_url()?>js/vendor/chartjs/chartjs-plugin-datalabels.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery.min.js"></script>
		<!--<script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery-migrate.min.js"></script>-->
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/moment/moment.min.js"></script>
		
        <script type="text/javascript" src="<?=base_url()?>js/vendor/customscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-select/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-select/ajax-bootstrap-select.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/select2/select2.full.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/multiselect/jquery.multi-select.js"></script>
		
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/maskedinput/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/form-validator/jquery.form-validator.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/noty/jquery.noty.packaged.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/datatables/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/datatables/dataTables.buttons.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/knob/jquery.knob.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jvectormap/jquery-jvectormap-us-aea-en.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/sparkline/jquery.sparkline.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/morris/raphael.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/morris/morris.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/rickshaw/rickshaw.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/isotope/isotope.pkgd.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/dropzone/dropzone.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/nestable/jquery.nestable.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/cropper/cropper.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/tableExport.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jquery.base64.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/html2canvas.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jspdf/libs/base64.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-daterange/daterangepicker.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-tour/bootstrap-tour.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

        <!--<script type="text/javascript" src="<?=base_url()?>js/vendor/fullcalendar/fullcalendar.js"></script>-->
        <script type="text/javascript" src="<?=base_url()?>js/vendor/fullcalendar/3.10.0/moment.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/fullcalendar/3.10.0/fullcalendar.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/vendor/fullcalendar/scheduler/scheduler.min.css"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/fullcalendar/scheduler/scheduler.min.js"></script>
        
		<script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
		
        <script type="text/javascript" src="<?=base_url()?>js/vendor/smartwizard/jquery.smartWizard.js"></script>
        <script src="<?=base_url()?>js/vendor/summernote/summernote.min.js"></script>
		<script src="<?=base_url()?>js/vendor/xeditable/bootstrap-editable.min.js"></script>
        <script src="<?=base_url()?>js/vendor/xeditable/ext/address.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/vendor/ionrangeslider/ion.rangeSlider.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/vendor/odometer/odometer.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/vendor/pusher/pusher.min.js"></script>
				
        <script type="text/javascript" src="<?=base_url()?>js/app.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/app_plugins.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/app_demo.js"></script>
		
		<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
		<script>
			var selectpat = "<?=$this->session->patient?>";
			var dbconfig = "<?=$this->session->dbconfig?>";			
		</script>
		<script type="text/javascript" src="<?=base_url()?>js/pages/global.js"></script>
		<script type="text/javascript" src="<?=base_url()?>js/jquery.smartWizard.min.js"></script>
		<?php foreach($scripts as $script): ?>
		<script type="text/javascript" src="<?=base_url()?><?=$script?>"></script>
		<?php endforeach; ?>
    </body>
</html>