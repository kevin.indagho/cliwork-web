<div class="app-header app-header-design-default">
	<ul class="app-header-buttons visible-mobile">
		<li><a href="#" class="btn btn-link btn-icon" data-header-navigation-toggle="true"><span class="icon-menu"></span></a></li>                    
	</ul>
	<a href="<?=base_url()?>" class="app-header-logo app-header-logo-light app-header-logo-condensed">Cliwork</a>
	<div class="app-header-navigation app-header-navigation-hover">
		<nav>
			<ul>
			<li><a <?=($menu == "home") ? 'class="active"' : "";?> href="<?=base_url("")?>"><span class="icon-home"></span> Home</a></li>
			<?php if(
			$this->session->permisos[4]['permitido'] == 1
			|| $this->session->permisos[8]['permitido'] == 1
			|| $this->session->permisos[5]['permitido'] == 1): ?>
			<li class="openable <?=($menu == "patiens") ? 'open' : "";?>">
				<a href="#" <?=($menu == "patiens") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-users"></i></span>Patients</a>
				<ul>
					<?php if($this->session->permisos[4]['permitido'] == 1): ?>
						<li><a <?=($submenu == "list") ? 'class="active"' : "";?> href="<?=base_url("patient/list")?>"><span class="fa fa-angle-right"></span> List</a></li>
					<?php endif; ?>
					<?php if($this->session->permisos[8]['permitido'] == 1): ?>
						<li><a <?=($submenu == "Account") ? 'class="active"' : "";?> href="<?=base_url("patient/account")?>"><span class="fa fa-angle-right"></span> Account</a></li>
					<?php endif; ?>
					<?php if($this->session->permisos[5]['permitido'] == 1): ?>
						<li><a <?=($submenu == "deals") ? 'class="active"' : "";?> href="<?=base_url("patient/deals")?>"><span class="fa fa-usd"></span> Deals</a></li>
					<?php endif; ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if($this->session->permisos[9]['permitido'] == 1): ?>
				<li <?=($menu == "appointments") ? 'open' : "";?>">
					<a href="<?=base_url("appointments/agenda")?>" <?=($menu == "appointments") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-calendar"></i></span>Appointments</a>
				</li>
			<?php endif; ?>
			<?php if($this->session->permisos[10]['permitido'] == 1
					|| $this->session->permisos[11]['permitido'] == 1
					|| $this->session->permisos[12]['permitido'] == 1
					|| $this->session->permisos[13]['permitido'] == 1): ?>
				<li class="openable <?=($menu == "reports") ? 'open' : "";?>">
					<a href="#" <?=($menu == "reports") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-file-text" aria-hidden="true"></i></span>Reports</a>
					<ul>
						<?php if($this->session->permisos[14]['permitido'] == 1): ?>
						<li><a <?=($submenu == "rem") ? 'class="active"' : "";?> href="<?=base_url("reports/procedure/reminders")?>"><span class="fa fa-chevron-right"></span> Procedures reminders</a></li>
						<?php endif; ?>
						<?php if($this->session->permisos[10]['permitido'] == 1): ?>
						<li><a <?=($submenu == "act") ? 'class="active"' : "";?> href="<?=base_url("reports/activities")?>"><span class="fa fa-chevron-right"></span> Activities</a></li>
						<?php endif; ?>
						<?php if($this->session->permisos[11]['permitido'] == 1): ?>
						<li><a <?=($submenu == "ven") ? 'class="active"' : "";?> href="<?=base_url("reports/deals")?>"><span class="fa fa-chevron-right"></span> Deals</a></li>
						<?php endif; ?>
						<?php if($this->session->permisos[12]['permitido'] == 1): ?>
						<li><a <?=($submenu == "deals") ? 'class="active"' : "";?> href="<?=base_url("patient/deals/lost")?>"><span class="fa fa-usd"></span> Deals Lost</a></li>
						<?php endif; ?>
						<?php if($this->session->permisos[13]['permitido'] == 1): ?>
						<li><a <?=($submenu == "others") ? 'class="active"' : "";?> href="<?=base_url("reports/others")?>"><span class="fa fa-usd"></span> Others</a></li>
						<?php endif; ?>
					</ul>
				</li>
			<?php endif; ?>
			<?php if($this->session->permisos[15]['permitido'] == 1 
			|| $this->session->permisos[16]['permitido'] == 1): ?>
			<li class="openable <?=( $menu == "data") ? 'open' : "";?>">
				<a href="#" <?=( $menu == "survey") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-database"></i></span>Survey</a>
				<ul>
					
					<?php if($this->session->permisos[15]['permitido'] == 1): ?>
					<li><a <?=( $submenu == "list") ? 'class="active"' : "";?> href="<?=base_url("survey/list")?>"><span class="fa fa-chevron-right"></span>list</a></li>
					<?php endif; ?>
					<?php if($this->session->permisos[16]['permitido'] == 1): ?>
					<li><a <?=( $submenu == "questions") ? 'class="active"' : "";?> href="<?=base_url("survey/questions")?>"><span class="fa fa-chevron-right"></span>Questions</a></li>
					<?php endif; ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if($this->session->permisos[6]['permitido'] == 1): ?>
				<li class="openable <?=( $menu == "config") ? 'open' : "";?>">
					<a href="#" <?=( $menu == "config") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-cog"></i></span>Configuration</a>
					<ul>
						<li><a <?=( $submenu == "system") ? 'class="active"' : "";?>href="<?=base_url("config")?>"><span class="fa fa-chevron-right"></span>System</a></li>
						<li><a <?=( $submenu == "noti") ? 'class="active"' : "";?>href="<?=base_url("config/notifications")?>"><span class="fa fa-chevron-right"></span>Notifications</a></li>
					</ul>
				</li>
			<?php endif; ?>
			<?php if(
					$this->session->permisos[7]['permitido'] == 1
					|| $this->session->permisos[2]['permitido'] == 1
					|| $this->session->permisos[3]['permitido'] == 1
					): ?>
				<li class="openable <?=( $menu == "data") ? 'open' : "";?>">
					<a href="#" <?=( $menu == "data") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-database"></i></span>Data</a>
					<ul>
						<?php if($this->session->permisos[2]['permitido'] == 1): ?>
							<li><a <?=($submenu == "list") ? 'class="active"' : "";?> href="<?=base_url("users/list")?>"><span class="fa fa-list-ol"></span>User List</a></li>
						<?php endif; ?>
						<?php if($this->session->permisos[3]['permitido'] == 1): ?>
							<li><a <?=($submenu == "per") ? 'class="active"' : "";?> href="<?=base_url("users/permission")?>"><span class="fa fa-ban"></span>User Permissions</a></li>                                                    
						<?php endif; ?>
						<?php if($this->session->permisos[7]['permitido'] == 1): ?>
							<li><a <?=( $submenu == "stage") ? 'class="active"' : "";?> href="<?=base_url("data/stage")?>"><span class="fa fa-chevron-right"></span>Stage</a></li>
							<li><a <?=( $submenu == "files") ? 'class="active"' : "";?> href="<?=base_url("data/files")?>"><span class="fa fa-chevron-right"></span>Files</a></li>
							<li><a <?=( $submenu == "provider") ? 'class="active"' : "";?> href="<?=base_url("data/providers")?>"><span class="fa fa-chevron-right"></span>Providers</a></li>
							<li><a <?=( $submenu == "procedure") ? 'class="active"' : "";?> href="<?=base_url("data/procedures")?>"><span class="fa fa-chevron-right"></span>Procedures</a></li>
							<li><a href="<?=base_url("data/origin")?>"><span class="fa fa-chevron-right"></span>Origin</a></li>
						<?php endif; ?>
					</ul>
				</li>
			<?php endif; ?>
			<?php if($this->session->permisos[17]['permitido'] == 1): ?>
			<li class="openable <?=( $menu == "analysis") ? 'open' : "";?>">
				<a href="#" <?=( $menu == "analysis") ? 'class="active"' : "";?>><span class="nav-icon-hexa"><i class="fa fa-database"></i></span>Analysis</a>
				<ul>
					<li><a <?=( $submenu == "comproviders") ? 'class="active"' : "";?> href="<?=base_url("analysis/providers")?>"><span class="fa fa-chevron-right"></span>Providers Comparations</a></li>
				</ul>
			</li>
			<?php endif; ?>
		</ul>
		</nav>
	</div>
	<ul class="app-header-buttons pull-right">
		<li>
			<div class="contact contact-rounded contact-bordered contact-lg contact-ps-controls hidden-xs">
				<img src="<?=base_url()?>img/user/no-image.png" alt="John Doe">
				<div class="contact-container">
					<input id="usermd5" type="hidden" value="<?=$this->session->md5?>">
					<input id="userid" type="hidden" value="<?=$this->session->id?>">
					<input id="usertype" type="hidden" value="<?=$this->session->tipoid?>">
					<input id="nombreid" type="hidden" value="<?=$this->session->nombreid?>">
					<a href="#"><?=$this->session->nombre?> <?=$this->session->apellidos?></a>
					<span><?=$this->session->tiponombre?></span>
				</div>
				<!--<div class="contact-controls">
					<div class="dropdown">
						<button type="button" class="btn btn-default btn-icon" data-toggle="dropdown"><span class="icon-layers"></span></button>                        
						<ul class="dropdown-menu dropdown-left">
							<li><a href="pages-profile-social.html"><span class="icon-users"></span> Account</a></li> 
							<li><a href="pages-messages-chat.html"><span class="icon-envelope"></span> Messages</a></li>
							<li><a href="pages-profile-card.html"><span class="icon-users"></span> Contacts</a></li>
							<li class="divider"></li>
							<li><a href="pages-email-inbox.html"><span class="icon-envelope"></span> E-mail <span class="label label-danger pull-right">19/2,399</span></a></li> 
						</ul>
					</div>
				</div>-->
			</div>
		</li>        
		<li>
			<div class="dropdown">                                            
				<button class="btn btn-default btn-icon btn-informer" data-toggle="modal" data-target="#modal_searchpatient">
					<span class="icon-magnifier"></span>
				</button>
			</div>
		</li>
		<li>
			<div class="dropdown">                                            
				<button id="informernoti" class="btn btn-default btn-icon btn-informer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<span class="icon-alarm"></span>
					<span id="informernoty" style="display:none;" class="informer informer-danger informer-sm informer-square"></span>
				</button>
				<ul class="dropdown-menu dropdown-form dropdown-left dropdown-form-wide">
					<li class="padding-0">                        
						<div class="app-heading title-only app-heading-bordered-bottom">
							<div class="icon"><span class="icon-text-align-left"></span></div>
							<div class="title"><h2>Notifications</h2></div>
							<div class="heading-elements">
								<a id="btnSyncNot" href="#" class="btn btn-default btn-icon"><span class="icon-sync"></span></a>
							</div>
						</div>
						<div id="block_noti" class="block block-condensed padding-0 margin-0" style="border:0px;">
							<div class="block-content padding-0 margin-0">
								<div id="listnoti" class="app-timeline app-timeline-simple text-sm padding-0 padding-bottom-5" style="height: 240px;"></div>
							</div>
						</div>
					</li>
					<!--<li class="padding-top-0">
						<button class="btn btn-block btn-link">Preview All</button>
					</li>-->
				</ul>
			</div>
		</li>
		<li>
			<button type="button" id="logout" class="btn btn-default btn-icon"><span class="icon-power-switch"></span></button>
		</li>
	</ul>
</div>
                
<div class="app-content">