
	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-calendar"></i></span>
		<div class="title">
			<h2>Appointments Agenda</h2>
			<ul class="breadcrumb">
				<li>appointments</li>
				<li class="active">Agenda</li>
			</ul>
		</div>
	</div>
	<div class="app-content-separate app-content-separated-left">
		<div class="app-content-separate-left" data-separate-control-height="true">
			<div class="app-content-separate-panel padding-20 visible-mobile">
				<div class="pull-left">
					<h4 class="text-sm text-uppercase text-bolder margin-bottom-0">Visible On Mobile</h4>
					<p class="subheader">Use this panel to control this sidebar</p>
				</div>
				<button class="btn btn-default btn-icon pull-right" data-separate-toggle-panel=".app-content-separate-left"><span class="icon-menu"></span></button>                                
			</div>
			<div class="app-content-separate-content padding-20">
				<div class="app-heading app-heading-small heading-transparent app-heading-condensed">
					<div class="title">
						<h2>Appointment Unscheduled List</h2>
						<p>Drag & Drop this events</p>
					</div>
				</div>
				<div class="list-group" id="external-events"></div>												
			</div>
		</div>
		<div class="app-content-separate-content">
			<div class="container">
				<div id="block_calendar" class="block block-condensed padding-top-20">
					<div class="block-content">
						<!--
						<button class="btn btn-info btn-xs btn-glow" data-toggle="modal" data-target="#modal_help">Help</button>
						<div class="block-divider"></div>
						-->
						<div class="col-md-">
							<div class="col-md-2 padding-0 padding-right-10">
								<select id="providercalendar" name="providercalendar" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true">
									<option value="all">All Doctors</option>
								</select>
							</div>
							<div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_newappointment" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newappointment"  autocomplete="off" class="form-horizontal" action="Api_cita/citas" method="post">
			<input type="hidden" name="inbox" value="0">
			<input type="hidden" name="planned" value="0">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">New Appointment</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<ul class="nav nav-tabs">
					<li class="active"><a id="indextabpat" href="#tabpat" data-toggle="tab">Patient</a></li>
					<li><a id="indextabaappt" href="#tabaappt" data-toggle="tab">Appointment</a></li>
					<li><a id="indextabaapptproc" href="#tabaapptproc" data-toggle="tab">Procedures Attach</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabpat">
						<div class="form-group">
							<label class="col-md-4 control-label">Patient <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<select name="patient[id]" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
								<div class="app-checkbox"> 
									<input type="hidden" name="new" value="0" checked="checked">
									<label><input type="checkbox" name="new" value="1"><span></span>It's new patient?</label> 
								</div>
							</div>
							<div class="col-md-8">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<input name="patient[nombre]" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Last Name</label>
							<div class="col-md-8">
								<input name="patient[apellidos]" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
								<input name="patient[correo]" class="form-control input-sm" data-validation="required" data-validation="email"
								data-validation-optional-if-answered="patient[casatelefono],patient[trabajotelefono],patient[moviltelefono]">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-home"></i></span>
									<input name="patient[casatelefono]" type="text" class="form-control input-sm" 
									data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="patient[correo],patient[trabajotelefono],patient[moviltelefono]" >
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<input name="patient[trabajotelefono]" type="text" class="form-control input-sm"
									data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="patient[correo],patient[casatelefono],patient[moviltelefono]">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									<input name="patient[moviltelefono]" type="text" class="form-control input-sm"
									data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="patient[correo],patient[casatelefono],patient[trabajotelefono]">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tabaappt">
						<div class="form-group">
							<label class="col-md-12 control-label">Date Appointment</label>
							<div class="col-md-6">
								<input name="date" class="form-control input-sm" readonly>
							</div>
							<div class="col-md-6">
								<input name="time" class="form-control input-sm" readonly>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<!--<div class="col-md-12">
									<label class="control-label col-md-12"></label>
									<button id="cancelAppt" type="button" class="btn btn-danger btn-glow btn-sm">Cancel</button>
								</div>-->
							</div>
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="app-checkbox">
										<input type="hidden" name="new" value="0">
										<label><input type="checkbox" name="new" value="1">New Patient</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="app-checkbox">
											<input type="hidden" name="needs" value="0">
										<label><input type="checkbox" name="needs" value="1">Special needs</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="app-checkbox">
										<input type="hidden" name="confirmed" value="0">
										<label><input type="checkbox" name="confirmed" value="1">Confirmed</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="app-checkbox">
										<input type="hidden" name="sedation" value="0">
										<label><input type="checkbox" name="sedation" value="1">Sedation</label>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-12 control-label">Sedation note</label>
							<div class="col-md-12">
								<input type="text" name="sedationnote" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<label class="col-md-12 control-label">Reason visit</label>
							</div>
							<div class="col-md-12">
								<select name="reason" class="form-control bs-select show-tick" data-style="btn-sm btn btn-default" data-size="5" data-live-search="true"></select>
							</div>
							<div class="col-md-12 scrollProceadd" style="height: 300px;" >
								<div id="listProceduresadd" class="list-group"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label class="col-md-4  padding-left-0 control-label">Status <b class="text-danger">*</b></label>
								<div class="col-md-8 padding-0">
									<select name="status" class="form-control bs-select show-tick" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
								</div>
							</div>
							<div class="col-md-12">
								<label class="col-md-4 padding-left-0 control-label">Doctor <b class="text-danger">*</b></label>
								<div class="col-md-8 padding-0">
									<select data-style="btn-sm btn btn-default" name="doctor" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"></select>
								</div>
							</div>
							<div class="col-md-12">
								<label class="col-md-4 padding-left-0 control-label">Hygienis</label>
								<div class="col-md-8 padding-0">
									<select data-style="btn-sm btn btn-default" name="hygienis" class="form-control bs-select show-tick" data-size="5" data-live-search="true"></select>
								</div>
							</div>
							<div class="col-md-12">
								<label class="col-md-4 padding-left-0 control-label">Assistant</label>
								<div class="col-md-8 padding-0">
									<select data-style="btn-sm btn btn-default" name="assistant[]" multiple class="form-control bs-select show-tick" data-size="5" data-live-search="true"></select>
								</div>
							</div>
						</div>
						<div class="form-group">
								<label class="col-md-12 control-label">Notes <b class="text-danger">*</b></label>
								<div class="col-md-12">
									<textarea name="notes" class="form-control" rows="5" data-validation-depends-on="needs"></textarea>
								</div>
							</div>
					</div>
					<div class="tab-pane" id="tabaapptproc">
						<div class="form-group">
							<label class="col-md-12 control-label">Services Treat Plan<b class="text-danger">*</b></label>
							<div class="col-md-12">
								<table id="tp_appt_proce_add" class="table table-bordered" width="100%" style="font-size: x-small;">
									<thead>
										<tr>
											<th class="padding-5 padding-top-0 padding-bottom-0">Time</th>
											<th class="padding-5 padding-top-0 padding-bottom-0">Code</th>
											<th class="padding-5 padding-top-0 padding-bottom-0">Description</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editappointment" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning modal-fw" role="document">
		<div class="modal-content">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Appointment</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div id="errormsg"></div>
				<div id="unschedlistnoty" class="col-md-12" style="display:none;">
					<div class="alert alert-danger alert-icon-block alert-dismissible" role="alert"> 
						<div class="alert-icon">
							<span class="fa fa-exclamation-triangle"></span>
						</div>
						This appointment will return to the list of unscheduled appointments and the doctor will be notified.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span></button>
					</div>
				</div>
				<ul class="nav nav-tabs">
					<li class="active"><a id="indextabpate" href="#tabpate" data-toggle="tab">Patient</a></li>
					<li><a id="indextabaappte" href="#tabaappte" data-toggle="tab">Appointment</a></li>
					<li><a href="#tabproce" data-toggle="tab">Procedures for this appointment</a></li>
					<li><a href="#tabitinerario" data-toggle="tab">Flight itinerary</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabpate">
						<form id="form_patappt">
							<input name="patient[id]" type="hidden">
							<div class="form-group">
								<label class="col-md-4 control-label">Full Name</label>
								<div class="col-md-8">
									<input name="patient[nombre]" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input name="patient[correo]" class="form-control input-sm" disabled data-validation="email">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Home Phone</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-home"></i></span>
										<input name="patient[casatelefono]" type="text" class="form-control input-sm" disabled>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Work Phone</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-building"></i></span>
										<input name="patient[trabajotelefono]" type="text" class="form-control input-sm"disabled>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Mobile Phone</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-phone"></i></span>
										<input name="patient[moviltelefono]" type="text" class="form-control input-sm" disabled>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="tabaappte">
						<form id="form_editappointment"  autocomplete="off" class="form-horizonta" action="Api_cita/citas" method="PUT">
							<input type="hidden" name="id">
							<div class="col-md-6">
								<div class="form-group">
									<div class="col-md-12">
										<label class="col-md-4 padding-left-0 control-label">Date Appointment</label>
										<div class="col-md-4 padding-0">
											<input name="date" class="form-control input-sm" readonly>
										</div>
										<div class="col-md-4 padding-0 padding-left-5">
											<input name="time" class="form-control input-sm" readonly>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label class="col-md-4  padding-left-0 control-label">Value Deal <b class="text-info"><i class="fa fa-info"></i></b></label>
										<div class="col-md-8 padding-0">
										<input type="text" name="amountapp" class="form-control input-sm" disabled>
										</div>
									</div>
									<div class="col-md-12">
										<label class="col-md-4  padding-left-0 control-label">Status <b class="text-danger">*</b></label>
										<div class="col-md-8 padding-0">
											<select name="status" class="form-control bs-select show-tick" data-style="btn-sm btn btn-default" data-size="5" data-validation="required" data-live-search="true"></select>
										</div>
									</div>
									<div class="col-md-12">
										<label class="col-md-4 padding-left-0 control-label">Doctor <b class="text-danger">*</b></label>
										<div class="col-md-8 padding-0">
											<select data-style="btn-sm btn btn-default" name="doctor" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"></select>
										</div>
									</div>
									<div class="col-md-12">
										<label class="col-md-4 padding-left-0 control-label">Hygienis</label>
										<div class="col-md-8 padding-0">
											<select data-style="btn-sm btn btn-default" name="hygienis" class="form-control bs-select show-tick" data-live-search="true"></select>
										</div>
									</div>
									<div class="col-md-12">
										<label class="col-md-4 padding-left-0 control-label">Assistant</label>
										<div class="col-md-8 padding-0">
											<select data-style="btn-sm btn btn-default" name="assistant[]" multiple class="form-control bs-select show-tick" data-size="5" data-live-search="true"></select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<div class="col-md-4">
											<div class="app-checkbox">
												<input type="hidden" name="new" value="0">
												<label><input type="checkbox" name="new" value="1">New Patient</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="app-checkbox">
													<input type="hidden" name="needs" value="0">
												<label><input type="checkbox" name="needs" value="1">Special needs</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="app-checkbox">
												<input type="hidden" name="confirmed" value="0">
												<label><input type="checkbox" name="confirmed" value="1">Confirmed</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="app-checkbox">
												<input type="hidden" name="alldaytime" value="0">
												<label><input type="checkbox" name="alldaytime" value="1">All Day</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="app-checkbox">
												<input type="hidden" name="sedation" value="0">
												<label><input type="checkbox" name="sedation" value="1">Sedation</label>
											</div>
										</div>
										<div class="col-md-4">
											<div class="app-checkbox">
												<input type="hidden" name="inclinic" value="0">
												<label><input type="checkbox" name="inclinic" value="1">In Clinic</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-12 control-label">Sedation note</label>
									<div class="col-md-12">
										<input type="text" name="sedationnote" class="form-control input-sm">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12 control-label">Notes <b class="text-danger">*</b></label>
									<div class="col-md-12">
										<textarea name="notes" class="form-control" rows="5" data-validation-depends-on="needs"></textarea>
									</div>
								</div>
								<div class="form-group"  style="max-height:300px;" >
									<div class="col-md-4">
										<label class="col-md-12 control-label">Reason visit</label>
									</div>
									<div class="col-md-12">
										<select name="reason" class="form-control bs-select show-tick" data-style="btn-sm btn btn-default" data-size="5" data-live-search="true"></select>
									</div>
									<div class="col-md-12" id="listProceduresScrol"style="max-height: 300px;" >
										<ul id="listProcedures" class="list-group"></ul>
									</div>
								</div>
							</div>
							<div class="form-group text-right">
								<button id="backdeal" type="button" class="btn btn-warning btn-icon-fixed btn-sm" data-dismiss="modal"><span class="fa"><i class="fa fa-usd"></i></span> Regresar a ventas</button>
								<button type="submit" class="btn btn-success btn-icon-fixed btn-sm"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="tabproce">
						<form id="form_appproce"  autocomplete="off" class="form-horizontal" action="Api_cita/citasproce" method="PUT">
							<input type="hidden" name="id">
							<div class="form-group">
								<label class="col-md-12 control-label">Services Treat Plan</label>
								<div class="col-md-12" id="editappproce" style="height:250px;">
									<input type="checkbox" name="procupd" value="1" style="display:none;">
									<table id="tp_appt_proce_edit" class="table table-bordered" width="100%" style="font-size: x-small;">
										<thead>
											<tr>
												<th class="padding-5 padding-top-0 padding-bottom-0">Time</th>
												<th class="padding-5 padding-top-0 padding-bottom-0">Code</th>
												<th class="padding-5 padding-top-0 padding-bottom-0">Description</th>
												<th class="padding-5 padding-top-0 padding-bottom-0 text-success"></th>
											</tr>
										</thead>
										<tbody></tbody>
										<tfoot>
											<tr>
												<th colspan="3" class="padding-5 padding-top-0 padding-bottom-0 text-right"></th>
												<th class="padding-5 padding-top-0 padding-bottom-0 text-right"></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="form-group margin-left-5 text-right">
								<button type="submit" class="btn btn-success btn-icon-fixed btn-sm"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="tabitinerario">
						<form id="form_edit_flight"  autocomplete="off" class="form-horizontal" action="Api_cita/aptflight" method="POST">
							<input type="hidden" name="id">
							<input type="hidden" name="patient">
							<div class="form-group">
								<label class="col-md-4 control-label">File</label>
								<div class="col-md-8">
								<div class="input-group">
                                            <input name="file" type="file" class="form-control input-sm margin-0">
                                            <span class="input-group-btn">
                                                <button disabled id="itinerarioPreview" type="button" class="btn btn-default btn-sm preview margin-0" data-preview-image=""><i class="fa fa-eye fa-lg"></i></button>
                                            </span>
                                        </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Vuelo</label>
								<div class="col-md-8">
									<input name="flight" type="text" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Date</label>
								<div class="col-md-8">
									<input name="date" type="text" class="form-control input-sm bs-datepickerusa">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Hora salida</label>
								<div class="col-md-4">
									<input name="horas" type="text" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Hora Llegada</label>
								<div class="col-md-4">
									<input name="horal" type="text" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Note</label>
								<div class="col-md-8">
									<textarea name="note" rows="5" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group margin-left-5 text-right">
								<button type="submit" class="btn btn-success btn-icon-fixed btn-sm"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_help" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-info" role="document">
		<div class="modal-content">
		<form id="form_editappointment"  autocomplete="off" class="form-horizontal" action="Api_cita/citas" method="PUT">
			<input type="hidden" name="id">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Help</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Square on appointments </label>
					<div class="col-md-8">
						<i class="fa fa-square text-orange-500" aria-hidden="true"></i> : Appointment is not confirmed<br>
						<i class="fa fa-square text-green-400" aria-hidden="true"></i> : Appointment is confirmed
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-primary">Close</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        