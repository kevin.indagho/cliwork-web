	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Treat Plan</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li class="active">Treatplan</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div id="msgprocplanapt" class="col-md-12" style="display:none;">
			<div class="alert alert-warning alert-icon-block alert-dismissible" role="alert"> 
				<div class="alert-icon"><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i></div>
				<strong>Info!</strong> Algunos procedimientos no estan asignados a una cita. 
			</div>
		</div>
		<div class="col-md-6" style="min-width:550px;">
			<div id="odontogram" class="block block-condensed block-danger">
				<div class="app-heading app-heading-small">
					<div class="heading-elements">
						<input type="hidden" id="treatplanid" value="<?=$tpid?>">
					</div>
				</div>                                    
				<div class="block-content">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tabs_proce" data-toggle="tab">Procedures</a></li>
						<li><a href="#tabs_planndappts" data-toggle="tab">Planned Appts</a></li>
					</ul>
					<div class="tab-content" style="height:334px;">
						<div class="tab-pane active" id="tabs_proce" style="height:334px;">
							<form id="form_procedure">
								<div class="form-group margin-bottom-0">
									<div class="col-md-4">
										<label class="control-label">Entry Status</label>										
										<select class="form-control bs-select" name="status" data-style="btn-sm btn btn-default" data-validation="required">
											<option value="1" selected>Treatplan</option>
											<option value="2" >Complete</option>
											<option value="3" >ExistCurProv</option>
											<option value="4" >ExistOther</option>
											<option value="5" >Referred</option>
											<option value="7" >Condition</option>
										</select>
									</div>
									<div class="col-md-4">
										<label class="control-label">Provider</label>
										<select class="form-control bs-select" name="doctor" data-style="btn-sm btn btn-default" data-validation="required">
											<option></option>
										</select>
									</div>
									<div class="col-md-4">
										<div class="form-group margin-bottom-0">
											<label class="col-md-12 control-label">Others</label>
											<button data-toggle="modal" data-target="#modal_procedures" type="button" class="btn btn-primary btn-glo btn-sm">Procedure list</button>
											<button onclick="opencdr();" type="button" class="btn btn-default btn-glo btn-sm"><img class="xrayicon" width="24px" height="24px" /> CDR dicom</button>
										</div>
									</div>
								</div>
								<div id="quickbuttons" class="col-md-12"></div>
							</form>
						</div>
						<div class="tab-pane" id="tabs_planndappts">
							<table id="dt_plaappt" class="table table-bordered table-head-custom" width="100%">
								<thead>
									<tr>
										<th>Date</th>
										<th>Doctor</th>
										<th>Procedures</th>
										<th></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6" style="min-width:550px;">
			<div id="odontogram" class="block block-condensed block-danger">
				<div class="app-heading app-heading-small">                                        
					<div class="title">
						<h2>Odontogram</h2>
					</div>
					<div class="heading-elements">
						<button data-toggle="modal" data-target="#modal_config_odon" class="btn btn-default btn-sm btn-icon dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></button>
						<input type="hidden" id="treatplanid" value="<?=$tpid?>">
					</div>
				</div>                                    
				<div class="block-content text-center">
					<?php echo file_get_contents(base_url()."img/svg/odontograma.svg"); ?>
				</div>
			</div>
		</div>
		
		<div class="col-md-12" style="min-width:550px;">
			<div class="block block-condensed block-arrow-top">
				<div class="app-heading app-heading-small">                                        
					<div class="title">
						<h2>Procedures List</h2>
					</div>
					<div class="heading-elements">
						<select class="form-control input-sm" id="proceshow">
							<option value="all">All procedures</option>
							<option value="1" selected>Treatment plan</option>
							<option value="2">Complete</option>
							<option value="3">ExistCurProv</option>
							<option value="4">ExistOther</option>
							<option value="5">Referred</option>
							<option value="6">Delete</option>
							<option value="7">Condition</option>
						</select>
					</div>
				</div>                                    
				<div class="block-content">
					<table id="dt_tpproc" class="table table-bordered table-head-custom table-condensed" width="100%">
						<thead>
							<tr>
								<th>Done</th>
								<th>Date</th>
								<th>Priority</th>
								<th>Tooth</th>
								<th>Surface</th>
								<th>Description</th>
								<th>Prov.</th>
								<th>Amount</th>
							</tr>
						</thead>
						<!--<tfoot>
							<tr>
								<th colspan="6"></th>
								<th class="text-right"></th>
							</tr>
						</tfoot>-->
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>
	
	<div class="modal fade" id="modal_procedures" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-info modal-lg" role="document">
			<div class="modal-content">
			<form id="form_configpati"  autocomplete="off" class="form-horizontal" action="" method="post">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">Procedure List</h4>
				</div>
				<div class="modal-body">
					<div class="col-md-12">
						<label class="control-label">Provider</label>
						<select id="doctorproclist" class="form-control bs-select" data-style="btn-sm btn btn-default" data-validation="required">
							<option></option>
						</select>
						<hr style="border-top: 1px solid #000000">
					</div>
					<table id="dt_procedurelist" class="table table-head-custom table-hover" width="100%">
						<thead>
							<tr>
								<th>Category</th>
								<th>Code</th>
								<th>Abbr</th>								
								<th>Description</th>								
								<th>$</th>								
							</tr>
						</thead>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<!--<button type="submit" class="btn btn-success">Save</button>-->
				</div>
			</form>
			</div>
		</div>            
	</div>
	
	<div class="modal fade" id="modal_procedures_edit" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
		<div class="modal-dialog modal-warning" role="document">
			<div class="modal-content">
			<form id="form_procedure_edit"  autocomplete="off" class="form-horizontal" action="Api_treatplan/tpprocedure" method="put">
				<input name="id" type="hidden">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-danger-header">Edit Procedure</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-4 control-label">Code</label>
						<div class="col-md-8">
							<input name="code" type="text" class="form-control input-sm" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Description</label>
						<div class="col-md-8">
							<textarea name="description" rows="5" class="form-control" disabled></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Status</label>
						<div class="col-md-8">
							<select data-style="btn-sm btn btn-default" name="status" class="form-control bs-select show-tick" data-validation="required">
								<option value="1">Treatment Plan</option>
								<option value="2">Complete</option>
								<option value="3">Existing Current Provider</option>
								<option value="4">Existing Other Provider</option>
								<option value="5">Referred Out</option>
								<option value="6">Delete</option>
								<option value="7">Condition</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Doctor</label>
						<div class="col-md-8">
							<select data-style="btn-sm btn btn-default" name="doctor" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"></select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Fee</label>
						<div class="col-md-4">
							<select data-style="btn-sm btn btn-default margin-top-0" name="feesched" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"></select>
						</div>
						<div class="col-md-4">
							<div class="input-group input-group-sm margin-top-0">
								<span class="input-group-addon"><i class="fa fa-usd"></i></span>
								<input name="fee" type="text" class="form-control input-sm" readonly>
							</div>
						</div>
					</div>
					<div class="form-group">		
						<label class="col-md-4 control-label"></label>
						<div class="col-md-2">
							<label class="control-label">Priority</label>
							<select name="priority" class="form-control input-sm">
								<option value="0">None</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-2">
							<label class="control-label">Tooth <i class="fa fa-exclamation-circle text-danger" aria-hidden="true"></i></label>
							<input name="tooth" type="text" class="form-control input-sm" data-validation="required">
						</div>
						<div class="col-md-2">
							<label class="control-label">Surf</label>
							<input name="surf" type="text" class="form-control input-sm" data-validation="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Notes</label>
						<div class="col-md-8">
							<textarea name="note" rows="5" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</form>
			</div>
		</div>            
	</div>
		
	<div class="modal fade" id="modal_add_planappt" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">
		<div class="modal-dialog modal-success" role="document">
			<div class="modal-content">
			<form id="form_newappointment"  autocomplete="off" class="form-horizontal" action="Api_cita/citas" method="post">
				<input type="hidden" name="inbox" value="0">
				<input type="hidden" name="planned" value="0">
				<input type="hidden" name="patient[id]" value="">
				<input type="hidden" name="status" value="7">
				<div class="modal-header">                        
					<h4 class="modal-title" id="modal-primary-header">New Appointment</h4>
					<div class="heading-elements text-right">
						<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
					</div>
				</div>
				<div class="modal-body">
					<div id="errormsg"></div>
					<ul class="nav nav-tabs">
						<li class="active"><a id="indextabpat" href="#tabpat" data-toggle="tab">Patient</a></li>
						<li><a id="indextabaappt" href="#tabaappt" data-toggle="tab">Appointment</a></li>
						<li><a id="indextabaapptproc" href="#tabaapptproc" data-toggle="tab">Procedures Attach</a></li>
					</ul>
					<div class="tab-content tab-content-bordered">
						<div class="tab-pane active" id="tabpat">
							<div class="form-group">
								<label class="col-md-4 control-label">Name</label>
								<div class="col-md-8">
									<input name="patient[nombre]" type="text" class="form-control input-sm" data-validation="required">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Last Name</label>
								<div class="col-md-8">
									<input name="patient[apellidos]" type="text" class="form-control input-sm" data-validation="required">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input name="patient[correo]" class="form-control input-sm" data-validation="required" data-validation="email"
									data-validation-optional-if-answered="patient[casatelefono],patient[trabajotelefono],patient[moviltelefono]">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Home Phone</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-home"></i></span>
										<input name="patient[casatelefono]" type="text" class="form-control input-sm" 
										data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="patient[correo],patient[trabajotelefono],patient[moviltelefono]" >
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Work Phone</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-building"></i></span>
										<input name="patient[trabajotelefono]" type="text" class="form-control input-sm"
										data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="patient[correo],patient[casatelefono],patient[moviltelefono]">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Mobile Phone</label>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-phone"></i></span>
										<input name="patient[moviltelefono]" type="text" class="form-control input-sm"
										data-validation="number" data-validation-ignore="*+#" data-validation-optional-if-answered="patient[correo],patient[casatelefono],patient[trabajotelefono]">
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tabaappt">
							<div class="form-group">
								<label class="col-md-12 control-label">Date Appointment</label>
								<div class="col-md-6">
									<input name="date" class="form-control input-sm bs-datepickerusa_dis_past" data-validation="required">
									<input type="hidden" name="time" value="9:00:00 am - 9:30:00 am">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<!--<div class="col-md-12">
										<label class="control-label col-md-12"></label>
										<button id="cancelAppt" type="button" class="btn btn-danger btn-glow btn-sm">Cancel</button>
									</div>-->
								</div>
								<div class="col-md-12">
									<div class="col-md-4">
										<div class="app-checkbox">
											<input type="hidden" name="new" value="0">
											<label><input type="checkbox" name="new" value="1">New Patient</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="app-checkbox">
												<input type="hidden" name="needs" value="0">
											<label><input type="checkbox" name="needs" value="1">Special needs</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="app-checkbox">
											<input type="hidden" name="confirmed" value="0">
											<label><input type="checkbox" name="confirmed" value="1">Confirmed</label>
										</div>
									</div>
									<div class="col-md-4">
										<div class="app-checkbox">
											<input type="hidden" name="sedation" value="0">
											<label><input type="checkbox" name="sedation" value="1">Sedation</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12 control-label">Sedation note</label>
								<div class="col-md-12">
									<input type="text" name="sedationnote" class="form-control input-sm">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label class="col-md-4 padding-left-0 control-label">Doctor <b class="text-danger">*</b></label>
									<div class="col-md-8 padding-0">
										<select data-style="btn-sm btn btn-default" name="doctor" class="form-control bs-select show-tick" data-size="5" data-live-search="true" data-validation="required"></select>
									</div>
								</div>
								<div class="col-md-12">
									<label class="col-md-4 padding-left-0 control-label">Hygienis</label>
									<div class="col-md-8 padding-0">
										<select data-style="btn-sm btn btn-default" name="hygienis" class="form-control bs-select show-tick" data-size="5" data-live-search="true"></select>
									</div>
								</div>
								<div class="col-md-12">
									<label class="col-md-4 padding-left-0 control-label">Assistant</label>
									<div class="col-md-8 padding-0">
										<select data-style="btn-sm btn btn-default" name="assistant[]" multiple class="form-control bs-select show-tick" data-size="5" data-live-search="true"></select>
									</div>
								</div>
							</div>
							<div class="form-group">
									<label class="col-md-12 control-label">Notes <b class="text-danger">*</b></label>
									<div class="col-md-12">
										<textarea name="notes" class="form-control" rows="5" data-validation-depends-on="needs"></textarea>
									</div>
								</div>
						</div>
						<div class="tab-pane" id="tabaapptproc">
							<div class="form-group">
								<label class="col-md-12 control-label">Services Treat Plan<b class="text-danger">*</b></label>
								<div class="col-md-12">
									<table id="tp_appt_proce_add" class="table table-bordered" width="100%" style="font-size: x-small;">
										<thead>
											<tr>
												<th class="padding-5 padding-top-0 padding-bottom-0">Time</th>
												<th class="padding-5 padding-top-0 padding-bottom-0">Code</th>
												<th class="padding-5 padding-top-0 padding-bottom-0">Description</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>
				</div>
			</form>
			</div>
		</div>            
	</div>
	
	<div id="modal_config_odon" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">
		<div class="modal-dialog modal-success" role="document">
		</div>
	</div>