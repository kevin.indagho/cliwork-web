	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-list-ol"></i></span>
		<div class="title">
			<h2>List</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Users</li>
				<li class="active">List</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>List</h2>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtusers" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_newuser" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newuser"  autocomplete="off" class="form-horizontal" action="Api_usuario/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add User</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Last Name</label>
					<div class="col-md-8">
						<input name="apellidos" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
						<input name="correo" class="form-control input-sm" data-validation="required"  data-validation="email" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="tipo" class="bs-select" data-live-search="true" data-validation="required"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="activo" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="activo" checked="" value="1">
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-8">
						<input name="contrasena" type="password" class="form-control input-sm" data-validation="required">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_edituser" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_edituser" autocomplete="off" class="form-horizontal" action="Api_usuario/editar" method="post">
			<input name="id" type="hidden" data-validation="required">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit User</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Last Name</label>
					<div class="col-md-8">
						<input name="apellidos" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
						<input name="correo" class="form-control input-sm" data-validation="required" data-validation="email">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="tipo" class="bs-select" data-live-search="true" data-validation="required"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Status</label>
					<div class="col-md-8">
						<input style="display:none;" type="checkbox" name="activo" checked="" value="0">
						<label class="switch switch-sm switch-cube">
							<input type="checkbox" name="activo" value="1">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        