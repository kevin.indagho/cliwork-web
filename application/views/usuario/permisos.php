	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-ban"></i></span>
		<div class="title">
			<h2>Permissions</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Users</li>
				<li class="active">Permissions</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">
						<div class="heading-elements">
							<button class="btn btn-success visible-mobile btn-sm btn-icon"><span class="fa fa-plus"></span></button>
							<button class="btn btn-default btn-sm btn-icon dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></button>
							<ul class="dropdown-menu dropdown-left">
								<li class="dropdown-header highlight"><i class="fa fa-eye-slash" aria-hidden="true"></i> Hide Columns</li>
								<li><a><div class="app-checkbox"><label><input class="hc" data-column="0" type="checkbox" checked="">Name<span></span></label></div></a></li>
								<li><a><div class="app-checkbox"><label><input class="hc" data-column="1" type="checkbox" checked="">Status<span></span></label></div></a></li>
								<li class="dropdown-header highlight"><span class="fa fa-cloud-download"></span> Export Data</li>
								<li><a id="btnEXC"><i class="fa fa-file-excel-o"></i> Excel</a></li>
								<li class="divider"></li>
								<li><a id="btnPDF"><i class="text-danger fa fa-file-pdf-o"></i> PDF</a></li>
							</ul>
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>
						<div class="title">
							<p class="hidden-mobile"><button data-backdrop="static" data-toggle="modal" data-target="#modal_newtype" class="btn btn-success btn-sm btn-icon-fixed"><span class="fa fa-plus"></span>New Type</button></p>
						</div>
					</div>
					<div class="block-content">
						<table id="dttypes" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Name</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_newuser" tabindex="-1" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_newuser"  autocomplete="off" class="form-horizontal" action="Api_usuario/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add User</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Last Name</label>
					<div class="col-md-8">
						<input name="apellidos" type="text" class="form-control input-sm" data-validation="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
						<input name="correo" class="form-control input-sm" data-validation="required"  data-validation="email" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="tipo" class="bs-select" data-live-search="true" data-validation="required"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-8">
						<input name="contrasena" type="password" class="form-control input-sm" data-validation="required">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editpermissions" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
			<input name="id" type="hidden" data-validation="required">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Edit Permission</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabtype" data-toggle="tab">Type</a></li>
					<li><a href="#tabPermi" data-toggle="tab">Permission</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabtype">
					<form id="form_edittype" autocomplete="off" class="form-horizontal" action="Api_usuario" method="post">
						<input type="hidden" name="id">
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Status</label>
							<div class="col-md-8">
								<input style="display:none;" type="checkbox" name="activo" checked="" value="0">
								<label class="switch switch-sm switch-cube">
									<input type="checkbox" name="activo" value="1">
								</label>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</form>
					</div>
					<div class="tab-pane" id="tabPermi">
						<form id="form_editpermission" autocomplete="off" class="form-horizontal" action="Api_usuario" method="post">
							<div class="alert alert-info alert-icon-block alert-dismissible" role="alert"> 
								<div class="alert-icon">
									<span class="icon-warning"></span>
								</div>
								<strong>Info!</strong> These changes will apply when the user logs out. 
							</div>
							
							<input type="hidden" name="id">
							<table id="dtpermissions" width="100%" class="table table-bordered table-head-custom">
								<thead>
									<tr>
										<th>Menu</th>
										<th>SubMenu</th>
										<th>Status</th>
									</tr>
								</thead> 
							</table>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_newtype" tabindex="-2" role="dialog" aria-labelledby="modal-primary-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
			<input name="id" type="hidden" data-validation="required">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-primary-header">Add Type User</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabtype" data-toggle="tab">Type</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabtype">
					<form id="form_newtype" autocomplete="off" class="form-horizontal" action="Api_usuario" method="post">
						<input type="hidden" name="id">
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<input name="nombre" type="text" class="form-control input-sm" data-validation="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Status</label>
							<div class="col-md-8">
								<input style="display:none;" type="checkbox" name="activo" checked="" value="0">
								<label class="switch switch-sm switch-cube">
									<input type="checkbox" name="activo" value="1">
								</label>
							</div>
						</div>
						<div class="form-group text-right">
							<button type="submit" class="btn btn-success btn-sm btn-icon-fixed"><span class="fa fa-floppy-o"></span>Save</button>
						</div>
					</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>            
</div>
        