<!DOCTYPE html>
<html lang="en">
    <head>    
        <title>Cliwork</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>        
        <div class="app">
            <div class="app-container">
                <div class="app-login-box">
					<div style="width: 100%; float: left; text-align: center; margin-bottom: 20px; padding: 30px 30px 0px;">
						<img style="width: 100%;" src="<?=base_url('img/logo.png')?>" alt="Logo">
					</div>
                    <div class="app-login-box-title">
                        <div class="title">Sign in to your account</div>                        
                    </div>
                    <div class="app-login-box-container">
                        <form id="form_login" action="Cuenta/autenticar" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="correo" data-validation="required"  data-validation="email" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="contrasena" data-validation="required" placeholder="Password">
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <button type="submit" class="btn btn-success btn-block">Sign In</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="app-login-box-footer">&copy; Indagho 2017. All rights reserved.</div>
                </div>
				
				<div id="msgsuccess" style="display:none;" class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="alert alert-success alert-icon-block" role="alert">
							<div class="alert-icon">
								<span class="icon-checkmark-circle"></span> 
							</div>
							<strong>Success!</strong> You successfully read this important alert message. 
						</div>                                           
					</div>
				</div>
				
				<div id="errorsuccess" style="display:none;" class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<div class="alert alert-info alert-icon-block" role="alert"> 
							<div class="alert-icon">
								<i class="fa fa-exclamation fa-2x" aria-hidden="true"></i>
							</div>
							<strong>Ops!</strong> Check the password and email. 
						</div>                                            
					</div>
				</div>
                                
            </div>
        </div>        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/moment/moment.min.js"></script>
		
        <script type="text/javascript" src="<?=base_url()?>js/vendor/customscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-select/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/select2/select2.full.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/maskedinput/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/form-validator/jquery.form-validator.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/noty/jquery.noty.packaged.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/datatables/dataTables.bootstrap.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/knob/jquery.knob.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jvectormap/jquery-jvectormap-us-aea-en.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/sparkline/jquery.sparkline.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/morris/raphael.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/morris/morris.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/rickshaw/rickshaw.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/isotope/isotope.pkgd.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/dropzone/dropzone.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/nestable/jquery.nestable.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/cropper/cropper.min.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/tableExport.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jquery.base64.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/html2canvas.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/tableexport/jspdf/libs/base64.js"></script>
        
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-daterange/daterangepicker.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-tour/bootstrap-tour.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/smartwizard/jquery.smartWizard.js"></script>
        

		<script type="text/javascript" src="<?=base_url()?>js/pages/global.js"></script>
        <script>
			$(document).ready(function(){
				$.validate({
					form : '#form_login',
					onError : function($form) {
						alert('error');
					},
					onSuccess : function($form){
						$.ajax({
							url: base_url+$form.attr('action'),
							type: $form.attr('method'),
							data: $form.serialize(),
							dataType: "json",
							success: function(response){
								if(response.status == true){									
									var data = $form.serializeArray();
									login(data,response.data,response.nombre);
								}else{
								}
							}
						});
					  return false; // Will stop the submission of the form
					},
				});
			});
			
			function login(formData,data,nombre){
				$.ajax({
					url: urlapi+"Api_usuario/login",
					type: "post",
					data: formData,
					dataType: "json",
					headers:{'dbconfig': data,'Authorization':auth},
					success: function(response){
						if(response.status == true){									
							newSession(response,data,nombre);
						}else{
						}
					},error: function(error){
						console.log(error);
					}
				});
			}
			
			function newSession(formData,db,nombre){
				let data = formData.data;
				data.db = db;
				data.nombreid = nombre;
				$.ajax({
					url: base_url+"Cuenta/crear_session",
					type: "post",
					data: data,
					dataType: "json",
					success: function(response){
						console.log(response);
						if(response.status == true){
							location.reload();
						}else{
						}
					}
				});
			}
		</script>
    </body>
</html>