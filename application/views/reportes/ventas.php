
	
	<!--<div class="app-heading app-heading-bordered app-heading-page">
		<div class="icon icon-lg">
			<span class="icon-home"></span>
		</div>
		<div class="title">
			<h1>Users</h1>
			<p>List of users</p>
		</div>
	</div>-->
	
	<div class="app-heading-container app-heading-bordered bottom">
		<ul class="breadcrumb">
			<li><a href="<?=base_url()?>">Home</a></li>
			<li>Reports</li>
			<li class="active">Deals</li>
		</ul>
	</div>
	
	<div class="container">
	
		<div class="row">
			<div class="col-md-2">
				<div class="app-widget-tile app-widget-tile-success">                                            
					<div id="totalInformer" class="intval intval-lg odometer">0</div>
					<div class="line">
						<div class="title wide text-center">Total</div>                                                
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>List</h2>
						</div>                 
						<div class="heading-elements">
							<button data-backdrop="static" data-toggle="modal" data-target="#modal_dtpatientconfig" class="btn btn-default btn-sm btn-icon"><span class="fa fa-cog"></span></button>
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtdeals" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Date</th>
									<th>Days</th>
									<th>User</th>
									<th>Patient</th>
									<th>Step</th>
									<th>Value</th>
									<th>Activity</th>
									<th>Activity Days</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_opendeal" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-info" role="document">
		<div class="modal-content">
		<form id="form_deal" class="form-horizonta">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Deal Lost</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabnewdeal" data-toggle="tab">Patient</a></li>
					<li><a href="#tadealdetail" data-toggle="tab">Deal Details</a></li>
					<li><a href="#tabStage" data-toggle="tab">Stage</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabnewdeal">
						<div class="form-group">
							<label class="col-md-4 control-label">Patient <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="patient" class="form-control input-sm" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
								<input name="email" class="form-control input-sm" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-home"></i></span>
									<input name="phome" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<input name="pwork" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									<input name="pmobile" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tadealdetail">
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label">Procedures Attach</label>
							</div>
							<div class="col-md-12 scrollProcedures" style="height: 300px;" >
								<ul id="listProcedures" class="list-group"></ul>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Value</label>
							<div class="col-md-4">
								<input type="text" class="form-control input-sm" name="value" disabled>
							</div>
							<div class="col-md-4">
								<select class="form-control bs-selec input-sm" name="typevalue" disabled>
									<option>US Dollar (USD)</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Expected close date</label>
							<div class="col-md-4">
								<input type="text" class="form-control bs-datepickerusa input-sm" name="closedate" disabled>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tabStage" style="height: 300px;">
						<table id="dtstagecontrol" class="table table-bordered table-head-custom" width="100%">
							<thead>
								<th>Name</th>
								<th>Date Start</th>
								<th>Date End</th>
								<th>Days Duration</th>
								<th>Days Stage</th>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<!--<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>-->
				<button type="button" class="dealdetails btn btn-info btn-sm btn-icon-fixed"><span class="fa fa-info"></span>Details</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        