
	
	<!--<div class="app-heading app-heading-bordered app-heading-page">
		<div class="icon icon-lg">
			<span class="icon-home"></span>
		</div>
		<div class="title">
			<h1>Users</h1>
			<p>List of users</p>
		</div>
	</div>-->
	
	<div class="app-heading-container app-heading-bordered bottom">
		<ul class="breadcrumb">
			<li><a href="<?=base_url()?>">Home</a></li>
			<li>Reports</li>
			<li class="active">Activities</li>
		</ul>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>List</h2>
						</div>                 
						<div class="heading-elements">
							<button data-backdrop="static" data-toggle="modal" data-target="#modal_dtpatientconfig" class="btn btn-default btn-sm btn-icon"><span class="fa fa-cog"></span></button>
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dtactivities" width="100%" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th></th>
									<th>Expired Days</th>
									<th>Patient</th>
									<th>Deal</th>
									<th>Value</th>
									<th>User</th>
									<th></th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_dtpatientconfig" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-danger" role="document">
		<div class="modal-content">
		<form id="form_configpatient"  autocomplete="off" class="form-horizontal" action="Api_paciente/agregar" method="post">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Config Table Patient</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Nombre</label>
					<div class="col-md-8">
						<label class="switch switch-cube">
							<input type="checkbox" name="nombre" checked="" value="0">
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<!--<button type="submit" class="btn btn-success">Save</button>-->
			</div>
		</form>
		</div>
	</div>            
</div>
        