	<div class="app-heading-container app-heading-bordered bottom">
		<ul class="breadcrumb">
			<li><a href="#">Others</a></li>                                                     
			<li class="active">Current</li>
		</ul>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Saldo pendiente</h4>
					<form id="form_rp_patdue" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download report</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Report annual</h4>
					<form id="form_rp_anual" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<label class="col-md-12 control-label">Date (Year)</label>
							<div class="col-md-12">
								<input placeholder="YYYY" name="date" type="text" class="form-control input-sm" data-validation="number">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download report</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Report production</h4>
					<form id="form_rp_production" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<label class="col-md-12 control-label">Date</label>
							<div class="col-md-12">
								<input name="date" type="text" class="form-control input-sm daterange">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download report</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Daily payments</h4>
					<form id="form_rp_daily_payments" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<label class="col-md-12 control-label">Date</label>
							<div class="col-md-12">
								<input name="date" type="text" class="form-control input-sm daterange">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download report</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Daily procedures</h4>
					<form id="form_rp_daily_procedures" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<label class="col-md-12 control-label">Date</label>
							<div class="col-md-12">
								<input name="date" type="text" class="form-control input-sm daterange">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download report</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Production procedures</h4>
					<form id="form_rp_production_procedures" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<label class="col-md-12 control-label">Date</label>
							<div class="col-md-12">
								<input name="date" type="text" class="form-control input-sm daterange">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-12 control-label">Limit</label>
							<div class="col-md-12">
								<input name="limit" type="text" class="form-control input-sm" data-validation="number">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download report</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!--<div class="col-md-3">
				<div class="block">
				<h4 class="text-uppercase text-bold text-rg heading-line-below">Envelope template</h4>
					<form id="form_rp_envelope_template" class="form-horizontal" action="" method="get">
						<div class="form-group">
							<label class="col-md-12 control-label">Search patient</label>
							<div class="col-md-10">
								<select name="patient" class="form-control" data-style="btn-sm btn btn-default" data-size="10" data-live-search="true"></select>
							</div>
							<div class="col-md-2">
								<button id="btn_clr_list" type="button" class="btn btn-info btn-xs">clear list</button>
							</div>
							<div id="patlist" class="col-md-12" style="height:100px;">
								<ul id="ulpatlist" class="list-group"></ul>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Logo</label>
							<div class="col-md-4">
								<div class="app-radio round"> 
									<label><input type="radio" name="logo" value="1">Yes</label> 
								</div>
							</div>
							<div class="col-md-4">
								<div class="app-radio round"> 
									<label><input type="radio" name="logo" value="0" checked="checked">No</label> 
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-danger btn-block">Download pdf</button>
							</div>
						</div>
					</form>
				</div>
			</div>-->
		</div>
	</div>
</div>

            