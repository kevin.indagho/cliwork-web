	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-users"></i></span>
		<div class="title">
			<h2>Procedure reminders</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>reports</li>
				<li class="active">Procedure reminders</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">
							<h2>Procedures reminders</h2>
						</div>
					</div>
					<div class="block-content">
						<table id="dtreminders" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Procedure</th>
									<th>Days</th>
									<th>Type</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_addreminder" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-success" role="document">
		<div class="modal-content">
		<form id="form_addreminder"  autocomplete="off" class="form-horizontal" action="Api_data/procereminders" method="post">
			<input type="hidden" name="id">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Add reminder</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure search</label>
					<div class="col-md-8">
						<select name="procedureid" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure</label>
					<div class="col-md-8">
						<input class="form-control" name="procedure" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Days</label>
					<div class="col-md-8">
						<input class="form-control" name="days" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="type" class="form-control">
							<option value="1">SMS</option>
							<option value="2">Email</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Message</label>
					<div class="col-md-12">
						<textarea name="message" class="form-control summernote"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editreminder" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editreminder"  autocomplete="off" class="form-horizontal" action="Api_data/procereminders" method="put">
			<input type="hidden" name="id">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Edit reminder</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure search</label>
					<div class="col-md-8">
						<select name="procedureid" class="form-control" data-style="btn-sm btn btn-default" data-size="5" data-live-search="true"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Procedure</label>
					<div class="col-md-8">
						<input class="form-control" name="procedure" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Days</label>
					<div class="col-md-8">
						<input class="form-control" name="days" >
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Type</label>
					<div class="col-md-8">
						<select name="type" class="form-control">
							<option value="1">SMS</option>
							<option value="2">Email</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Message</label>
					<div class="col-md-12">
						<textarea name="message" class="form-control summernote"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>