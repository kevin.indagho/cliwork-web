	
	<!-- START PAGE HEADING -->
	<!--<div class="app-heading app-heading-bordered app-heading-page">
		<div class="icon icon-lg">
			<span class="icon-home"></span>
		</div>
		<div class="title">
			<h1>Blank</h1>
			<p>Subtitle</p>
		</div>
	</div>-->
	<div class="app-heading-container app-heading-bordered bottom">
		<ul class="breadcrumb">
			<li class="active">Home</li>
		</ul>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="block">
					<div style="float:left; height:250px; width:100%;">
						<canvas id="g_origins"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="block">
					<div style="float:left; height:250px; width:100%;">
						<canvas id="chartjs_bar"></canvas>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="block">
					<div style="float:left; height:250px; width:100%;">
						<canvas id="g_procedimientos"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="block">
					<div style="float:left; height:250px; width:100%;">
						<canvas id="g_procedimientosmoney"></canvas>
					</div>
				</div>
			</div>
		<div/>
		<div class="row">
			<div class="col-md-12">
				<div class="block">
					<div style="float:left; height:250px; width:100%;">
						<canvas id="g_provingresos"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

            