	<style>
	.button_example{
		/*background-size: contain;
		background-position: right;
		background-repeat: no-repeat;
		background-image: url(https://terapiametabolica.com/wp-content/uploads/2013/03/left-arrow-right-hi.png);
		*/
		min-width: 250px;
		background-color:#F5F5F5;
		/*border:1px solid #cacaca; 
		font-size:12px;font-family:arial, helvetica, sans-serif; 
		padding: 10px 10px 10px 10px; 
		color:#FFFFFF;
		font-weight:bold; color: #FFFFFF;
		background-color: #E6E6E6; background-image: -webkit-gradient(linear, left top, left bottom, from(#E6E6E6), to(#CCCCCC));
		background-image: -webkit-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -moz-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -ms-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -o-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: linear-gradient(to bottom, #E6E6E6, #CCCCCC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E6E6E6, endColorstr=#CCCCCC);*/
	}
	</style>
	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-usd"></i></span>
		<div class="title">
			<h2>Deals lost</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Patient</li>
				<li class="active">Deals Lost</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="app-widget-tile app-widget-tile-danger">                                            
					<div id="totalLostInformer" class="intval intval-lg odometer">0</div>
					<div class="line">
						<div class="title wide text-center">Total Lost</div>                                                
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="block" id="block-expand">
					<div class="app-heading app-heading-small">                                        
						<div class="title">Deal Lost</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.expand('#block-expand');"><span class="fa fa-expand"></span></button>
						</div>                
					</div>
					<div class="block-content">
						<table id="dt_deallost" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Date</th>
									<th>Patient</th>
									<th>User</th>
									<th>Date Lost</th>
									<th>Reazon</th>
									<th>Value</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>						
		</div>
</div>

<div class="modal fade" id="modal_opendeal" tabindex="-1" role="dialog" aria-labelledby="modal-danger-header">                        
	<div class="modal-dialog modal-info" role="document">
		<div class="modal-content">
		<form id="form_deal" class="form-horizonta">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Deal Lost</h4>
				<div class="heading-elements text-right">
					<button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-icon"><span class="fa fa-times"></span></button>
				</div>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tabnewdeal" data-toggle="tab">Patient</a></li>
					<li><a href="#tadealdetail" data-toggle="tab">Deal Details</a></li>
				</ul>
				<div class="tab-content tab-content-bordered">
					<div class="tab-pane active" id="tabnewdeal">
						<div class="form-group">
							<label class="col-md-4 control-label">Patient <b class="text-danger">*</b></label>
							<div class="col-md-8">
								<input name="patient" class="form-control input-sm" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-8">
								<input name="email" class="form-control input-sm" disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Home Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-home"></i></span>
									<input name="phome" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Work Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-building"></i></span>
									<input name="pwork" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone</label>
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
									<input name="pmobile" type="text" class="form-control input-sm" disabled>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tadealdetail">
						<div class="form-group">
							<div class="col-md-4">
								<label class="control-label">Procedures Attach</label>
							</div>
							<div class="col-md-12 scrollProcedures" style="height: 300px;" >
								<ul id="listProcedures" class="list-group"></ul>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Value</label>
							<div class="col-md-4">
								<input type="text" class="form-control input-sm" name="value" disabled>
							</div>
							<div class="col-md-4">
								<select class="form-control bs-selec input-sm" name="typevalue" disabled>
									<option>US Dollar (USD)</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Expected close date</label>
							<div class="col-md-4">
								<input type="text" class="form-control bs-datepickerusa input-sm" name="closedate" disabled>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tablostdeals">
						<table id="dtlostdeals" class="table table-striped table-bordered" width="100%">
							<thead>
								<th></th>
								<th>Patient</th>
								<th>Deal</th>
								<th>Value</th>
								<th>User</th>
								<th></th>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<!--<button type="submit" class="btn btn-success btn-icon-fixed"><span class="fa"><i class="fa fa-floppy-o"></i></span> Save</button>-->
			</div>
		</form>
		</div>
	</div>            
</div>