<style>
</style>

	<div class="app-heading app-heading-bordered">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-usd"></i></span>
		<div class="title">
			<h2>Detail Deal</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url("patient/list")?>">Patients</a></li>
				<li><a href="<?=base_url("patient/deals")?>">Deals</a></li>
				<li class="active">details</li>
			</ul>
		</div>
	</div>
	
	<div class="app-content-separate app-content-separated-left">                        
		<div class="app-content-separate-left" data-separate-control-height="true">
			<div class="app-content-separate-panel padding-20 visible-mobile">
				<div class="pull-left">
					<h4 class="text-sm text-uppercase text-bolder margin-bottom-0">Patient Details</h4>
				</div>
				<button class="btn btn-default btn-icon pull-right" data-separate-toggle-panel=".app-content-separate-left"><span class="icon-menu"></span></button>
			</div>
			<div class="app-content-separate-content padding-20">
				<input id="ofertaid" value="<?=$ofertaid?>" type="hidden">
				<input id="pacienteid" value="<?=$pacienteid?>" type="hidden">
				<div class="col-xs-12">
					<h5>Contact</h5>
					<b>Patient : </b> <span id="patientdeal"></span><br>
					<b>Home Phone : </b> <span id="conHmPhone"></span><br>
					<b>Work Phone : </b> <span id="conWkPhone"></span><br>
					<b>Mobile Phone : </b> <span id="conWiPhone"></span><br>
					<b>Email : </b> <span id="conEmail"></span><br>
					<b>Address</b><br><span id="conAddress"></span>
					<hr style="border-top: 1px solid #000000">
					<b>Treatment </b><br> <span id="treatmentdeal"></span><br>
				</div>
			</div>
		</div>
		
		<div class="app-content-separate-content">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="app-widget-informer">
							<div class="title">Deal Value</div>
							<div class="intval"><p id="valuedeal" class="text-success">0</p></div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="app-widget-informer">
							<div class="title">Current Stage</div>
							<div class="intval" id="currentStage"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="block padding-0">
							<div id="smartwizard">
								<ul>
									<!--<li><a href="#step-1">Step 1<br /><small>This is step description</small></a></li>
									<li><a href="#step-2">Step 2<br /><small>This is step description</small></a></li>
									<li><a href="#step-3">Step 3<br /><small>This is step description</small></a></li>
									<li><a href="#step-4">Step 4<br /><small>This is step description</small></a></li>*/-->
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tabActi" data-toggle="tab"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Add Activity</a></li>
							<li><a href="#tabNote" data-toggle="tab"><i class="fa fa-sticky-note" aria-hidden="true"></i> Take Notes</a></li>
							<li><a href="#tabEmail" data-toggle="tab"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send Email</a></li>
							<li><a href="#tabsFiles" data-toggle="tab"><i class="fa fa-paperclip" aria-hidden="true"></i> Upload Files</a></li>
							<li><a href="#tabsSms" data-toggle="tab"><i class="fa fa-paperclip" aria-hidden="true"></i> Send SMS</a></li>
						</ul>
						<div class="tab-content tab-content-bordered margin-bottom-20">
							<div class="tab-pane" id="tabNote">
								<form id="form_addnote" action="Api_paciente/agregarnota" method="post">
									<input name="tipo" value="1" type="hidden">
									<div class="form-group">
										<label>Note <b class="text-danger">*</b></label>
										<textarea name="note" class="summernote" data-validation="required"></textarea>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-floppy-o"></span>Save Note</button>
									</div>
								</form>
							</div>
							<div class="tab-pane active" id="tabActi">
								<form id="form_addact" action="Api_paciente/agregaractividad" method="post">
									<input name="tipo" value="2" type="hidden">
									<fieldset class="col-md-6">
										<div class="form-group">
											<label class="col-md-12 control-label">Type</label>
											<div class="col-md-12">
												<div class="btn-group" data-toggle="buttons">
												  <label class="btn btn-default btn-sm active"><input type="radio" name="type" value="1" checked="checked"><i class="fa fa-phone"></i> Call</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="2"><i class="fa fa-users"></i> Meeting</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="3"><i class="fa fa-clock-o"></i> Task</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="4"><i class="fa fa-flag"></i> Deadline</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="5"><i class="fa fa-envelope"></i> Email</label>
												  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="6"><i class="fa fa-cutlery"></i> Lunch</label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">User</label>
											<div class="col-md-8">
												<select class="form-control bs-select" data-live-search="true" name="user" data-validation="required" data-style="btn-sm btn btn-default"></select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Date</label>
											<div class="col-md-8">
												<input name="startdate" type="text" class="form-control input-sm bs-datepickerusa_dis_past" data-validation="required">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Hour Start</label>
											<div class="col-md-8">
												<input name="starthour" type="text" class="form-control input-sm timepicker" data-validation="required" disabled>
											</div>
										</div>
										<!--<div class="form-group">
											<label class="col-md-4 control-label">End Time</label>
											<div class="col-md-8">
												<input name="finishhour" type="text" class="form-control input-sm timepicker" data-validation="required" disabled>
											</div>
										</div>-->
										<div class="form-group">
											<label class="col-md-12 control-label">Description</label>
											<div class="col-md-12 margin-left-5">
												<textarea name="note" class="summernote" data-validation="required"></textarea>
											</div>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-floppy-o"></span>Save Activity</button>
										</div>
									</fieldset>
									<fieldset class="col-md-6" height="650px">
										<div id="calendar"></div>
									</fieldset>
								</form>
							</div>
							<div class="tab-pane" id="tabEmail">
							<form id="form_sendemail" class="form-horizontal" action="Api_paciente/sendemail" method="post">
								<div class="form-group">
									<label class="col-md-2">From <b class="text-danger">*</b></label>                                            
									<div class="col-md-10">
										<select class="form-control bs-select" data-validation="required" data-live-search="true" data-style="btn-sm btn btn-default">
											<option>System@smile4ever.com</option>
										</select>                                            
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">To <b class="text-danger">*</b></label>                                            
									<div class="col-md-10">
										<input name="to" type="text" class="form-control input-sm" data-validation="required" data-role="tagsinput">
									</div>
								</div>
								<div class="form-group" id="mail-cc">
									<label class="col-md-2">Cc </label>                                            
									<div class="col-md-10">
										<input name="cc" type="text" class="form-control input-sm" data-role="tagsinput" data-placeholder="add email">                                            
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Subject <b class="text-danger">*</b></label>
									<div class="col-md-10">
										<input name="subject" type="text" class="form-control input-sm" data-validation="required">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2">Attachement</label>                                            
									<div class="col-md-10">
										<select name="templates[]" class="form-control bs-select" data-live-search="true" multiple data-style="btn-sm btn btn-default"></select>                                            
									</div>
								</div>
								<div class="form-group" id="fileinputs">
									<label class="col-md-2">File Attachement</label>                                            
									<div class="col-md-10">
										<input name="files[]" type="file" class="form-control" multiple />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Body <b class="text-danger">*</b></label>
									<div class="col-md-12">
										<textarea  name="body" class="summernote"></textarea>                                                                        
									</div>
								</div>
								<div class="form-group">
									<div class="pull-right">
										<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-envelope"></span>Send Email</button>
									</div>
								</div>
							</form>
							</div>
							<div class="tab-pane" id="tabsFiles">
								<h5>Allowed </h5>
								<p><b>Size:</b> 2MB. <b>Types:</b> JPG | PNG | PDF</p>
								<form id="formaddfile" class="dropzone" enctype="multipart/form-data"></form>
							</div>
							<div class="tab-pane" id="tabsSms">
								<form id="form_sendsms" class="form-horizontal" action="Api_paciente/sms" method="post">
								<input name="tipo" value="5" type="hidden">
									<div class="form-group">
										<label class="col-md-2">Number <b class="text-danger">*</b></label>                                            
										<div class="col-md-10">
											<input name="number" type="text" class="form-control input-sm" data-validation="required">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2">Message <b class="text-danger">*</b></label>
										<div class="col-md-10">
											<textarea name="message" class="form-control" rows="5" data-validation="required"></textarea>
										</div>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-success btn-icon-fixed pull-right"><span class="fa fa-paper-plane"></span>Send sms</button>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<div id="filter_log" class="btn-group" role="group">
								<button data-type="all" type="button" class="btn btn-default btn-xs">All</button>
								<button data-type="2" type="button" class="btn btn-default btn-xs">Activities</button>
								<button data-type="1" type="button" class="btn btn-default btn-xs">Notes</button>
								<button data-type="3" type="button" class="btn btn-default btn-xs">Files</button>
								<button data-type="4" type="button" class="btn btn-default btn-xs">Email</button>
								<button data-type="5" type="button" class="btn btn-default btn-xs">SMS</button>
								<button type="button" class="btn btn-default btn-xs">ChangeLog</button>
							</div>                                    
						</div>
						<div id="activitiestimeline" style="height: 500px;" class="app-timeline"></div>
					</div>
				</div>

			</div>
		</div>
	</div>
	
</div>

<div class="modal fade" id="modal_editact" tabindex="-1" role="dialog" aria-labelledby="modal-warning-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editact"  autocomplete="off" class="form-horizontal" action="Api_paciente/editaracti" method="post">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Edit Activity</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="col-md-12 control-label">Type</label>
					<div class="col-md-12">
						<div class="btn-group" data-toggle="buttons">
						  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="1"><i class="fa fa-phone"></i> Call</label>
						  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="2"><i class="fa fa-users"></i> Meeting</label>
						  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="3"><i class="fa fa-clock-o"></i> Task</label>
						  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="4"><i class="fa fa-flag"></i> Deadline</label>
						  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="5"><i class="fa fa-envelope"></i> Email</label>
						  <label class="btn btn-default btn-sm"><input type="radio" name="type" value="6"><i class="fa fa-cutlery"></i> Lunch</label>
					</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12 control-label">Description</label>
					<div class="col-md-12 margin-left-5">
						<textarea name="note" class="summernote" data-validation="required"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>

<div class="modal fade" id="modal_editnote" tabindex="-1" role="dialog" aria-labelledby="modal-warning-header">                        
	<div class="modal-dialog modal-warning" role="document">
		<div class="modal-content">
		<form id="form_editnote"  autocomplete="off" action="Api_paciente/editarnota" method="post">
			<input name="id" type="hidden">
			<div class="modal-header">                        
				<h4 class="modal-title" id="modal-danger-header">Edit Note</h4>
			</div>
			<div class="modal-body">
				<input name="tipo" value="1" type="hidden">
				<div class="form-group">
					<label>Note <b class="text-danger">*</b></label>
					<textarea name="note" class="summernote" data-validation="required"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
		</div>
	</div>            
</div>
        