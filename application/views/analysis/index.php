	<style>
	.button_example{
		/*background-size: contain;
		background-position: right;
		background-repeat: no-repeat;
		background-image: url(https://terapiametabolica.com/wp-content/uploads/2013/03/left-arrow-right-hi.png);
		*/
		min-width: 250px;
		background-color:#F5F5F5;
		/*border:1px solid #cacaca; 
		font-size:12px;font-family:arial, helvetica, sans-serif; 
		padding: 10px 10px 10px 10px; 
		color:#FFFFFF;
		  font-weight:bold; color: #FFFFFF;
		background-color: #E6E6E6; background-image: -webkit-gradient(linear, left top, left bottom, from(#E6E6E6), to(#CCCCCC));
		background-image: -webkit-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -moz-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -ms-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: -o-linear-gradient(top, #E6E6E6, #CCCCCC);
		background-image: linear-gradient(to bottom, #E6E6E6, #CCCCCC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E6E6E6, endColorstr=#CCCCCC);*/
	}
	</style>
	<div class="app-heading">
		<span class="icon label-icon label-icon-primary label-icon-rounded"><i class="fa fa-usd"></i></span>
		<div class="title">
			<h2>Deals</h2>
			<ul class="breadcrumb">
				<li><a href="<?=base_url()?>">Home</a></li>                                                     
				<li>Patient</li>
				<li class="active">Deals</li>
			</ul>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- BLOCk -->
				<div class="block">
					<div class="app-heading app-heading-small padding-0">                                        
						<div class="title">
							<h2>Providers comparison</h2>
							<p id="origencheck"></p>
						</div>                 
						<div class="heading-elements">
							<button class="btn btn-default btn-sm btn-icon" onclick="app.block.toggle('#block-toggle');"><span class="fa fa-chevron-down"></span></button>
						</div>                
					</div>
					
					<div class="block-content padding-10" style="height: 400px; overflow-y: scroll;">
						<table id="tbl_provcomp" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th class="padding-5">Mount</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						
						<table id="dtproviders" width="100%" class="table table-bordered table-head-custom">
							<thead>
								<tr>
									<th>Name</th>
									<th>Mount</th>
								</tr>
							</thead> 
						</table>
					</div>
				</div>
			</div>
		</div>	
</div>