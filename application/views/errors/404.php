<!DOCTYPE html>
<html lang="en">
    <head>                        
        <title>Boooya - 404</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?=base_url()?>css/styles.css">
    </head>
    <body>
        <div class="app" style="height: 100%;">           
            <div class="app-container app-error-container">                
                <div class="app-error">                    
                    <div class="app-error-code">404</div>
					<div class="app-error-message">The page you requested was not found.</div>
                    <div class="app-error-message">Woooops... something has gone terribly wrong here</div>
                    <div class="app-error-button">
                        <a href="<?=base_url()?>" class="btn btn-primary btn-lg btn-shadowed btn-rounded">Back to home</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/moment/moment.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/vendor/customscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/app.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/app_plugins.js"></script>
        <script type="text/javascript" src="<?=base_url()?>js/app_demo.js"></script>
    </body>
</html>